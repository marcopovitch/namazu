# 1. Editeur

Ce site est le site officiel de l'action transverse sismicité de RESIF.
Adresse RESIF : RESIF - 3 rue Michel-Ange, F-75794 Paris cedex 16

* Directeur de publication : Andréa WALPERSDORF
* Responsable éditorial : Frédéric MASSON
* Hébergeur : DSI de l'Université de Strasbourg

# 2. Protection des informations nominatives

Conformément à la loi n° 78-17 du 6 janvier 1978, relative à l’Informatique, aux fichiers et aux Libertés (articles 38, 39, 40), vous disposez d’un droit d’accès, de rectification et de suppression des données vous concernant, en ligne sur ce site.
Pour exercer ce droit, vous pouvez vous adresser au webmestre.

# 3. Conditions d’utilisation

## 3.1. Compatibilité navigateurs

Ce site est optimisé pour les versions actuelles des principaux navigateurs (Mozilla Firefox, Google Chrome, Safari, Internet Explorer). Nous nous efforçons de le maintenir compatible avec les nouvelles versions.

## 3.2. Clause de non-responsabilité

La responsabilité de RESIF ne peut, en aucune manière, être engagée quant au contenu des informations figurant sur ce site ou aux conséquences pouvant résulter de leur utilisation ou interprétation.

## 3.3. Propriété intellectuelle

Ce site est une œuvre de création, propriété exclusive de RESIF, protégé par la législation française et internationale sur le droit de la propriété intellectuelle. Aucune reproduction ou représentation ne peut être réalisée en contravention avec les droits de RESIF issus de la législation susvisée.

## 3.4. Liens hypertextes

La mise en place de liens hypertextes par des tiers vers des pages ou des documents diffusés sur ce site, est autorisée sous réserve que les liens ne contreviennent pas aux intérêts de RESIF, et, qu’ils garantissent la possibilité pour l’utilisateur d’identifier l’origine et l’auteur du document. 
Ce site web est susceptible de renvoyer à d'autres sites au moyen de liens ; RESIF ne saurait être tenu responsable du contenu de ceux-ci.

# 4. Crédits

## 4.1. Réalisation technique

Ce site est réalisé par l'Ecole et observatoire des sciences de la Terre (EOST) de Strasbourg.

## 4.2. Crédits photographiques

* Images des bandeaux d’entête : © RESIF
* Images insérées dans les pages : se référer aux crédits mentionnés en légende, par défaut : © RESIF
