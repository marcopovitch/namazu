## Mayotte

En mai 2018 une séquence sismique a débuté à proximité du département français de Mayotte. Dans les mois qui ont suivi, des moyens scientifiques inédits ont été mobilisés pour tenter de comprendre cette crise dans une zone dont la sismicité est mal connue.

 Cette page liste les trente derniers évènements enregistrés par les stations sismologiques de Mayotte, des Comores et des réseaux mondiaux.   Les évènements plus anciens sont disponible via le lien en fin de page "Evènements précédents".

>**A noter:** pour suivre l’actualité des campagnes scientifiques et obtenir les bulletins de suivi de l’activité sismo-volcanique à Mayotte, consultez le site du  [ReVoSiMa](http://www.ipgp.fr/fr/reseau-de-surveillance-volcanologique-sismologique-de-mayotte), le réseau de surveillance volcanologique et sismologique de Mayotte,  et celui du [BRGM](https://www.brgm.fr/regions/reseau-regional/mayotte).