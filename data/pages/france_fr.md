## France métropolitaine

Cette page liste les derniers évènements sismiques survenus sur le territoire métropolitain ou à proximité. Les stations de surveillance du Réseau sismologique et géodésique français [RESIF](https://www.resif.fr) et les stations des réseaux des pays frontaliers (Allemagne, Angleterre, Belgique, Espagne, Italie, Luxembourg, Suisse) enregistrent en continu les secousses et  transmettent les mesures en temps réel vers le BCSF-RENASS. 

Tous les évènements sont d’abord détéctés et localisés automatiquement avant de faire l’objet d’une analyse par un opérateur qui détermine, notamment, leur origine sismique ou anthropique (par exemple les tirs de carrière ou les explosions, voir onglet "Autres évènements").