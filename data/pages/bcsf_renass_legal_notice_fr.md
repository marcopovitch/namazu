# Crédits et mentions légales

Editeur
=======

Ce site est le site officiel du [Service National d'Observation](http://www.insu.cnrs.fr/fr/les-services-nationaux-dobservation) BCSF-RENASS (Bureau central sismologique français - Réseau national de surveillance sismique), labellisé par l’[INSU](http://www.insu.cnrs.fr), 
et de l\'action transverse sismicité de [RESIF](http://www.resif.fr/). 

Il est opéré par le BCSF-RENASS situé à l\'Ecole et observatoire des sciences de la
Terre à Strasbourg.

Adresse : EOST, 5 rue René Descartes, 67000 Strasbourg

-   **Directeur de publication :** Frédéric MASSON, Directeur de l\'EOST

-   **Responsable éditorial** : Marc Grunberg, Antoine Schlupp

-   **Hébergeur :** DSI de l\'Université de Strasbourg

Protection des informations nominatives
=======================================

Conformément à la loi n° 78-17 du 6 janvier 1978, relative à
l'Informatique, aux fichiers et aux Libertés (articles 38, 39, 40), vous
disposez d'un droit d'accès, de rectification et de suppression des
données vous concernant, en ligne sur ce site. Pour exercer ce droit,
vous pouvez vous adresser
au [webmestre](mailto:bcsf-renass@unistra.fr).

Conditions d'utilisation
========================

Compatibilité navigateurs
-------------------------

Ce site est optimisé pour les versions actuelles des principaux
navigateurs (Mozilla Firefox, Google Chrome, Safari, Internet Explorer).
Nous nous efforçons de le maintenir compatible avec les nouvelles
versions.

Contenu du site - Responsabilité
--------------------------------

En dépit du soin apporté au recueil des informations ainsi qu\'à la
réalisation du site, des erreurs, omissions, inexactitudes, coupures ou
additions indépendantes de notre volonté peuvent demeurer ou s\'insérer
sur ce site. En outre, de même que pour toute publication, après un
certain temps, des informations ou éléments contenus dans ce site,
peuvent devenir obsolètes. Les informations fournies sont non
contractuelles.\
\
L\'utilisateur du site et des informations qu\'il contient reconnaît
qu\'il en fait usage sous sa seule responsabilité. L\'EOST ne saurait en
conséquence voire sa responsabilité engagée à raison de tout préjudice,
direct ou indirect, de quelque nature que ce soit, résultant pour tout
ou partie de l\'utilisation des informations du site.\
\
En outre, l\'EOST ne saurait voir sa responsabilité engagée à raison de
la nature ou du contenu des sites référencés sur les pages du site et
notamment ceux pour lesquels il existe un lien direct aux ressources
constituées par les sites tiers sélectionnés.

Propriété intellectuelle
------------------------

Les contenus, y compris les images, cartes et données du site sont
protégés par le droit de la propriété intellectuelle et sont la
propriété de l\'EOST.

Dans une démarche de science ouverte, la reproduction, la réutilisation
et la mise à disposition du public des informations et cartes de
sismicité est autorisée, sous réserve de citation de la source.

Données personnelles
--------------------

<!-- A compléter quand formulaire témoignage. -->

Liens hypertextes
-----------------

La mise en place de liens hypertextes par des tiers vers des pages ou
des documents diffusés sur ce site, est autorisée sous réserve que les
liens ne contreviennent pas aux intérêts de l\'EOST, et, qu'ils
garantissent la possibilité pour l'utilisateur d'identifier l'origine et
l'auteur du document. 

Ce site web est susceptible de renvoyer à d\'autres sites au moyen de
liens ; l\'EOST ne saurait être tenu responsable du contenu de ceux-ci.

Crédits
=======

Crédits photographiques
-----------------------

-   Images des bandeaux d'entête : © BCSF-RENASS

-   Images insérées dans les pages : se référer aux crédits mentionnés
    en légende, par défaut : © BCSF-RENASS
