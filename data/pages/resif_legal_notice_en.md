# 1. Editor

This site is the official website of RESIF's transverse seismicity action.
RESIF Address : RESIF - 3 rue Michel-Ange, 75794 Paris cedex 16 - France

* Publication Director : Andréa  WALPERSDORF
* Editorial Heads : Frédéric Masson
* Web Host : DSI de l'Université de Strasbourg

# 2. Protection of personal information

In accordance with Law No. 78-17 of 6 January 1978, relating to computing, files and Freedoms (Articles 38, 39, 40), you have a right to access, correct and delete information about you, online on this site.
To exercise this right, please contact the webmaster .

# 3. Terms of use

## 3.1. Browser compatibility

This website is optimized for the current versions of major browsers (Mozilla Firefox, Google Chrome, Safari, Internet Explorer). We strive to keep it compatible with new versions.

## 3.2. Disclaimer of Liability (use)

Responsibility for RESIF cannot, in no way, be committed for the content of the information on this site or the consequences that may result from their use or interpretation.

## 3.3. Intellectual property

This site is a creative work, exclusive property of RESIF, protected by French and international legislation on the right to intellectual property. No reproduction or representation can be done in contravention of the rights of RESIF from the above legislation.

## 3.4. Hyperlinks

The establishment of hyperlinks by others to pages or documents available on this website, is authorized provided that the links are not contrary to the interests of RESIF, and that they guarantee the possibility for the user to identify the origin and the author of the document.
This RESIF website may refer to other websites via links; RESIF cannot be held responsible for the content thereof.

# 4. Credits

## 4.1. Technical implmentation

This website is achieved by Ecole et observatoire des sciences de la Terre (EOST) de Strasbourg.

## 4.2. Photo credits

* Images of header bands: © RESIF
* Images inserted in the pages: see the credits mentioned in legend, default : © RESIF
