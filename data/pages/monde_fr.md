## Monde

Vous trouverez ici les informations sur les évènements sismiques mondiaux majeurs. Sauf exception, ces évènements ne font pas l’objet d’une analyse par un opérateur (uniquement la localisation automatique faite par le BCSF-RENASS).

Pour en savoir plus sur les séismes de forte magnitude dans le monde, consultez le site web du [CSEM](https://www.emsc-csem.org) et de l’[USGS](https://earthquake.usgs.gov).