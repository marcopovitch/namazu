defmodule Namazu.Application do
  @moduledoc false

  use Application

  @default_parameters [
    %{key: "site_description", value: "**Please update site_description parameter**"},
    %{key: "contact_phone", value: "000-000-0000"},
    %{key: "contact_email", value: "update@youremail.com"},
    %{key: "contact_address", value: "Update your address"},
    %{key: "contact_address_longitude", value: "7.7629490"},
    %{key: "contact_address_latitude", value: "48.5796009"}
  ]

  def start(_type, _args) do
    children = [
      Supervisor.child_spec({Namazu.Repo, []}, %{}),
      Supervisor.child_spec(
        {ConCache, [name: :parameters, ttl_check_interval: false]},
        id: :concache_parameters
      ),
      Supervisor.child_spec(
        {ConCache, [name: :events, ttl_check_interval: 5000, global_ttl: 30_000]},
        id: :concache_events
      ),
      # Supervisor.child_spec(
      #   {ConCache, [name: :zones, ttl_check_interval: 5000, global_ttl: 60_000]},
      #   id: :concache_zones
      # ),
      Supervisor.child_spec(
        {ConCache, [name: :misc, ttl_check_interval: 5000, global_ttl: 60_000]},
        id: :concache_misc
      ),
      Supervisor.child_spec(
        {ConCache, [name: :stations, ttl_check_interval: 60_000, global_ttl: 86_400_000]},
        id: :concache_stations
      ),
      Supervisor.child_spec(
        {ConCache, [name: :past_seismicity, ttl_check_interval: 60_000, global_ttl: 86_400_000]},
        id: :concache_past_seismicity
      ),
      Supervisor.child_spec(
        {ConCache, [name: :pages, ttl_check_interval: false]},
        id: :concache_pages
      )
    ]

    opts = [strategy: :one_for_one, name: Namazu.Supervisor]
    link = Supervisor.start_link(children, opts)

    case link do
      {:ok, _} ->
        Namazu.Repo.insert_all(Namazu.Parameter, @default_parameters, on_conflict: :nothing)

        link

      _ ->
        link
    end
  end
end
