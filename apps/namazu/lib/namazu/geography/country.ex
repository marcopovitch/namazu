defmodule Namazu.Geography.Country do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :geography

  schema "countries" do
    has_many(:cities, Namazu.Geography.City)

    field(:code_iso2, :string)
    field(:code_iso3, :string)

    field(:source, :string)
    field(:source_version, :string)
    field(:source_id, :string)

    field(:name, :string)
    field(:name_ar, :string)
    field(:name_bn, :string)
    field(:name_de, :string)
    field(:name_el, :string)
    field(:name_en, :string)
    field(:name_es, :string)
    field(:name_fr, :string)
    field(:name_hi, :string)
    field(:name_hu, :string)
    field(:name_id, :string)
    field(:name_it, :string)
    field(:name_ja, :string)
    field(:name_ko, :string)
    field(:name_nl, :string)
    field(:name_pl, :string)
    field(:name_pt, :string)
    field(:name_ru, :string)
    field(:name_sv, :string)
    field(:name_tr, :string)
    field(:name_vi, :string)
    field(:name_zh, :string)

    field(:boundary, Geo.PostGIS.Geometry)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:source, :source_id]
  end

  def changeset(country, params \\ %{}) do
    country
    |> cast(params, [
      :code_iso2,
      :code_iso3,
      :source,
      :source_version,
      :source_id,
      :name,
      :name_ar,
      :name_bn,
      :name_de,
      :name_el,
      :name_en,
      :name_es,
      :name_fr,
      :name_hi,
      :name_hu,
      :name_id,
      :name_it,
      :name_ja,
      :name_ko,
      :name_nl,
      :name_pl,
      :name_pt,
      :name_ru,
      :name_sv,
      :name_tr,
      :name_vi,
      :name_zh,
      :boundary
    ])
  end
end
