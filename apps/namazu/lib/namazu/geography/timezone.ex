defmodule Namazu.Geography.Timezone do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :geography
  @primary_key {:timezone_id, :string, autogenarate: true}

  schema "timezones" do
    field(:boundary, Geo.PostGIS.Geometry)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:timezone_id]
  end

  def changeset(timezone, params \\ %{}) do
    timezone
    |> cast(params, [
      :timezone_id,
      :boundary
    ])
  end
end
