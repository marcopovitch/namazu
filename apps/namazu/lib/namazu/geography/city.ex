defmodule Namazu.Geography.City do
  use Ecto.Schema

  import Geo.PostGIS
  import Ecto.Changeset
  import Ecto.Query

  alias Namazu.Repo

  @schema_prefix :geography

  schema "cities" do
    belongs_to(:country, Namazu.Geography.Country)

    field(:country_iso2, :string)
    field(:district, :boolean)
    field(:administrative_code_type, :string)
    field(:administrative_code, :string)
    field(:population, :integer)

    field(:source, :string)
    field(:source_version, :string)
    field(:source_id, :string)
    field(:geoname_id, :integer)

    field(:latitude, :float)
    field(:longitude, :float)
    field(:location_type, :string)
    field(:location, Geo.PostGIS.Geometry)
    field(:boundary, Geo.PostGIS.Geometry)

    field(:period_start, :utc_datetime_usec)
    field(:period_end, :utc_datetime_usec)

    field(:name, :string)
    field(:name_ar, :string)
    field(:name_bn, :string)
    field(:name_de, :string)
    field(:name_el, :string)
    field(:name_en, :string)
    field(:name_es, :string)
    field(:name_fr, :string)
    field(:name_hi, :string)
    field(:name_hu, :string)
    field(:name_id, :string)
    field(:name_it, :string)
    field(:name_ja, :string)
    field(:name_ko, :string)
    field(:name_nl, :string)
    field(:name_pl, :string)
    field(:name_pt, :string)
    field(:name_ru, :string)
    field(:name_sv, :string)
    field(:name_tr, :string)
    field(:name_vi, :string)
    field(:name_zh, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(city, params \\ %{}) do
    city
    |> cast(params, [
      :country_iso2,
      :district,
      :administrative_code_type,
      :administrative_code,
      :population,
      :source,
      :source_version,
      :source_id,
      :geoname_id,
      :latitude,
      :longitude,
      :location_type,
      :location,
      :boundary,
      :name,
      :name_ar,
      :name_bn,
      :name_de,
      :name_el,
      :name_en,
      :name_es,
      :name_fr,
      :name_hi,
      :name_hu,
      :name_id,
      :name_it,
      :name_ja,
      :name_ko,
      :name_nl,
      :name_pl,
      :name_pt,
      :name_ru,
      :name_sv,
      :name_tr,
      :name_vi,
      :name_zh
    ])
  end

  def distinctive_fields() do
    [:source, :source_id]
  end

  def distintive_cities(location, distance, limit \\ 15) do
    cities =
      __MODULE__
      |> maximal_distance(location, distance)
      |> select([c], %{
        name: c.name,
        population: c.population,
        country_iso2: c.country_iso2,
        distance: st_distance_in_meters(c.location, ^location) / 1000
      })
      |> order_by(desc: :population)
      |> limit(^limit)
      |> Repo.all()

    if length(cities) < limit do
      distintive_cities(location, distance * 2, limit)
    else
      cities
    end
  end

  def maximal_distance(query, location = %Geo.Point{}, max_distance) do
    query
    |> where([c], st_dwithin_in_meters(c.location, ^location, ^max_distance * 1000))
  end

  def match_by_boundary(query, boundary = %Geo.MultiPolygon{}, district \\ false) do
    query
    |> where([c], st_within(c.location, ^boundary) and c.district == ^district)
    |> Repo.one()
  end

  def match_by_location(query, name, location = %Geo.Point{}, _district \\ false) do
    query
    |> maximal_distance(location, 10)
    |> where([c], c.name == ^name)
    |> Repo.one()
    |> case do
      nil ->
        splitted_name =
          name
          |> String.split("-")
          |> List.first()

        __MODULE__
        |> maximal_distance(location, 10)
        |> where([c], c.name == ^splitted_name)
        |> Repo.one()
        |> case do
          nil ->
            __MODULE__
            |> maximal_distance(location, 10)
            |> where([c], fragment("levenshtein(?, ?) < 2", c.name, ^name))
            |> Repo.one()

          matched_city ->
            matched_city
        end

      matched_city ->
        matched_city
    end
  end
end
