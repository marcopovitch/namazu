defmodule Namazu.Geography.ExclusiveEconomicZone do
  use Ecto.Schema

  @schema_prefix :geography

  schema "exclusive_economic_zones" do
    field(:code_iso3, :string)
    field(:country_name, :string)
    field(:buffer, :integer)

    field(:boundary, Geo.PostGIS.Geometry)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end
end
