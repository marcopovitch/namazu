defmodule Namazu.Search.EventSearch do
  alias Namazu.Seismic

  import Ecto.Query

  defstruct start_time: nil,
            end_time: nil,
            minimal_longitude: nil,
            maximal_longitude: nil,
            minimal_latitude: nil,
            maximal_latitude: nil,
            minimal_magnitude: nil,
            maximal_magnitude: nil,
            minimal_depth: nil,
            maximal_depth: nil,
            event_types: nil,
            event_id: nil,
            limit: nil,
            offset: nil,
            order_by: nil

  def start_time_filter(query, _event_search = %{start_time: nil}), do: query

  def start_time_filter(query, event_search) do
    query |> where([preferred_origin: po], ^dynamic(^Seismic.start_time(event_search.start_time)))
  end

  def end_time_filter(query, _event_search = %{end_time: nil}), do: query

  def end_time_filter(query, event_search) do
    query |> where([preferred_origin: po], ^dynamic(^Seismic.end_time(event_search.end_time)))
  end

  def minimal_longitude_filter(query, _event_search = %{minimal_longitude: nil}), do: query

  def minimal_longitude_filter(query, event_search) do
    query
    |> where(
      [preferred_origin: po],
      ^dynamic(^Seismic.minimal_longitude(event_search.minimal_longitude))
    )
  end

  def maximal_longitude_filter(query, _event_search = %{maximal_longitude: nil}), do: query

  def maximal_longitude_filter(query, event_search) do
    query
    |> where(
      [preferred_origin: po],
      ^dynamic(^Seismic.maximal_longitude(event_search.maximal_longitude))
    )
  end

  def minimal_latitude_filter(query, _event_search = %{minimal_latitude: nil}), do: query

  def minimal_latitude_filter(query, event_search) do
    query
    |> where(
      [preferred_origin: po],
      ^dynamic(^Seismic.minimal_latitude(event_search.minimal_latitude))
    )
  end

  def maximal_latitude_filter(query, _event_search = %{maximal_latitude: nil}), do: query

  def maximal_latitude_filter(query, event_search) do
    query
    |> where(
      [preferred_origin: po],
      ^dynamic(^Seismic.maximal_latitude(event_search.maximal_latitude))
    )
  end

  def minimal_magnitude_filter(query, _event_search = %{minimal_magnitude: nil}), do: query

  def minimal_magnitude_filter(query, event_search) do
    query
    |> where(
      [preferred_origin: po],
      ^dynamic(^Seismic.minimal_magnitude(event_search.minimal_magnitude))
    )
  end

  def maximal_magnitude_filter(query, _event_search = %{maximal_magnitude: nil}), do: query

  def maximal_magnitude_filter(query, event_search) do
    query
    |> where(
      [preferred_origin: po],
      ^dynamic(^Seismic.maximal_magnitude(event_search.maximal_magnitude))
    )
  end

  def minimal_depth_filter(query, _event_search = %{minimal_depth: nil}), do: query

  def minimal_depth_filter(query, event_search) do
    query
    |> where([preferred_origin: po], ^dynamic(^Seismic.minimal_depth(event_search.minimal_depth)))
  end

  def maximal_depth_filter(query, _event_search = %{maximal_depth: nil}), do: query

  def maximal_depth_filter(query, event_search) do
    query
    |> where([preferred_origin: po], ^dynamic(^Seismic.maximal_depth(event_search.maximal_depth)))
  end

  def event_types_filter(query, _event_search = %{event_types: nil}), do: query
  def event_types_filter(query, _event_search = %{event_types: []}), do: query

  def event_types_filter(query, event_search) do
    query
    |> where([preferred_origin: po], ^dynamic(^Seismic.event_types(event_search.event_types)))
  end

  def event_id_filter(query, _event_types = %{event_id: nil}), do: query

  def event_id_filter(query, event_search) do
    query |> where([preferred_origin: po], ^dynamic(^Seismic.event_id(event_search.event_id)))
  end

  def limit_filter(query, _event_search = %{limit: nil}), do: query

  def limit_filter(query, event_search) do
    query |> limit(^event_search.limit)
  end

  def offset_filter(query, _event_search = %{offset: nil}), do: query

  def offset_filter(query, event_search) do
    query |> offset(^event_search.offset)
  end

  def order_by_filter(query, event_search) do
    case event_search.order_by do
      :descending_time -> query |> Seismic.descending_time()
      :ascending_time -> query |> Seismic.ascending_time()
      :descending_magnitude -> query |> Seismic.descending_magnitude()
      :ascending_magnitude -> query |> Seismic.ascending_magnitude()
      nil -> query
    end
  end

  def query(event_search = %__MODULE__{event_id: nil}) do
    Seismic.event_query()
    |> Seismic.preload_preferred()
    |> start_time_filter(event_search)
    |> end_time_filter(event_search)
    |> minimal_longitude_filter(event_search)
    |> maximal_longitude_filter(event_search)
    |> minimal_latitude_filter(event_search)
    |> maximal_latitude_filter(event_search)
    |> minimal_magnitude_filter(event_search)
    |> maximal_magnitude_filter(event_search)
    |> minimal_depth_filter(event_search)
    |> maximal_depth_filter(event_search)
    |> event_types_filter(event_search)
    |> limit_filter(event_search)
    |> offset_filter(event_search)
    |> order_by_filter(event_search)
  end

  def query(event_search = %__MODULE__{}) do
    Seismic.event_query()
    |> Seismic.preload_preferred()
    |> event_id_filter(event_search)
  end

  def fdsn_query_params(event_search = %Namazu.Search.EventSearch{}) do
    %{
      "starttime" =>
        if event_search.start_time do
          DateTime.to_iso8601(event_search.start_time)
        else
          nil
        end,
      "endtime" =>
        if event_search.end_time do
          DateTime.to_iso8601(event_search.end_time)
        else
          nil
        end,
      "minlatitude" => event_search.minimal_latitude,
      "maxlatitude" => event_search.maximal_latitude,
      "minlongitude" => event_search.minimal_longitude,
      "maxlongitude" => event_search.maximal_longitude,
      "mindepth" => event_search.minimal_depth,
      "maxdepth" => event_search.maximal_depth,
      "minmagnitude" => event_search.minimal_magnitude,
      "maxmagnitude" => event_search.maximal_magnitude,
      "eventtype" =>
        event_search.event_types
        |> Enum.map(&Namazu.Seismic.Event.namazu_to_quakeml_event_type(&1))
        |> Enum.join(","),
      "eventid" => event_search.event_id,
      "limit" => event_search.limit,
      "offset" => event_search.offset,
      "orderby" => event_search.order_by
    }
    |> Enum.filter(fn {_key, value} -> not is_nil(value) and value != "" end)
    |> Enum.into(%{})
  end

  def fdsn_query_path(event_search, format \\ nil) do
    params = fdsn_query_params(event_search)

    params =
      if format do
        Map.put(params, "format", format)
      else
        params
      end
      |> Enum.map(fn {key, value} -> {key, "#{value}"} end)
      |> Enum.into(%{})

    Web.Router.Helpers.fdsn_event_path(Web.Endpoint, :query, params)
  end
end
