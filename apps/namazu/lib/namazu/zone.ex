defmodule Namazu.Zone do
  use Ecto.Schema
  import Ecto.Query

  alias Namazu.Seismic
  alias Namazu.Repo

  @schema_prefix :namazu

  schema "zones" do
    belongs_to(:exclusive_economic_zone, Namazu.Geography.ExclusiveEconomicZone)

    field(:minimal_latitude, :float)
    field(:maximal_latitude, :float)
    field(:minimal_longitude, :float)
    field(:maximal_longitude, :float)

    field(:name, :string)
    field(:slug, :string)
    field(:weight, :integer)
    field(:description, :string)
    field(:introduction, :string)

    field(:show_manual, :boolean)
    field(:manual_minimal_magnitude, :float)
    field(:manual_maximal_depth, :float)
    field(:manual_minimal_used_phase_count, :integer)
    field(:manual_event_types, {:array, :string})

    field(:show_automatic, :boolean)
    field(:automatic_minimal_magnitude, :float)
    field(:automatic_maximal_depth, :float)
    field(:automatic_minimal_used_phase_count, :integer)
  end

  def get_events(zone) do
    Seismic.event_query()
    |> Seismic.preload_preferred()
    |> Seismic.preload_preferred_origin_quality()
    |> where(
      ^dynamic(
        ^Seismic.existing_and_locatable() and
          ^Seismic.minimal_longitude(zone.minimal_longitude) and
          ^Seismic.maximal_longitude(zone.maximal_longitude) and
          ^Seismic.minimal_latitude(zone.minimal_latitude) and
          ^Seismic.maximal_latitude(zone.maximal_latitude) and
          ^Seismic.only_manual() and
          ^Seismic.event_types(zone.manual_event_types) and
          ^Seismic.maximal_depth(zone.manual_maximal_depth) and
          ^Seismic.minimal_used_phase_count(zone.manual_minimal_used_phase_count) and
          ^Seismic.minimal_magnitude(zone.manual_minimal_magnitude)
      )
    )
    |> or_where(
      ^dynamic(
        ^Seismic.existing_and_locatable() and
          ^Seismic.minimal_longitude(zone.minimal_longitude) and
          ^Seismic.maximal_longitude(zone.maximal_longitude) and
          ^Seismic.minimal_latitude(zone.minimal_latitude) and
          ^Seismic.maximal_latitude(zone.maximal_latitude) and
          ^Seismic.only_automatic() and
          ^Seismic.maximal_depth(zone.automatic_maximal_depth) and
          ^Seismic.minimal_used_phase_count(zone.automatic_minimal_used_phase_count) and
          ^Seismic.minimal_magnitude(zone.automatic_minimal_magnitude)
      )
    )
  end

  def get_stations(zone) do
    from(s in Namazu.Instruments.Station,
      join: sp in assoc(s, :periods),
      join: n in assoc(s, :network),
      where: is_nil(sp.period_end) and not s.hidden and not n.hidden,
      where: sp.longitude >= ^zone.minimal_longitude and sp.longitude <= ^zone.maximal_longitude,
      where: sp.latitude >= ^zone.minimal_latitude and sp.latitude <= ^zone.maximal_latitude,
      preload: [periods: sp, network: n],
      order_by: [asc: n.network_code, asc: s.station_code]
    )
  end

  def get_from_location(_location = %Geo.Point{coordinates: {longitude, latitude}}) do
    __MODULE__
    |> where(
      [z],
      z.minimal_longitude <= ^longitude and z.maximal_longitude >= ^longitude and
        z.minimal_latitude <= ^latitude and z.maximal_latitude >= ^latitude
    )
    |> order_by(desc: :weight)
    |> limit(1)
    |> Repo.one()
  end
end
