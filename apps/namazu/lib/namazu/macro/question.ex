defmodule Namazu.Macro.Question do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Namazu.Macro.{Question, QuestionChoice, QuestionCondition, Testimony}
  alias Namazu.Repo

  @schema_prefix :macro

  schema "questions" do
    belongs_to(:form, Namazu.Macro.Form)

    has_many(:conditions, QuestionCondition)
    has_many(:question_choices, QuestionChoice)

    field(:type, :string)
    field(:multiple, :boolean)
    field(:order, :integer)
    field(:text, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(question, params \\ %{}) do
    question
    |> cast(params, [:form_id, :type, :multiple, :order, :text])

    # |> cast_assoc(:question_choices)
    # |> cast_assoc(:conditions)
    # |> validate_required([:type, :multiple, :order, :text])
  end

  def distinctive_fields() do
    [:form_id, :text]
  end

  def choices(question) do
    QuestionChoice |> where(question_id: ^question.id) |> Repo.all()
  end

  def conditions_fulfilled?(question, testimony) do
    case question.conditions do
      # The next question has no conditions
      [] ->
        true

      # We have conditions to check
      _ ->
        question_choices_ids =
          from(
            qc in QuestionCondition,
            where: qc.question_id == ^question.id,
            select: [qc.question_choice_id]
          )
          |> Repo.all()

        answers_choice_ids =
          from(
            t in Testimony,
            where: t.id == ^testimony.id,
            join: a in assoc(t, :answers),
            select: [a.question_choice_id]
          )
          |> Repo.all()

        Enum.map(question_choices_ids, &Enum.member?(answers_choice_ids, &1))
        |> Enum.all?()
    end
  end

  def get_next_question(questions, question, testimony) do
    # questions : list of questions
    # question : preloaded with conditions
    # testimony : preloaded with answers
    question_index = Enum.find_index(questions, fn a -> a.id == question.id end)

    case Enum.at(questions, question_index + 1) do
      # last question
      nil ->
        {:error, :no_question}

      next_question ->
        conditions_checked = Question.conditions_fulfilled?(next_question, testimony)

        case conditions_checked do
          true ->
            {:ok, next_question}

          false ->
            get_next_question(questions, next_question, testimony)
        end
    end
  end

  def get_previous_question(questions, question, testimony) do
    # questions : list of questions
    # question : preloaded with conditions
    # testimony : preloaded with answers
    question_index = Enum.find_index(questions, fn a -> a.id == question.id end)

    if question_index > 0 do
      case Enum.at(questions, question_index - 1) do
        previous_question ->
          conditions_checked = Question.conditions_fulfilled?(previous_question, testimony)

          case conditions_checked do
            true ->
              {:ok, previous_question}

            false ->
              get_previous_question(questions, previous_question, testimony)
          end
      end
    else
      # First question
      {:error, :no_question}
    end
  end
end
