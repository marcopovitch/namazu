defmodule Namazu.Macro.Answer do
  use Ecto.Schema
  import Ecto.Changeset

  @schema_prefix :macro

  schema "answers" do
    belongs_to(:question_choice, Namazu.Macro.QuestionChoice)
    belongs_to(:testimony, Namazu.Macro.Testimony)

    field(:value, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:question_choice_id, :testimony_id]
  end

  def changeset(answer, params \\ %{}) do
    # question_choice = params[:question_choice]

    answer
    |> cast(params, [:question_choice_id, :testimony_id, :value])

    # |> put_assoc(:testimony, params[:testimony])
    # |> put_assoc(:question_choice, params[:question_choice])
    # |> put_value(question_choice)
    # |> validate_required([:value, :question_choice, :testimony])
  end

  def put_value(changeset, question_choice) do
    # Checks if we force a value to the changeset
    value = get_field(changeset, :value)

    if is_nil(question_choice) or not is_nil(value) do
      changeset
    else
      put_change(changeset, :value, question_choice.value)
    end
  end
end
