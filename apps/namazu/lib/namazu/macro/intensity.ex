defmodule Namazu.Macro.Intensity do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :macro

  schema "intensities" do
    belongs_to(:event, Namazu.Seismic.Event)
    many_to_many(:testimonies, Namazu.Macro.Testimony, join_through: "testimonies_contributions")

    field(:intensity, :float)
    field(:intensity_lower_uncertainty, :float)
    field(:intensity_upper_uncertainty, :float)
    field(:intensity_type, :string)
    field(:method, :string)
    field(:evaluation_status, :string)
    field(:automatic, :boolean)
    field(:author, :string)
    field(:notes, :string)
    field(:quality, :float)
    field(:city_source, :string)
    field(:city_source_id, :string)
    field(:longitude, :float)
    field(:latitude, :float)
    field(:location, Geo.PostGIS.Geometry)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:event_id, :intensity, :intensity_type, :method, :automatic, :latitude, :longitude, :author]
  end

  def changeset(intensity, params \\ {}) do
    intensity
    |> cast(params, [
      :event_id,
      :intensity,
      :intensity_lower_uncertainty,
      :intensity_upper_uncertainty,
      :intensity_type,
      :method,
      :evaluation_status,
      :automatic,
      :author,
      :quality,
      :notes,
      :city_source,
      :city_source_id,
      :longitude,
      :latitude,
      :location
      # :created_at
    ])
  end
end
