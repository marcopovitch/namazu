defmodule Namazu.Macro.Testimony do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Namazu.Repo
  alias Namazu.Macro.Answer

  @schema_prefix :macro

  schema "testimonies" do
    belongs_to(:form, Namazu.Macro.Form)
    belongs_to(:event, Namazu.Seismic.Event)

    belongs_to(:preferred_intensity, Namazu.Macro.Intensity, on_replace: :update)
    has_many(:answers, Namazu.Macro.Answer)
    many_to_many(:intensities, Namazu.Macro.Intensity, join_through: "testimonies_contributions")

    field(:key, :binary_id)
    field(:naive_felt_time, :naive_datetime)
    field(:utc_offset, :integer)
    field(:quality, :float)
    field(:author, :string)
    field(:address, :string)
    field(:address_source, :string)
    field(:geocoding_type)
    field(:longitude, :float)
    field(:latitude, :float)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:key]
  end

  def changeset(testimony, params \\ %{}) do
    testimony
    |> cast(params, [
      :event_id,
      :form_id,
      :preferred_intensity_id,
      :key,
      :naive_felt_time,
      :utc_offset,
      :quality,
      :author,
      :address,
      :address_source,
      :geocoding_type,
      :longitude,
      :latitude
    ])

    # form = Map.get(params, "form", Form |> Repo.get_by(default: true))

    # event_id = Map.get(params, "event_id")

    # event =
    #   if event_id != nil and event_id != "" do
    #     Repo.get(Namazu.Seismic.Event, event_id)
    #   else
    #     nil
    #   end

    # testimony
    # |> cast(params, [:naive_felt_time, :utc_offset])
    # |> put_assoc(:address, address)
    # |> put_assoc(:form, form)
    # # Generates a key
    # |> put_change(:key, Ecto.UUID.bingenerate())
    # |> add_event(event)
    # |> Namazu.Macro.Address.validate_required_multiple([[:event], [:naive_felt_time]])
  end

  @doc """
    Takes a testimony and a question, returns the given user answer if exists
    case :
    nil -> No answer has been provided
    value -> %Namazu.Macro.Answer{...}
  """
  def get_answer_for_question(testimony, question) do
    Answer
    |> join(:left, [a], t in assoc(a, :testimony))
    |> join(:left, [a, t], qc in assoc(a, :question_choice))
    |> join(:left, [a, t, qc], q in assoc(qc, :question))
    |> where([a, t, qc, q], q.id == ^question.id)
    |> where([a, t, qc, q], t.id == ^testimony.id)
    |> Repo.one()
  end

  defp add_event(changeset, event) do
    if event do
      changeset |> put_assoc(:event, event)
    else
      changeset
    end
  end

  def get_by_key(key) do
    {:ok, binkey} = Ecto.UUID.dump(key)

    __MODULE__
    |> join(:left, [t], f in assoc(t, :form))
    |> join(:left, [t, f], q in assoc(f, :questions))
    |> where([t, f, q], t.key == ^binkey)
    |> preload([t, f, q], form: {f, questions: q})
    |> Repo.one()
  end
end
