defmodule Namazu.Macro.QuestionChoice do
  use Ecto.Schema
  import Ecto.Changeset

  @schema_prefix :macro

  schema "questions_choices" do
    belongs_to(:question, Namazu.Macro.Question)

    field(:value, :string)
    field(:order, :integer)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(question_choice, params \\ %{}) do
    question_choice
    |> cast(params, [:question_id, :value, :order])

    # |> validate_required([:order])
  end

  def distinctive_fields() do
    [:question_id, :order, :value]
  end
end
