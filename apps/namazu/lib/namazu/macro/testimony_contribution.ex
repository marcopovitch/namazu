defmodule Namazu.Macro.TestimonyContribution do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :macro

  schema "testimonies_contributions" do
    belongs_to(:testimony, Namazu.Macro.Testimony)
    belongs_to(:intensity, Namazu.Macro.Intensity)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:testimony_id, :intensity_id]
  end

  def changeset(testimony_contribution, params \\ %{}) do
    testimony_contribution
    |> cast(params, [:testimony_id, :intensity_id])
  end
end
