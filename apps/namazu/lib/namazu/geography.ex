defmodule Namazu.Geography do
  def km_per_degree(), do: 111.139

  def polygon_to_multi(geom = %Geo.MultiPolygon{}), do: geom

  def polygon_to_multi(geom = %Geo.Polygon{}) do
    %Geo.MultiPolygon{
      coordinates: [geom.coordinates],
      srid: geom.srid,
      properties: geom.properties
    }
  end
end
