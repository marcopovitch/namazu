defmodule Namazu.Instruments.Network do
  use Ecto.Schema
  import Ecto.Changeset

  @schema_prefix :instruments

  schema "networks" do
    field(:network_code, :string)
    field(:description, :string)
    field(:hidden, :boolean)
    field(:color, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(network, params \\ %{}) do
    network
    |> cast(params, [:network_code, :description])
  end

  def distinctive_fields() do
    [:network_code]
  end
end
