defmodule Namazu.Instruments.StationPeriod do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  @schema_prefix :instruments

  schema "stations_periods" do
    belongs_to(:station, Namazu.Instruments.Station)

    field(:period_start, :utc_datetime_usec)
    field(:period_end, :utc_datetime_usec)
    field(:latitude, :float)
    field(:longitude, :float)

    timestamps(type: :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:station_id, :period_start]
  end

  def format_period(period_start, period_end) do
    case period_end do
      nil ->
        "Period from #{period_start} to today"

      period_end ->
        "Period from #{period_start} to #{period_end}"
    end
  end

  def format_period(changeset = %Ecto.Changeset{}) do
    period_start = get_field(changeset, :period_start)
    period_end = get_field(changeset, :period_end)

    format_period(period_start, period_end)
  end

  def format_period(%{period_start: period_start, period_end: period_end} = _period) do
    format_period(period_start, period_end)
  end

  def add_overlapping_errors(changeset, []), do: changeset

  def add_overlapping_errors(changeset, [period | remaining_periods]) do
    changeset
    |> add_error(
      :period,
      "Trying to insert #{format_period(changeset)} but overlaps existing #{format_period(period)}",
      period_start: period.period_start,
      period_end: period.period_end
    )
    |> add_overlapping_errors(remaining_periods)
  end

  def validate_period(changeset) do
    case changeset.valid? do
      true ->
        new_period_start = get_field(changeset, :period_start)
        new_period_end = get_field(changeset, :period_end)

        overlapping_periods =
          __MODULE__
          |> Namazu.Repo.overlapping_periods(new_period_start, new_period_end)
          |> where([p], p.period_start != ^new_period_start)
          |> Namazu.Repo.station_periods(get_field(changeset, :station_id))
          |> Namazu.Repo.all()

        add_overlapping_errors(changeset, overlapping_periods)

      _ ->
        changeset
    end
  end

  def changeset(station_period, params \\ %{}) do
    station_period
    |> cast(params, [:station_id, :period_start, :period_end, :latitude, :longitude])
    |> validate_required(:station_id)
    |> validate_period()
  end
end
