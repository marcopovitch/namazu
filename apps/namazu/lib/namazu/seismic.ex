defmodule Namazu.Seismic do
  import Ecto.Query

  alias Namazu.Seismic.Event

  def event_query() do
    from(e in Event,
      as: :event,
      where: not (e.event_type in ^["not locatable", "not existing"]) or is_nil(e.event_type)
    )
  end

  def preload_preferred(query) do
    from([event: e] in query,
      join: po in assoc(e, :preferred_origin),
      as: :preferred_origin,
      join: pm in assoc(e, :preferred_magnitude),
      as: :preferred_magnitude,
      preload: [preferred_origin: po, preferred_magnitude: pm]
    )
  end

  def preload_distinctive_cities(query) do
    from(
      [event: e] in query,
      left_join: c in assoc(e, :distinctive_city),
      as: :distinctive_city,
      preload: [distinctive_city: c]
    )
  end

  def preload_preferred_origin_quality(query) do
    from(
      [preferred_origin: po] in query,
      left_join: poq in assoc(po, :quality),
      as: :preferred_origin_quality,
      preload: [preferred_origin: {po, quality: poq}]
    )
  end

  def preload_origins(query, only_preferred \\ false) do
    query =
      from(
        [event: e] in query,
        join: o in assoc(e, :origins),
        as: :origin,
        preload: [origins: o]
      )

    if only_preferred do
      query |> where([event: e, origin: o], e.preferred_origin_id == o.id)
    else
      query
    end
  end

  def preload_origins_qualities(query) do
    from(
      [origin: o] in query,
      left_join: oq in assoc(o, :quality),
      as: :origin_quality,
      preload: [origins: {o, quality: oq}]
    )
  end

  def preload_origins_uncertainties(query) do
    from(
      [origin: o] in query,
      left_join: ou in assoc(o, :uncertainty),
      as: :origin_uncertainty,
      preload: [origins: {o, uncertainty: ou}]
    )
  end

  def preload_origins_arrivals(query) do
    from(
      [origin: o] in query,
      left_join: a in assoc(o, :arrivals),
      join: p in assoc(a, :pick),
      join: s in assoc(p, :stream),
      as: :origin_arrival,
      preload: [origins: {o, arrivals: {a, pick: {p, stream: s}}}]
    )
  end

  @doc """
  Preload Origin.magnitudes association.

  Careful if only_preferred is set to true, origins will also
  be filtered.
  """
  def preload_magnitudes(query, only_preferred \\ false) do
    query =
      from(
        [origin: o] in query,
        join: m in assoc(o, :magnitudes),
        as: :magnitude,
        preload: [origins: {o, magnitudes: m}]
      )

    if only_preferred do
      query |> where([event: e, magnitude: m], e.preferred_magnitude_id == m.id)
    else
      query
    end
  end

  def preload_station_magnitude_contributions(query) do
    from(
      [origin: o, magnitude: m] in query,
      left_join: smc in assoc(m, :station_magnitude_contributions),
      as: :station_magnitude_contribution,
      preload: [origins: {o, magnitudes: {m, station_magnitude_contributions: smc}}]
    )
  end

  def preload_station_magnitudes(query) do
    from(
      [origin: o, magnitude: m, station_magnitude_contribution: smc] in query,
      left_join: sm in assoc(smc, :station_magnitude),
      as: :station_magnitude,
      preload: [
        origins:
          {o, magnitudes: {m, station_magnitude_contributions: {smc, station_magnitude: sm}}}
      ]
    )
  end

  def last_days(query, days) do
    datetime = DateTime.utc_now() |> DateTime.add(-days * 24 * 3600, :second)
    from([preferred_origin: po] in query, where: po.time >= ^datetime)
  end

  def descending_time(query) do
    from([preferred_origin: po] in query, order_by: [desc: po.time])
  end

  def ascending_time(query) do
    from([preferred_origin: po] in query, order_by: [asc: po.time])
  end

  def descending_magnitude(query) do
    from([preferred_magnitude: pm] in query, order_by: [desc: pm.magnitude])
  end

  def ascending_magnitude(query) do
    from([preferred_magnitude: pm] in query, order_by: [asc: pm.magnitude])
  end

  def start_time(nil), do: true

  def start_time(starttime) do
    dynamic([preferred_origin: po], po.time >= ^starttime)
  end

  def end_time(nil), do: true

  def end_time(endtime) do
    dynamic([preferred_origin: po], po.time <= ^endtime)
  end

  def minimal_latitude(nil), do: true

  def minimal_latitude(minimal_latitude) do
    dynamic([preferred_origin: po], po.latitude >= ^minimal_latitude)
  end

  def maximal_latitude(nil), do: true

  def maximal_latitude(maximal_latitude) do
    dynamic([preferred_origin: po], po.latitude <= ^maximal_latitude)
  end

  def minimal_longitude(nil), do: true

  def minimal_longitude(minimal_longitude) do
    dynamic([preferred_origin: po], po.longitude >= ^minimal_longitude)
  end

  def maximal_longitude(nil), do: true

  def maximal_longitude(maximal_longitude) do
    dynamic([preferred_origin: po], po.longitude <= ^maximal_longitude)
  end

  def minimal_distance(nil, nil, nil), do: true

  def minimal_distance(longitude, latitude, minimal_distance) do
    degrees = minimal_distance / 111.139

    dynamic(
      [preferred_origin: po],
      fragment(
        "public.st_distance(public.st_point(?,?),public.st_point(?,?))",
        po.longitude,
        po.latitude,
        ^latitude,
        ^longitude
      ) > ^degrees
    )
  end

  def maximal_distance(nil, nil, nil), do: true

  def maximal_distance(longitude, latitude, maximal_distance) do
    degrees = maximal_distance / 111.139

    dynamic(
      [preferred_origin: po],
      fragment(
        "public.st_distance(public.st_point(?,?),public.st_point(?,?))",
        po.longitude,
        po.latitude,
        ^longitude,
        ^latitude
      ) < ^degrees
    )
  end

  def minimal_depth(nil), do: true

  def minimal_depth(minimal_depth) do
    dynamic([preferred_origin: po], po.depth >= ^minimal_depth)
  end

  def maximal_depth(nil), do: true

  def maximal_depth(maximal_depth) do
    dynamic([preferred_origin: po], po.depth <= ^maximal_depth)
  end

  def minimal_magnitude(nil), do: true

  def minimal_magnitude(minimal_magnitude) do
    dynamic([preferred_magnitude: pm], pm.magnitude >= ^minimal_magnitude)
  end

  def maximal_magnitude(nil), do: true

  def maximal_magnitude(maximal_magnitude) do
    dynamic([preferred_magnitude: pm], pm.magnitude <= ^maximal_magnitude)
  end

  def magnitude_type(nil), do: true

  def magnitude_type(magnitude_type) do
    dynamic([preferred_magnitude: pm], pm.magnitude_type == ^magnitude_type)
  end

  def minimal_used_phase_count(nil), do: true

  def minimal_used_phase_count(used_phase_count) do
    dynamic([preferred_origin_quality: poq], poq.used_phase_count >= ^used_phase_count)
  end

  def event_types(nil), do: true
  def event_types([]), do: true

  def event_types(event_types) when is_list(event_types) do
    dynamic([event: e], e.event_type in ^event_types)
  end

  def event_types(event_type) when is_bitstring(event_type) do
    dynamic([event: e], e.event_type == ^event_type)
  end

  def event_id(nil), do: true

  def event_id(event_id) do
    dynamic([event: e], e.id == ^event_id)
  end

  def only_earthquakes(), do: event_types("earthquake")

  def only_automatic() do
    dynamic([preferred_origin: po], po.automatic == true)
  end

  def only_manual() do
    dynamic([preferred_origin: po], po.automatic == false)
  end

  def existing_and_locatable() do
    dynamic(
      [event: e],
      not (e.event_type in ^["not locatable", "not existing"]) or is_nil(e.event_type)
    )
  end
end
