defmodule Namazu.Seismic.Event do
  use Ecto.Schema

  @schema_prefix :seismic

  schema "events" do
    has_many(:origins, Namazu.Seismic.Origin)
    belongs_to(:preferred_origin, Namazu.Seismic.Origin, on_replace: :update)
    belongs_to(:preferred_magnitude, Namazu.Seismic.Magnitude, on_replace: :update)
    belongs_to(:distinctive_city, Namazu.Geography.City)
    belongs_to(:merged_with_event, __MODULE__)

    field(:event_type, :string)
    field(:timezone, :string)
    field(:distinctive_city_distance, :integer)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def namazu_to_quakeml_event_type(event_type) do
    case event_type do
      "outside of network interest" -> "other event"
      "induced" -> "induced or triggered event"
      _ -> event_type
    end
  end

  def quakeml_to_namazu_event_type(event_type) do
    case event_type do
      "other event" -> "outside of network interest"
      "induced or triggered event" -> "induced"
      _ -> event_type
    end
  end

  def seiscomp3_to_namazu_event_type(event_type) do
    case event_type do
      "other" -> "other event"
      "induced earthquake" -> "induced"
      _ -> event_type
    end
  end

  def get_localtime(event) do
    Namazu.local_datetime(event.preferred_origin.time, event.timezone)
  end

  def chunk_by_local_date(events) do
    events
    |> Enum.map(fn e -> {get_localtime(e), e} end)
    |> Enum.chunk_by(fn {l, _} -> DateTime.to_date(l) end)
  end

  def separate_events(events) do
    events
    |> Enum.group_by(fn e ->
      cond do
        e.preferred_origin.automatic -> :earthquakes_and_automatic_events
        e.event_type == "earthquake" -> :earthquakes_and_automatic_events
        e.event_type == "induced" -> :earthquakes_and_automatic_events
        true -> :other_events
      end
    end)
  end
end
