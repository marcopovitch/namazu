defmodule Namazu.Seismic.Pick do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic

  schema "picks" do
    belongs_to(:stream, Namazu.Seismic.Stream)
    has_many(:arrivals, Namazu.Seismic.Arrival)
    has_many(:amplitudes, Namazu.Seismic.Amplitude)

    field(:time, :utc_datetime_usec)
    field(:time_lower_uncertainty, :float)
    field(:time_upper_uncertainty, :float)
    field(:filter_id, :string)
    field(:method_id, :string)
    field(:phase_hint_code, :string)
    field(:phase_hint_used, :boolean)
    field(:polarity, :string)
    field(:automatic, :boolean)
    field(:agency, :string)
    field(:author, :string)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(pick, params \\ %{}) do
    pick
    |> cast(params, [
      :stream_id,
      :time,
      :time_lower_uncertainty,
      :time_upper_uncertainty,
      :filter_id,
      :method_id,
      :phase_hint_code,
      :phase_hint_used,
      :polarity,
      :automatic,
      :agency,
      :author,
      :created_at
    ])
    |> validate_required(:stream_id)
  end

  def distinctive_fields() do
    [
      :stream_id,
      :time,
      :time_lower_uncertainty,
      :time_upper_uncertainty,
      :phase_hint_used,
      :phase_hint_code,
      :automatic
    ]
  end
end
