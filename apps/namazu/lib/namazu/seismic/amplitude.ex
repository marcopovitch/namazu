defmodule Namazu.Seismic.Amplitude do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic

  schema "amplitudes" do
    belongs_to(:pick, Namazu.Seismic.Pick)
    belongs_to(:stream, Namazu.Seismic.Stream)
    has_many(:station_magnitudes, Namazu.Seismic.StationMagnitude)

    field(:amplitude, :float)
    field(:amplitude_type, :string)
    field(:time_window_reference, :utc_datetime_usec)
    field(:time_window_begin, :float)
    field(:time_window_end, :float)
    field(:time_window_used, :boolean)
    field(:period_value, :float)
    field(:period_used, :boolean)
    field(:snr, :float)
    field(:waveformid_used, :boolean)
    field(:filter_id, :string)
    field(:automatic, :boolean)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [
      :pick_id,
      :stream_id,
      :amplitude,
      :amplitude_type,
      :time_window_reference,
      :time_window_begin,
      :time_window_end
    ]
  end

  def changeset(amplitude, params \\ %{}) do
    amplitude
    |> cast(params, [
      :pick_id,
      :stream_id,
      :amplitude,
      :amplitude_type,
      :time_window_reference,
      :time_window_begin,
      :time_window_end,
      :time_window_used,
      :period_value,
      :period_used,
      :snr,
      :waveformid_used,
      :filter_id,
      :automatic,
      :created_at
    ])
  end
end
