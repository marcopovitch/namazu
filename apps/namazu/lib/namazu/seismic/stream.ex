defmodule Namazu.Seismic.Stream do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic

  schema "streams" do
    field(:network_code, :string)
    field(:station_code, :string)
    field(:location_code, :string)
    field(:channel_code, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:network_code, :station_code, :location_code, :channel_code]
  end

  def changeset(stream, params \\ %{}) do
    stream
    |> cast(params, [:network_code, :station_code, :location_code, :channel_code])
  end
end
