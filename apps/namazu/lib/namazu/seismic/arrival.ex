defmodule Namazu.Seismic.Arrival do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic

  schema "arrivals" do
    belongs_to(:origin, Namazu.Seismic.Origin)
    belongs_to(:pick, Namazu.Seismic.Pick)

    field(:phase_code, :string)
    field(:azimuth, :float)
    field(:distance, :float)
    field(:time_residual, :float)
    field(:time_weight, :float)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields(), do: [:origin_id, :pick_id]

  def changeset(arrival, params \\ %{}) do
    arrival
    |> cast(params, [
      :origin_id,
      :pick_id,
      :phase_code,
      :azimuth,
      :distance,
      :time_residual,
      :time_weight
    ])
  end
end
