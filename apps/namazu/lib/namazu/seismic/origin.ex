defmodule Namazu.Seismic.Origin do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Namazu.Repo
  alias Namazu.Seismic.Arrival

  @schema_prefix :seismic

  schema "origins" do
    belongs_to(:event, Namazu.Seismic.Event, on_replace: :update)
    has_many(:magnitudes, Namazu.Seismic.Magnitude)
    has_many(:arrivals, Namazu.Seismic.Arrival)
    has_one(:quality, Namazu.Seismic.OriginQuality)
    has_one(:uncertainty, Namazu.Seismic.OriginUncertainty)

    field(:location, Geo.PostGIS.Geometry)

    field(:time, :utc_datetime_usec)
    field(:time_uncertainty, :float)
    field(:latitude, :float)
    field(:latitude_uncertainty, :float)
    field(:longitude, :float)
    field(:longitude_uncertainty, :float)
    field(:depth, :float)
    field(:depth_uncertainty, :float)
    field(:depth_used, :boolean)
    field(:depth_type, :string)
    field(:method_id, :string)
    field(:earthmodel_id, :string)
    field(:automatic, :boolean)
    field(:evaluation_status, :string)
    field(:origin_type, :string)
    field(:agency, :string)
    field(:author, :string)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:longitude, :latitude, :time, :depth, :depth_type, :automatic]
  end

  def changeset(origin, params \\ %{}) do
    origin
    |> cast(params, [
      :event_id,
      :time,
      :time_uncertainty,
      :latitude,
      :latitude_uncertainty,
      :longitude,
      :longitude_uncertainty,
      :depth,
      :depth_used,
      :depth_type,
      :method_id,
      :earthmodel_id,
      :automatic,
      :evaluation_status,
      :origin_type,
      :agency,
      :author,
      :created_at
    ])
  end

  def get_used_arrivals(origin_id) do
    from(
      a in Arrival,
      left_join: p in assoc(a, :pick),
      left_join: s in assoc(p, :stream),
      preload: [pick: {p, stream: s}],
      where: a.origin_id == ^origin_id,
      where: a.time_weight > 0.0,
      order_by: p.time
    )
    |> Repo.all()
  end
end
