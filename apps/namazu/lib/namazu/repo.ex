defmodule Namazu.Repo do
  use Ecto.Repo, otp_app: :namazu, adapter: Ecto.Adapters.Postgres

  import Ecto.Query

  def overlapping_periods(query, period_start, period_end) do
    query
    |> where([p], fragment("? && tstzrange(?, ?)", p.period, ^period_start, ^period_end))
  end

  def station_periods(query, station_id) do
    query
    |> where([p], p.station_id == ^station_id)
  end
end
