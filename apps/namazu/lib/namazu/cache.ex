defmodule Namazu.Cache do
  alias Namazu.Seismic
  alias Namazu.Zone
  alias Namazu.Repo

  import Ecto.Query

  def get_actors() do
    ConCache.get_or_store(
      :misc,
      :actors,
      fn ->
        Namazu.Actor
        |> order_by([a], asc: a.country_iso2, asc: a.name)
        |> Repo.all()
      end
    )
  end

  def get_event(event_id) do
    ConCache.get_or_store(
      :events,
      event_id,
      fn ->
        Seismic.event_query()
        |> Seismic.preload_preferred()
        |> Seismic.preload_preferred_origin_quality()
        |> Seismic.preload_distinctive_cities()
        |> Repo.get(event_id)
      end
    )
  end

  def get_event_types() do
    ConCache.get_or_store(
      :misc,
      :event_types,
      fn ->
        Seismic.event_query()
        |> group_by([event: e], e.event_type)
        |> select([event: e], %{event_type: e.event_type, count: count(e.id)})
        |> where(
          [event: e],
          not is_nil(e.event_type) and not is_nil(e.preferred_origin_id) and
            not is_nil(e.preferred_magnitude_id)
        )
        |> order_by([event: e], desc: fragment("count"))
        |> Repo.all()
      end
    )
  end

  def get_page(locale, page_slug) do
    key = "#{locale}:#{page_slug}"
    ConCache.get_or_store(:pages, key, fn -> Web.Page.get_page(locale, page_slug) end)
  end

  def get_parameter(key) do
    ConCache.get_or_store(:parameters, key, fn -> Namazu.get_parameter(key) end)
  end

  def get_past_seismicity(event_id) do
    event = get_event(event_id)

    ConCache.get_or_store(
      :past_seismicity,
      event_id,
      fn ->
        Seismic.event_query()
        |> Seismic.preload_preferred()
        |> Seismic.preload_preferred_origin_quality()
        |> Seismic.preload_distinctive_cities()
        |> where(
          [e, po, pm, poq],
          ^dynamic(
            ^Seismic.minimal_longitude(event.preferred_origin.longitude - 2) and
              ^Seismic.maximal_longitude(event.preferred_origin.longitude + 2) and
              ^Seismic.minimal_latitude(event.preferred_origin.latitude - 2) and
              ^Seismic.maximal_latitude(event.preferred_origin.latitude + 2)
          )
        )
        |> order_by([e, po, pm, poq], desc: pm.magnitude)
        |> limit(50)
        |> Repo.all()
      end
    )
  end

  def get_stations_by_zone(zone) do
    ConCache.get_or_store(
      :stations,
      zone.slug,
      fn ->
        zone
        |> Zone.get_stations()
        |> Repo.all()
      end
    )
  end
end
