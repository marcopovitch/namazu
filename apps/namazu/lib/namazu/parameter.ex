defmodule Namazu.Parameter do
  use Ecto.Schema

  @schema_prefix :namazu

  @primary_key {:key, :string, autogenarate: false}

  schema "parameters" do
    field(:value, :string)
  end
end
