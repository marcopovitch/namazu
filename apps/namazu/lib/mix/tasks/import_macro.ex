defmodule Mix.Tasks.Namazu.ImportMacro do
  use Mix.Task
  use Timex

  import Ecto.Query

  alias Namazu.Macro.Answer
  alias Namazu.Macro.Form
  alias Namazu.Macro.Intensity
  alias Namazu.Macro.Question
  alias Namazu.Macro.QuestionChoice
  alias Namazu.Macro.Testimony
  alias Namazu.Macro.TestimonyContribution
  alias Namazu.Repo
  alias Namazu.Seismic

  @shortdoc "Import macro data from a JSON file"

  def parse_file(file) do
    with {:ok, binary} = File.read(file),
         {:ok, data} = Poison.decode(binary),
         do: data
  end

  def cast_event(event) do
    {:ok, datetime, _} = DateTime.from_iso8601(event["time"])

    %{
      id: event["id"],
      longitude: event["longitude"],
      latitude: event["latitude"],
      magnitude: event["Ml"],
      time: datetime
    }
  end

  def schema_to_kw(event) do
    %{
      longitude: event.preferred_origin.longitude,
      latitude: event.preferred_origin.latitude,
      time: event.preferred_origin.time,
      magnitude: event.preferred_magnitude.magnitude
    }
  end

  def match_event(
        %{
          longitude: longitude,
          latitude: latitude,
          magnitude: magnitude,
          time: datetime
        },
        max_distance \\ 150
      ) do
    Seismic.event_query()
    |> Seismic.preload_preferred()
    |> where(
      [preferred_origin: po, preferred_magnitude: pm],
      ^dynamic(
        ^Seismic.start_time(Timex.shift(datetime, seconds: -80)) and
          ^Seismic.end_time(Timex.shift(datetime, seconds: 80)) and
          ^Seismic.maximal_distance(longitude, latitude, max_distance) and
          ^Seismic.minimal_magnitude(magnitude - 0.9) and
          ^Seismic.maximal_magnitude(magnitude + 0.9)
      )
    )
    |> Repo.all()
  end

  def get_events_mapping(events) do
    events
    |> Enum.filter(fn event -> not is_nil(event["Ml"]) end)
    |> Enum.map(fn event ->
      event = cast_event(event)

      case match_event(event) do
        [] ->
          nil

        matches when length(matches) == 1 ->
          {event.id, List.first(matches).id}

        matches ->
          sort_by_time = fn match ->
            abs(DateTime.diff(event.time, match.preferred_origin.time))
          end

          match =
            matches
            |> Enum.sort_by(&sort_by_time.(&1))
            |> List.first()

          {event.id, match.id}
      end
    end)
    |> Enum.reject(&is_nil(&1))
    |> Map.new()
  end

  def import_forms(forms) do
    forms
    |> Enum.map(fn f ->
      {:ok, form} =
        %Form{
          name: f["name"],
          type: f["type"],
          default: f["default"]
        }
        |> Map.from_struct()
        |> Namazu.upsert(Form)

      choices =
        f["questions"]
        |> Enum.map(fn q ->
          {:ok, question} =
            %Question{
              form_id: form.id,
              type: q["type"],
              multiple: q["multiple"],
              order: q["order"],
              text: q["text"]
            }
            |> Map.from_struct()
            |> Namazu.upsert(Question)

          q["question_choices"]
          |> Enum.map(fn c ->
            {:ok, choice} =
              %QuestionChoice{
                question_id: question.id,
                order: c["order"],
                value: c["value"]
              }
              |> Map.from_struct()
              |> Namazu.upsert(QuestionChoice)

            {c["id"], choice.id}
          end)
        end)
        |> List.flatten()
        |> Map.new()

      {f["id"], %{id: form.id, choices: choices}}
    end)
    |> Map.new()
  end

  def import_testimonies(testimonies, events_mapping, forms_mapping) do
    testimonies
    |> Enum.map(fn t ->
      naive_felt_time =
        case t["naive_felt_time"] do
          nil ->
            nil

          _datetime ->
            {:ok, datetime, offset} = DateTime.from_iso8601(t["naive_felt_time"])
            Timex.shift(datetime, seconds: offset)
        end

      old_form_id = t["form_id"]
      new_form_id = forms_mapping[old_form_id].id

      quality =
        case t["quality"] do
          "fiable" -> 1
          "moyennement fiable" -> 0.5
          "peu fiable" -> 0.1
        end

      {:ok, testimony} =
        %Testimony{
          event_id: events_mapping[t["event_id"]],
          form_id: new_form_id,
          key: t["key"],
          naive_felt_time: naive_felt_time,
          quality: quality,
          address: t["address"]["street"],
          address_source: t["address"]["address_source"],
          geocoding_type: t["address"]["geocoding_type"],
          longitude: t["address"]["longitude"],
          latitude: t["address"]["latitude"]
        }
        |> Map.from_struct()
        |> Namazu.upsert(Testimony)

      t["answers"]
      |> Enum.each(fn a ->
        %Answer{
          question_choice_id: forms_mapping[old_form_id][:choices][a["question_choice_id"]],
          testimony_id: testimony.id,
          value: a["value"]
        }
        |> Map.from_struct()
        |> Namazu.upsert(Answer)
      end)

      {t["id"], testimony.id}
    end)
    |> Map.new()
  end

  def import_intensities(intensities, testimonies_mapping, events_mapping) do
    intensities
    |> Enum.each(fn i ->
      testimony_id =
        case Map.get(i, "testimony_id", nil) do
          nil ->
            nil

          testimony_id ->
            testimonies_mapping[testimony_id]
        end

      event_id =
        case Map.get(i, "event_id", nil) do
          nil ->
            nil

          event_id ->
            events_mapping[event_id]
        end

      [city_source, city_source_id] =
        case Map.get(i, "code_insee", nil) do
          nil ->
            [nil, nil]

          code_insee ->
            ["INSEE", code_insee]
        end

      latitude = i["latitude"]
      longitude = i["longitude"]
      intensity = i["value"]
      intensity_type = i["type"]
      method = Map.get(i, "source", "non renseigné")
      author = i["author"]
      automatic = i["automatic"]

      quality =
        case i["quality"] do
          nil -> nil
          "sûr" -> 1
          "moyennement sûr" -> 0.5
          "peu sûr" -> 0.1
        end

      filters = [
        latitude: latitude,
        longitude: longitude,
        intensity: intensity,
        intensity_type: intensity_type,
        method: method,
        automatic: automatic
      ]

      query =
        case testimony_id do
          nil ->
            from(i in Intensity,
              select: i
            )

          _testimony_id ->
            from(i in Intensity,
              join: c in TestimonyContribution,
              where: c.intensity_id == i.id,
              where: c.testimony_id == ^i["testimony_id"],
              select: i
            )
        end

      query =
        case event_id do
          nil ->
            from(i in query,
              where: is_nil(i.event_id)
            )

          event_id ->
            from(i in query,
              where: i.event_id == ^event_id
            )
        end

      matched_intensity =
        Repo.one(
          from(i in query,
            where: ^filters
          )
        )

      map = %{
        event_id: event_id,
        intensity: intensity,
        intensity_type: intensity_type,
        method: method,
        # evaluation_status: i["???"],
        automatic: automatic,
        author: author,
        latitude: latitude,
        longitude: longitude,
        notes: i["notes"],
        city_source: city_source,
        city_source_id: city_source_id,
        quality: quality
      }

      Repo.transaction(fn ->
        intensity =
          case matched_intensity do
            nil ->
              %Intensity{}
              |> Intensity.changeset(map)
              |> Repo.insert!()

            matched_intensity ->
              matched_intensity
              |> Intensity.changeset(map)
              |> Repo.update!()
          end

        if testimony_id do
          %TestimonyContribution{
            intensity_id: intensity.id,
            testimony_id: testimony_id
          }
          |> Map.from_struct()
          |> Namazu.upsert(TestimonyContribution)

          # Finally set the intensity as preferred
          Testimony
          |> Repo.get!(testimony_id)
          |> Testimony.changeset(%{preferred_intensity_id: intensity.id})
          |> Repo.update!()
        end
      end)
    end)
  end

  def run(file) do
    Mix.Task.run("app.start")

    data = parse_file(file)
    events_mapping = get_events_mapping(data["events"])
    forms_mapping = import_forms(data["forms"])
    testimonies_mapping = import_testimonies(data["testimonies"], events_mapping, forms_mapping)
    import_intensities(data["intensities"], testimonies_mapping, events_mapping)

    data
  end
end
