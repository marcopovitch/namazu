defmodule Mix.Tasks.Namazu.ImportTimezones do
  use Mix.Task
  use Timex

  alias Namazu.Geography.Timezone
  alias Namazu.Repo
  alias Namazu.Geography

  @shortdoc "Import timezones data from a JSON file"

  def parse_feature(feature) do
    %Timezone{
      timezone_id: feature["properties"]["timezone_id"],
      boundary: Geo.JSON.decode!(feature["geometry"]) |> Geography.polygon_to_multi()
    }
  end

  def run(file) do
    Mix.Task.run("app.start")

    data =
      File.read!(file)
      |> Poison.decode!()

    data["features"]
    |> Enum.map(&parse_feature(&1))
    |> Enum.map(&Timezone.changeset(&1))
    |> Enum.map(&Repo.insert_or_update!(&1))
  end
end
