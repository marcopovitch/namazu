defmodule Mix.Tasks.Namazu.ImportPage do
  use Mix.Task
  # use Timex

  alias Web.Page

  @shortdoc "Import a page from a Markdown file"

  def run(args) do
    Mix.Task.run("app.start")

    arguments = [locale: :string, slug: :string, title: :string]

    {options, args, _} = OptionParser.parse(args, strict: arguments)
    [locale: locale, slug: slug, title: title] = options

    content =
      args
      |> List.first()
      |> File.read!()

    %{
      locale: locale,
      slug: slug,
      title: title,
      content: content,
      published: true
    }
    |> Namazu.upsert(Web.Page)
  end
end
