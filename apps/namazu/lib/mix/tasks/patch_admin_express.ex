defmodule Mix.Tasks.Namazu.PatchAdminExpress do
  use Mix.Task
  use Timex

  @shortdoc "Patch AdminExpress json file with chef-lieux"

  def run(args) do
    [cities_file_path, chef_lieux_file_path] = args

    chef_lieux_coordinates =
      File.read!(chef_lieux_file_path)
      |> Poison.decode!()
      |> Map.get("features")
      |> Enum.map(fn f ->
        {f["properties"]["administrative_code"], f["geometry"]["coordinates"]}
      end)
      |> Enum.into(%{})

    cities =
      File.read!(cities_file_path)
      |> Poison.decode!()

    updated_features =
      cities
      |> Map.get("features")
      |> Enum.map(fn feature ->
        [longitude, latitude] =
          chef_lieux_coordinates[feature["properties"]["administrative_code"]]

        properties =
          feature["properties"]
          |> Map.put("longitude", longitude)
          |> Map.put("latitude", latitude)
          |> Map.put("location_type", "chef-lieu")

        feature
        |> Map.put("properties", properties)
      end)

    cities
    |> Map.put("features", updated_features)
    |> Poison.encode!()
    |> IO.write()
  end
end
