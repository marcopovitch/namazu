defmodule Mix.Tasks.Namazu.ImportCountries do
  use Mix.Task
  use Timex

  alias Namazu.Geography

  @shortdoc "Import countries data from a JSON file"

  def upsert(feature) do
    params = %{
      code_iso2: feature["properties"]["code_iso2"],
      code_iso3: feature["properties"]["code_iso3"],
      source: feature["properties"]["source"],
      source_version: feature["properties"]["source_version"],
      source_id: feature["properties"]["source_id"],
      name: feature["properties"]["name"],
      name_ar: feature["properties"]["name_ar"],
      name_bn: feature["properties"]["name_bn"],
      name_de: feature["properties"]["name_de"],
      name_el: feature["properties"]["name_el"],
      name_en: feature["properties"]["name_en"],
      name_es: feature["properties"]["name_es"],
      name_fr: feature["properties"]["name_fr"],
      name_hi: feature["properties"]["name_hi"],
      name_hu: feature["properties"]["name_hu"],
      name_id: feature["properties"]["name_id"],
      name_it: feature["properties"]["name_it"],
      name_ja: feature["properties"]["name_ja"],
      name_ko: feature["properties"]["name_ko"],
      name_nl: feature["properties"]["name_nl"],
      name_pl: feature["properties"]["name_pl"],
      name_pt: feature["properties"]["name_pt"],
      name_ru: feature["properties"]["name_ru"],
      name_sv: feature["properties"]["name_sv"],
      name_tr: feature["properties"]["name_tr"],
      name_vi: feature["properties"]["name_vi"],
      name_zh: feature["properties"]["name_zh"],
      boundary: Geo.JSON.decode!(feature["geometry"]) |> Geography.polygon_to_multi()
    }

    Namazu.upsert(params, Geography.Country)
  end

  def run(file) do
    Mix.Task.run("app.start")

    File.read!(file)
    |> Poison.decode!()
    |> Map.get("features")
    |> Enum.map(&upsert(&1))
  end
end
