defmodule Namazu do
  use Timex

  import Ecto.Query

  alias Namazu.Cache

  require Logger

  @moduledoc """
  Documentation for Namazu.
  """

  @key_ignore_list [
    :__struct__,
    :__meta__,
    :id,
    :origin_id,
    :created_at,
    :inserted_at,
    :updated_at
  ]

  @spec _nullify(map(), []) :: map()
  defp _nullify(map, []), do: map

  defp _nullify(map, [key | remaining_keys]) do
    case Map.get(map, key) do
      "" -> Map.put(map, key, nil)
      _ -> _nullify(map, remaining_keys)
    end
  end

  @spec nullify(map()) :: map()
  def nullify(map), do: _nullify(map, Map.keys(map))

  defp build_query(query, []), do: query

  defp build_query(query, [{field, value} | remaining_fields]) do
    if is_nil(value) and String.ends_with?(Atom.to_string(field), "_id") do
      raise "Can't try to match without #{field} set."
    end

    case is_nil(value) do
      true ->
        where(query, [e], is_nil(field(e, ^field)))

      false ->
        where(query, [e], field(e, ^field) == ^value)
    end
    |> build_query(remaining_fields)
  end

  def match(map, matchable) do
    map = nullify(map)

    fields_to_match =
      matchable.distinctive_fields()
      |> Enum.map(fn field -> {field, Map.get(map, field)} end)

    build_query(matchable, fields_to_match)
    |> Namazu.Repo.one()
  end

  def upsert(map, matchable) do
    map = nullify(map)

    case Namazu.match(map, matchable) do
      nil ->
        changeset =
          matchable
          |> struct
          |> matchable.changeset(map)

        case changeset.valid? do
          true ->
            changeset
            |> Namazu.Repo.insert()

          _ ->
            {:error, changeset}
        end

      match ->
        match
        |> matchable.changeset(map)
        |> Namazu.Repo.update()
    end
  end

  def local_datetime(datetime, timezone) do
    timex_timezone = Timezone.get(timezone)
    Timezone.convert(datetime, timex_timezone)
  end

  def to_string(entry) do
    id =
      case Map.get(entry, :id) do
        nil ->
          Map.get(entry, :origin_id)

        id ->
          id
      end

    name =
      case Map.has_key?(entry, :__struct__) do
        true ->
          entry.__struct__
          |> Atom.to_string()
          |> String.split(".")
          |> List.last()

        false ->
          nil
      end

    values =
      entry
      |> Map.keys()
      |> Enum.filter(fn key -> key not in @key_ignore_list end)
      |> Enum.map(fn key ->
        value = Map.get(entry, key)

        value_str =
          case value do
            %{__cardinality__: _} ->
              "<not_printed>"

            %{__calendar__: _} ->
              DateTime.to_string(value)

            _ ->
              value
          end

        "#{key}:#{value_str}"
      end)
      |> Enum.join("/")

    case [name, id] do
      [nil, nil] ->
        "[#{values}]"

      [name, id] ->
        "#{name}##{id}[#{values}]"
    end
  end

  def timezone_from_coordinate(latitude, longitude) do
    from(t in Namazu.Geography.Timezone,
      where:
        fragment(
          "ST_Within(ST_SetSRID(ST_MakePoint(?,?),4326), ?)",
          ^longitude,
          ^latitude,
          t.boundary
        )
    )
  end

  def get_parameter(key) do
    Namazu.Parameter
    |> Namazu.Repo.get(key)
    |> Map.get(:value)
  end

  def get_site_description(), do: Cache.get_parameter("site_description")

  def get_contact_phone(), do: Cache.get_parameter("contact_phone")

  def get_contact_email(), do: Cache.get_parameter("contact_email")

  def get_contact_address(), do: Cache.get_parameter("contact_address")

  def get_contact_address_longitude(),
    do: Cache.get_parameter("contact_address_longitude") |> Web.FDSNEventController.parse_float()

  def get_contact_address_latitude(),
    do: Cache.get_parameter("contact_address_latitude") |> Web.FDSNEventController.parse_float()

  def get_matomo_host(), do: Application.get_env(:web, Web.Endpoint)[:matomo_host]

  case Application.get_env(:web, Web.Endpoint)[:matomo_host] do
    "" -> nil
    nil -> nil
    host -> host
  end
end
