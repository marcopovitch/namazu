defmodule Namazu.Mixfile do
  use Mix.Project

  def project do
    [
      app: :namazu,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :timex],
      mod: {Namazu.Application, []}
    ]
  end

  defp deps do
    [
      {:ecto_sql, "~>3.0"},
      {:geo_postgis, "~> 2.0"},
      {:poison, "~> 3.1"},
      {:postgrex, ">= 0.0.0"},
      {:timex, "~> 3.5"},
      {:con_cache, "~> 0.14"}
    ]
  end
end
