defmodule Seiscomp3 do
  import Ecto.Query

  require Logger

  @spec import_entries(list, Seiscomp3.Syncer, integer) :: {:ok, integer}
  def import_entries(list, syncer, count \\ 0)
  def import_entries([], _, count), do: {:ok, count}

  def import_entries([first | remaining], syncer, count) do
    result =
      Namazu.Repo.transaction(fn ->
        try do
          state =
            case Seiscomp3.State
                 |> lock("FOR UPDATE NOWAIT")
                 |> Namazu.Repo.get(syncer.state_key) do
              nil -> %Seiscomp3.State{table_name: syncer.state_key}
              state -> state
            end

          case syncer.import_entry(first) do
            {:ok, _imported, last_modified, oid} ->
              Seiscomp3.State.set_last_modified(state, last_modified, oid)

            {:error, changeset} ->
              changeset
              |> Ecto.Changeset.traverse_errors(fn {msg, _} -> Logger.error(msg) end)

              Namazu.Repo.rollback({:error, changeset.data, count})

            {:missing_match, map} ->
              Logger.info("Missing match : #{Namazu.to_string(map)}")
              Namazu.Repo.rollback({:missing_match, map, count})
          end
        rescue
          _error ->
            Logger.info("Table #{syncer.state_key} is already be synced by another worker.")
            # Returns {:ok, 0} to go to the next syncer
            {:ok, 0}
        end
      end)

    case result do
      {:ok, _} -> import_entries(remaining, syncer, count + 1)
      {:error, _} -> {:error, count}
    end
  end

  @spec filter_on_state(Ecto.Query, Seiscomp3.Syncer) :: %Ecto.Query{}
  def filter_on_state(query, syncer) do
    case Seiscomp3.State.get_last_modified(syncer.state_key()) do
      nil ->
        query

      state ->
        query
        |> where(
          [z],
          fragment("(?, ?)", z._last_modified, z._oid) >
            fragment("(?, ?)", ^state.last_modified, ^state.last_oid)
        )
    end
  end

  @spec execute_syncer(Seiscomp3.Syncer) :: :ok
  def execute_syncer(syncer) do
    {result, count} =
      syncer.query
      |> filter_on_state(syncer)
      |> limit(1000)
      |> Seiscomp3.Repo.all()
      |> import_entries(syncer)

    case [result == :ok, count > 0] do
      [true, true] ->
        execute_syncer(syncer)

      _ ->
        :ok
    end
  end
end
