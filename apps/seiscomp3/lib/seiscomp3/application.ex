defmodule Seiscomp3.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: true

    children =
      case [System.get_env("SC3_DATABASE"), Mix.env()] do
        [nil, _] -> []
        ["", _] -> []
        [_, :prod] -> [supervisor(Seiscomp3.Repo, []), worker(Seiscomp3.Process, [])]
        [_, _] -> [supervisor(Seiscomp3.Repo, [])]
      end

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Seiscomp3.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
