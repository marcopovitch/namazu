defmodule Seiscomp3.Data.Events do
  @behaviour Seiscomp3.Syncer

  require Logger

  import Ecto.Query

  alias Namazu.Seismic.Event
  alias Namazu.Seismic.Magnitude
  alias Namazu.Seismic.Origin

  def state_key, do: "event"

  def query do
    from(
      e in "event",
      join: pe in "publicobject",
      on: [_oid: e._oid],
      join: po in "publicobject",
      on: [m_publicid: e.m_preferredoriginid],
      join: o in "origin",
      on: [_oid: po._oid],
      join: pm in "publicobject",
      on: [m_publicid: e.m_preferredmagnitudeid],
      join: m in "magnitude",
      on: [_oid: pm._oid],
      order_by: [e._last_modified, e._oid],
      select: %{
        mapping: %{
          event_oid: e._oid,
          event_publicid: pe.m_publicid
        },
        event: %{
          event_type: e.m_type
        },
        preferred_origin: %{
          time:
            fragment("(?||'.'||?)::timestamp with time zone", o.m_time_value, o.m_time_value_ms),
          longitude: o.m_longitude_value,
          latitude: o.m_latitude_value,
          depth: o.m_depth_value,
          depth_type: o.m_depthtype,
          automatic: o.m_evaluationmode == "automatic",
          origin_type: o.m_type
        },
        preferred_magnitude: %{
          magnitude: m.m_magnitude_value,
          magnitude_uncertainty: m.m_magnitude_uncertainty,
          magnitude_type: m.m_type,
          method_id: m.m_methodid
        },
        last_modified: e._last_modified,
        oid: e._oid
      }
    )
  end

  defp fix_event_type(event_map) do
    %{event_map | event_type: Event.seiscomp3_to_namazu_event_type(event_map.event_type)}
  end

  def prepare(event), do: fix_event_type(event)

  def import_entry(entry) do
    unless Namazu.Repo.in_transaction?(), do: raise("Can't be run outside a transaction")

    last_modified = Seiscomp3.Time.cast_time(entry.last_modified)
    magnitude_map = entry.preferred_magnitude
    origin_map = Seiscomp3.Data.Origins.prepare(entry.preferred_origin)
    event_map = prepare(entry.event)
    mapping_map = entry.mapping

    event_map =
      case event_map do
        %{event_type: "other"} -> %{event_map | event_type: "other event"}
        %{event_type: "induced earthquake"} -> %{event_map | event_type: "induced"}
        _ -> event_map
      end

    preferred_origin = Namazu.match(origin_map, Origin)

    if preferred_origin do
      magnitude_map = Map.put(magnitude_map, :origin_id, preferred_origin.id)
      preferred_magnitude = Namazu.match(magnitude_map, Magnitude)

      if preferred_magnitude do
        [_, event] = Seiscomp3.Mapping.upsert!(mapping_map, event_map)

        event =
          event
          |> Ecto.Changeset.change(%{
            preferred_origin_id: preferred_origin.id,
            preferred_magnitude_id: preferred_magnitude.id,
            event_type: event_map.event_type
          })
          |> Namazu.Repo.update!()

        Logger.info("Importing : #{Namazu.to_string(event)}")

        {:ok, event, last_modified, entry.oid}
      else
        {:missing_match, magnitude_map}
      end
    else
      {:missing_match, origin_map}
    end
  end
end
