defmodule Seiscomp3.Data.OriginsReferences do
  @behaviour Seiscomp3.Syncer

  import Ecto.Query

  require Logger

  def state_key, do: "originreference"

  def query do
    from(
      r in "originreference",
      join: e in "event",
      on: [_oid: r._parent_oid],
      join: p in "publicobject",
      on: [m_publicid: r.m_originid],
      join: o in "origin",
      on: [_oid: p._oid],
      order_by: [r._last_modified, r._oid],
      select: %{
        origin: %{
          time:
            fragment("(?||'.'||?)::timestamp with time zone", o.m_time_value, o.m_time_value_ms),
          latitude: o.m_latitude_value,
          longitude: o.m_longitude_value,
          depth: o.m_depth_value,
          depth_type: o.m_depthtype,
          origin_type: o.m_type,
          automatic: o.m_evaluationmode == "automatic"
        },
        event_oid: e._oid,
        last_modified: r._last_modified,
        oid: r._oid
      }
    )
  end

  def import_entry(entry) do
    last_modified = Seiscomp3.Time.cast_time(entry.last_modified)
    mapping = Namazu.Repo.get_by(Seiscomp3.Mapping, event_oid: entry.event_oid)

    case mapping do
      nil ->
        {:missing_match, %Seiscomp3.Mapping{event_oid: entry.event_oid}}

      mapping ->
        origin =
          entry.origin
          |> Seiscomp3.Data.Origins.prepare()
          |> Namazu.match(Namazu.Seismic.Origin)

        case origin do
          nil ->
            {:missing_match, entry.origin}

          origin ->
            if origin.event_id != mapping.event_id do
              Logger.info(
                "Updating : #{Namazu.to_string(origin)} (setting event_id to #{mapping.event_id})"
              )

              origin
              |> Namazu.Seismic.Origin.changeset(%{event_id: mapping.event_id})
              |> Namazu.Repo.update!()
            end

            {:ok, origin, last_modified, entry.oid}
        end
    end
  end
end
