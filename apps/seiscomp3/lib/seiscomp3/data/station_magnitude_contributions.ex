defmodule Seiscomp3.Data.StationMagnitudeContributions do
  import Ecto.Query

  def query do
    from(
      smc in "stationmagnitudecontribution",
      join: psm in "publicobject",
      on: [m_publicid: smc.m_stationmagnitudeid],
      join: sm in "stationmagnitude",
      on: [_oid: psm._oid],
      join: pa in "publicobject",
      on: [m_publicid: sm.m_amplitudeid],
      join: a in "amplitude",
      on: [_oid: pa._oid],
      join: pp in "publicobject",
      on: [m_publicid: a.m_pickid],
      join: p in "pick",
      on: [_oid: pp._oid],
      select: %{
        station_magnitude_contribution: %{
          residual: smc.m_residual,
          weight: smc.m_weight
        },
        station_magnitude: %{
          magnitude: sm.m_magnitude_value,
          magnitude_type: sm.m_type,
          created_at: sm.m_creationinfo_creationtime
        },
        station_magnitude_stream: %{
          network_code: sm.m_waveformid_networkcode,
          station_code: sm.m_waveformid_stationcode,
          location_code: sm.m_waveformid_locationcode,
          channel_code: sm.m_waveformid_channelcode
        },
        amplitude: %{
          amplitude: a.m_amplitude_value,
          amplitude_type: a.m_type,
          time_window_reference:
            fragment(
              "(?||'.'||?)::timestamp with time zone",
              a.m_timewindow_reference,
              a.m_timewindow_reference_ms
            ),
          time_window_begin: a.m_timewindow_begin,
          time_window_end: a.m_timewindow_end,
          time_window_used: a.m_timewindow_used,
          period_value: a.m_period_value,
          period_used: a.m_period_used,
          snr: a.m_snr,
          waveformid_used: a.m_waveformid_used,
          filter_id: a.m_filterid,
          automatic: a.m_evaluationmode == "automatic",
          created_at: a.m_creationinfo_creationtime
        },
        amplitude_stream: %{
          network_code: a.m_waveformid_networkcode,
          station_code: a.m_waveformid_stationcode,
          location_code: a.m_waveformid_locationcode,
          channel_code: a.m_waveformid_channelcode
        },
        pick: %{
          time:
            fragment("(?||'.'||?)::timestamp with time zone", p.m_time_value, p.m_time_value_ms),
          time_lower_uncertainty: p.m_time_loweruncertainty,
          time_upper_uncertainty: p.m_time_upperuncertainty,
          filter_id: p.m_filterid,
          method_id: p.m_methodid,
          phase_hint_code: p.m_phasehint_code,
          phase_hint_used: p.m_phasehint_used,
          polarity: p.m_polarity,
          automatic: p.m_evaluationmode == "automatic",
          agency: p.m_creationinfo_agencyid,
          author: p.m_creationinfo_author,
          created_at: p.m_creationinfo_creationtime
        },
        pick_stream: %{
          network_code: p.m_waveformid_networkcode,
          station_code: p.m_waveformid_stationcode,
          location_code: p.m_waveformid_locationcode,
          channel_code: p.m_waveformid_channelcode
        }
      }
    )
  end
end
