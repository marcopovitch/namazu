defmodule Seiscomp3.Data.Networks do
  @behaviour Seiscomp3.Syncer

  require Logger

  import Ecto.Query

  alias Namazu.Instruments.Network

  def state_key, do: "network"

  def query do
    from(
      n in "network",
      order_by: [n._last_modified, n._oid],
      select: %{
        network: %{
          network_code: n.m_code,
          description: n.m_description
        },
        last_modified: n._last_modified,
        oid: n._oid
      }
    )
  end

  def import_entry(entry) do
    last_modified = Seiscomp3.Time.cast_time(entry.last_modified)

    case Namazu.upsert(entry.network, Network) do
      {:ok, network} ->
        Logger.info("Importing : #{Namazu.to_string(network)}")
        {:ok, network, last_modified, entry.oid}

      {:error, changeset} ->
        {:error, changeset}
    end
  end
end
