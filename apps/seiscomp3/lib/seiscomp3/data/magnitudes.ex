defmodule Seiscomp3.Data.Magnitudes do
  @behaviour Seiscomp3.Syncer

  require Logger

  import Ecto.Query

  alias Namazu.Seismic.Amplitude
  alias Namazu.Seismic.Origin
  alias Namazu.Seismic.Magnitude
  alias Namazu.Seismic.Pick
  alias Namazu.Seismic.StationMagnitude
  alias Namazu.Seismic.StationMagnitudeContribution
  alias Namazu.Seismic.Stream

  def state_key, do: "magnitude"

  def query do
    from(
      m in "magnitude",
      join: o in "origin",
      on: [_oid: m._parent_oid],
      join: po in "publicobject",
      on: [_oid: o._oid],
      join: r in "originreference",
      on: [m_originid: po.m_publicid],
      join: e in "event",
      on: [_oid: r._parent_oid],
      order_by: [m._last_modified, m._oid],
      select: %{
        origin: %{
          time:
            fragment("(?||'.'||?)::timestamp with time zone", o.m_time_value, o.m_time_value_ms),
          longitude: o.m_longitude_value,
          latitude: o.m_latitude_value,
          depth: o.m_depth_value,
          depth_type: o.m_depthtype,
          automatic: o.m_evaluationmode == "automatic",
          origin_type: o.m_type
        },
        magnitude: %{
          magnitude: m.m_magnitude_value,
          magnitude_uncertainty: m.m_magnitude_uncertainty,
          magnitude_type: m.m_type,
          method_id: m.m_methodid,
          station_count: m.m_stationcount,
          evaluation_status: m.m_evaluationstatus,
          agency: m.m_creationinfo_agencyid,
          author: m.m_creationinfo_author,
          created_at: m.m_creationinfo_creationtime
        },
        last_modified: m._last_modified,
        oid: m._oid
      }
    )
  end

  def import_entry(entry) do
    unless Namazu.Repo.in_transaction?(), do: raise("Can't be run outside a transaction")

    last_modified = Seiscomp3.Time.cast_time(entry.last_modified)

    origin_map = Seiscomp3.Data.Origins.prepare(entry.origin)
    magnitude_map = Seiscomp3.Time.cast(entry.magnitude)

    case Namazu.match(origin_map, Origin) do
      nil ->
        {:missing_match, origin_map}

      origin ->
        {:ok, magnitude} =
          magnitude_map
          |> Map.put(:origin_id, origin.id)
          |> Namazu.upsert(Magnitude)

        Logger.info("Importing : #{Namazu.to_string(magnitude)}")

        # Get station magnitude contributions, amplitudes and picks
        Seiscomp3.Data.StationMagnitudeContributions.query()
        |> where([smc], smc._parent_oid == ^entry.oid)
        |> Seiscomp3.Repo.all()
        |> Enum.map(fn row ->
          {:ok, amplitude_stream} = Namazu.upsert(row.amplitude_stream, Stream)
          {:ok, station_magnitude_stream} = Namazu.upsert(row.station_magnitude_stream, Stream)
          {:ok, pick_stream} = Namazu.upsert(row.pick_stream, Stream)

          {:ok, pick} =
            row.pick
            |> Map.put(:stream_id, pick_stream.id)
            # |> Seiscomp3.Time.cast()
            |> Namazu.upsert(Pick)

          Logger.info("Importing : #{Namazu.to_string(pick)}")

          {:ok, amplitude} =
            row.amplitude
            # |> Seiscomp3.Time.cast()
            |> Map.put(:pick_id, pick.id)
            |> Map.put(:stream_id, amplitude_stream.id)
            |> Namazu.upsert(Amplitude)

          Logger.info("Importing : #{Namazu.to_string(amplitude)}")

          {:ok, station_magnitude} =
            row.station_magnitude
            |> Seiscomp3.Time.cast()
            |> Map.put(:amplitude_id, amplitude.id)
            |> Map.put(:stream_id, station_magnitude_stream.id)
            |> Namazu.upsert(StationMagnitude)

          Logger.info("Importing : #{Namazu.to_string(station_magnitude)}")

          {:ok, station_magnitude_contribution} =
            row.station_magnitude_contribution
            |> Map.put(:magnitude_id, magnitude.id)
            |> Map.put(:station_magnitude_id, station_magnitude.id)
            |> Namazu.upsert(StationMagnitudeContribution)

          Logger.info("Importing : #{Namazu.to_string(station_magnitude_contribution)}")
        end)

        {:ok, magnitude, last_modified, entry.oid}
    end
  end
end
