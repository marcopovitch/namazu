defmodule Seiscomp3.Data.Stations do
  @behaviour Seiscomp3.Syncer

  require Logger

  import Ecto.Query
  import Ecto.Changeset

  alias Namazu.Instruments.Station
  alias Namazu.Instruments.StationPeriod
  alias Namazu.Instruments.Network

  def state_key, do: "station"

  def query do
    from(
      s in "station",
      join: n in "network",
      on: [_oid: s._parent_oid],
      order_by: [s._last_modified, s._oid],
      select: %{
        network: %{
          network_code: n.m_code
        },
        station: %{
          station_code: s.m_code
        },
        station_period: %{
          period_start: s.m_start,
          period_end: s.m_end,
          latitude: s.m_latitude,
          longitude: s.m_longitude
        },
        last_modified: s._last_modified,
        oid: s._oid
      }
    )
  end

  def import_entry(entry) do
    unless Namazu.Repo.in_transaction?(), do: raise("Can't be run outside a transaction")

    last_modified = Seiscomp3.Time.cast_time(entry.last_modified)

    network =
      Network
      |> Namazu.Repo.get_by(network_code: entry.network.network_code)

    {:ok, station} =
      entry.station
      |> Map.put(:network_id, network.id)
      |> Namazu.upsert(Station)

    Logger.info("Importing : #{Namazu.to_string(station)}")

    result =
      entry.station_period
      |> Seiscomp3.Time.cast()
      |> Map.put(:station_id, station.id)
      |> Namazu.upsert(StationPeriod)

    case result do
      {:ok, station_period} ->
        Logger.info("Importing : #{Namazu.to_string(station_period)}")
        {:ok, station_period, last_modified, entry.oid}

      {:error, changeset} ->
        {:error,
         add_error(changeset, :station, "Failed to insert Station #{entry.station.station_code}")}
    end
  end
end
