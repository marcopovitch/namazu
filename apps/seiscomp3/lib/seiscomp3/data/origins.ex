defmodule Seiscomp3.Data.Origins do
  @behaviour Seiscomp3.Syncer

  import Ecto.Query

  require Logger

  alias Namazu.Seismic.Arrival
  alias Namazu.Seismic.Origin
  alias Namazu.Seismic.OriginQuality
  alias Namazu.Seismic.OriginUncertainty
  alias Namazu.Seismic.Pick
  alias Namazu.Seismic.Stream

  def state_key, do: "origin"

  def query do
    from(
      o in "origin",
      join: po in "publicobject",
      on: [_oid: o._oid],
      join: r in "originreference",
      on: [m_originid: po.m_publicid],
      join: e in "event",
      on: [_oid: r._parent_oid],
      join: pe in "publicobject",
      on: [_oid: e._oid],
      order_by: [o._last_modified, o._oid],
      select: %{
        origin: %{
          time:
            fragment("(?||'.'||?)::timestamp with time zone", o.m_time_value, o.m_time_value_ms),
          latitude: o.m_latitude_value,
          latitude_uncertainty: o.m_latitude_uncertainty,
          longitude: o.m_longitude_value,
          longitude_uncertainty: o.m_longitude_uncertainty,
          depth: o.m_depth_value,
          depth_uncertainty: o.m_depth_uncertainty,
          depth_used: o.m_depth_used,
          depth_type: o.m_depthtype,
          method_id: o.m_methodid,
          earthmodel_id: o.m_earthmodelid,
          automatic: o.m_evaluationmode == "automatic",
          evaluation_status: o.m_evaluationstatus,
          origin_type: o.m_type,
          agency: o.m_creationinfo_agencyid,
          author: o.m_creationinfo_author,
          created_at: o.m_creationinfo_creationtime
        },
        origin_quality_used: o.m_quality_used,
        origin_quality: %{
          associated_phase_count: o.m_quality_associatedphasecount,
          used_phase_count: o.m_quality_usedphasecount,
          associated_station_count: o.m_quality_associatedstationcount,
          used_station_count: o.m_quality_usedstationcount,
          standard_error: o.m_quality_standarderror,
          azimuthal_gap: o.m_quality_azimuthalgap,
          maximum_distance: o.m_quality_maximumdistance,
          minimum_distance: o.m_quality_minimumdistance,
          median_distance: o.m_quality_mediandistance
        },
        origin_uncertainty_used: o.m_uncertainty_used,
        origin_uncertainty: %{
          horizontal: o.m_uncertainty_horizontaluncertainty,
          minimum_horizontal: o.m_uncertainty_minhorizontaluncertainty,
          maximum_horizontal: o.m_uncertainty_maxhorizontaluncertainty,
          confidenceellipsoid_semimajoraxislength:
            o.m_uncertainty_confidenceellipsoid_semimajoraxislength,
          confidenceellipsoid_semiminoraxislength:
            o.m_uncertainty_confidenceellipsoid_semiminoraxislength,
          confidenceellipsoid_semiintermediateaxislength:
            o.m_uncertainty_confidenceellipsoid_semiintermediateaxislength,
          confidenceellipsoid_majoraxisplunge:
            o.m_uncertainty_confidenceellipsoid_majoraxisplunge,
          confidenceellipsoid_majoraxisazimuth:
            o.m_uncertainty_confidenceellipsoid_majoraxisazimuth,
          confidenceellipsoid_majoraxisrotation:
            o.m_uncertainty_confidenceellipsoid_majoraxisrotation,
          confidenceellipsoid_used: o.m_uncertainty_confidenceellipsoid_used
        },
        mapping: %{
          event_oid: e._oid,
          event_publicid: pe.m_publicid
        },
        event: %{
          event_type: e.m_type
        },
        last_modified: o._last_modified,
        oid: o._oid
      }
    )
  end

  defp fix_origin_type(origin_map) do
    if origin_map.origin_type do
      origin_map
    else
      %{origin_map | origin_type: "hypocenter"}
    end
  end

  defp fix_depth_type(origin_map) do
    if origin_map.depth_type do
      origin_map
    else
      %{origin_map | depth_type: "from location"}
    end
  end

  def prepare(origin) do
    origin
    |> fix_origin_type
    |> fix_depth_type
  end

  def import_entry(entry) do
    unless Namazu.Repo.in_transaction?(), do: raise("Can't be run outside a transaction")

    last_modified = Seiscomp3.Time.cast_time(entry.last_modified)
    event_map = Seiscomp3.Data.Events.prepare(entry.event)

    [_, event] = Seiscomp3.Mapping.upsert!(entry.mapping, event_map)

    {:ok, origin} =
      entry.origin
      |> prepare
      |> Map.put(:event_id, event.id)
      |> Namazu.upsert(Origin)

    Logger.info("Importing : #{Namazu.to_string(origin)}")

    # Get origin's arrivals and associated picks
    Seiscomp3.Data.Arrivals.query()
    |> where([a], a._parent_oid == ^entry.oid)
    |> Seiscomp3.Repo.all()
    |> Enum.map(fn row ->
      {:ok, stream} = Namazu.upsert(row.pick_stream, Stream)

      {:ok, pick} =
        row.pick
        |> Map.put(:stream_id, stream.id)
        |> Namazu.upsert(Pick)

      Logger.info("Importing : #{Namazu.to_string(pick)}")

      {:ok, arrival} =
        row.arrival
        |> Map.put(:origin_id, origin.id)
        |> Map.put(:pick_id, pick.id)
        |> Namazu.upsert(Arrival)

      Logger.info("Importing : #{Namazu.to_string(arrival)}")
    end)

    # Create an associated quality if used
    if entry.origin_quality_used do
      {:ok, origin_quality} =
        entry.origin_quality
        |> Map.put(:origin_id, origin.id)
        |> Namazu.upsert(OriginQuality)

      Logger.info("Importing : #{Namazu.to_string(origin_quality)}")
    end

    # Create an associated uncertainty if used
    if entry.origin_uncertainty_used do
      {:ok, origin_uncertainty} =
        entry.origin_uncertainty
        |> Map.put(:origin_id, origin.id)
        |> Namazu.upsert(OriginUncertainty)

      Logger.info("Importing : #{Namazu.to_string(origin_uncertainty)}")
    end

    {:ok, origin, last_modified, entry.oid}
  end
end
