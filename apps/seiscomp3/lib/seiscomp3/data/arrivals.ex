defmodule Seiscomp3.Data.Arrivals do
  import Ecto.Query

  def query do
    from(
      a in "arrival",
      join: pp in "publicobject",
      on: [m_publicid: a.m_pickid],
      join: p in "pick",
      on: [_oid: pp._oid],
      select: %{
        arrival: %{
          phase_code: a.m_phase_code,
          azimuth: a.m_azimuth,
          distance: a.m_distance,
          time_residual: a.m_timeresidual,
          time_weight: a.m_weight
        },
        pick_stream: %{
          network_code: p.m_waveformid_networkcode,
          station_code: p.m_waveformid_stationcode,
          location_code: p.m_waveformid_locationcode,
          channel_code: p.m_waveformid_channelcode
        },
        pick: %{
          time:
            fragment("(?||'.'||?)::timestamp with time zone", p.m_time_value, p.m_time_value_ms),
          time_lower_uncertainty: p.m_time_loweruncertainty,
          time_upper_uncertainty: p.m_time_upperuncertainty,
          filter_id: p.m_filterid,
          method_id: p.m_methodid,
          phase_hint_code: p.m_phasehint_code,
          phase_hint_used: p.m_phasehint_used,
          polarity: p.m_polarity,
          automatic: p.m_evaluationmode == "automatic",
          agency: p.m_creationinfo_agencyid,
          author: p.m_creationinfo_author,
          created_at: p.m_creationinfo_creationtime
        }
      }
    )
  end
end
