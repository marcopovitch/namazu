defmodule Seiscomp3.Process do
  require Logger

  @moduledoc false

  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, :ok)
  end

  defp schedule_work do
    Process.send_after(self(), :sync, 10000)
  end

  def init(state) do
    Logger.info("Start Seiscomp3 app")
    send(self(), :sync)
    {:ok, state}
  end

  def handle_info(:sync, :ok) do
    # Logger.info("Start synchronization...")

    [
      Seiscomp3.Data.Origins,
      Seiscomp3.Data.OriginsReferences,
      Seiscomp3.Data.Magnitudes,
      Seiscomp3.Data.Events,
      Seiscomp3.Data.Networks,
      Seiscomp3.Data.Stations
    ]
    |> Enum.each(&Seiscomp3.execute_syncer(&1))

    schedule_work()

    {:noreply, :ok}
  end
end
