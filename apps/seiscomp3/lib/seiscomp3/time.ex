defmodule Seiscomp3.Time do
  def cast_time(naive_datetime) do
    case naive_datetime do
      nil ->
        nil

      _ ->
        Elixir.DateTime.from_naive!(naive_datetime, "Etc/UTC")
    end
  end

  def cast(map = %{period_start: _, period_end: _}) do
    %{map | period_start: cast_time(map.period_start), period_end: cast_time(map.period_end)}
  end

  def cast(map = %{created_at: _, time_window_reference: _}) do
    %{
      map
      | created_at: cast_time(map.created_at),
        time_window_reference: cast_time(map.time_window_reference)
    }
  end

  def cast(map = %{created_at: _, time: _}) do
    %{map | created_at: cast_time(map.created_at), time: cast_time(map.time)}
  end

  def cast(map = %{time: _}) do
    %{map | time: cast_time(map.time)}
  end

  def cast(map = %{created_at: _}) do
    %{map | created_at: cast_time(map.created_at)}
  end
end
