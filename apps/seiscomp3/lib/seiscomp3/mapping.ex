defmodule Seiscomp3.Mapping do
  use Ecto.Schema
  import Ecto.Changeset

  alias Namazu.Seismic.Event, as: Event

  @primary_key {:event_oid, :integer, autogenerate: false}
  @schema_prefix :seiscomp3

  schema "mappings" do
    belongs_to(:event, Event)

    field(:event_publicid, :string)
  end

  def distinctive_fields(), do: [:event_oid]

  def upsert!(mapping_map, event_map) do
    case Namazu.Repo.get(Seiscomp3.Mapping, mapping_map.event_oid) do
      nil ->
        # No mapping found, let's create it with its event
        event =
          %Event{}
          |> change(event_map)
          |> Namazu.Repo.insert!()

        mapping =
          %Seiscomp3.Mapping{event_id: event.id}
          |> change(mapping_map)
          |> Namazu.Repo.insert!()

        [mapping, event]

      mapping ->
        # Mapping found, just return the associated event
        [mapping, Namazu.Repo.get(Event, mapping.event_id)]
    end
  end
end
