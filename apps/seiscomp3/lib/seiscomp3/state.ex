defmodule Seiscomp3.State do
  use Ecto.Schema

  import Ecto.Changeset

  @primary_key {:table_name, :string, autogenerate: false}
  @schema_prefix :seiscomp3

  schema "states" do
    field(:last_modified, :utc_datetime_usec)
    field(:last_oid, :integer)
  end

  def changeset(state, params \\ %{}) do
    state
    |> cast(params, [:last_modified, :last_oid])
  end

  @spec set_last_modified(%Seiscomp3.State{}, %Elixir.DateTime{}, Integer) ::
          {:ok, %Seiscomp3.State{}} | {:error, Ecto.Changeset.t()}
  def set_last_modified(state, datetime, oid) do
    state
    |> changeset(%{last_modified: datetime, last_oid: oid})
    |> Namazu.Repo.insert_or_update(on_conflict: :replace_all, conflict_target: :table_name)
  end

  @spec get_last_modified(String.t()) :: %Seiscomp3.State{} | nil
  def get_last_modified(table_name) do
    Namazu.Repo.get(Seiscomp3.State, table_name)
  end
end
