# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :web,
  namespace: Web

config :web, Web.Gettext, locales: ~w(en fr), default_locale: "en"

# Configures the endpoint
config :web, Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "erxvLI5TEvDupv85zVLZyIqIvVQknXRhB34j+gB2LVlVpPeMHrn6QRxWlgqnRoN2",
  render_errors: [view: Web.ErrorView, accepts: ~w(html json)],
  matomo_host: System.get_env("NMZ_MATOMO_HOST"),
  pubsub: [name: Web.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :web, :generators, context_app: :web

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
