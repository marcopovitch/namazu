��    0      �  C         (     )     >     M     a  	   s     }  
   �     �     �     �     �     �     �     �  
               	   )     3     D     T  	   h     r     z  	   �  	   �     �     �     �     �     �     �     �          $     0     =     O  
   X  	   c     m     s     �  
   �     �     �     �  �   �     �     �     �     �  	         
          1     K     f     y     �     �     �     �     �     �  	   �     �     	     '	     C	     X	     _	     v	     �	     �	     �	  
   �	     �	     �	     �	     �	      
     /
     =
     N
     g
     v
     �
  
   �
     �
     �
     �
     �
     �
     �
                                                               *         &       	   -   $       '         (          %         ,         )   "   +               !                        #       /         .   
      0                         accidental explosion acoustic noise anthropogenic event atmospheric event avalanche blasting levee boat crash building collapse cavity collapse chemical explosion collapse controlled explosion crash debris avalanche earthquake event experimental explosion explosion fluid extraction fluid injection hydroacoustic event ice quake induced industrial explosion landslide meteorite mine collapse mining explosion not existing not locatable not reported nuclear explosion other event outside of network interest plane crash quarry blast reservoir loading road cut rock burst rockslide slide snow avalanche sonic blast sonic boom thunder train crash volcanic eruption Language: fr
Plural-Forms: nplurals=2;
Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
 explosion accidentelle bruit acoustique évènement anthropique évènement atmosphérique avalanche digue de dynamitage accident de bateau effondrement de bâtiment effondrement de la cavité explosion chimique effondrement explosion contrôlée accident avalanche de débris tremblement de terre évènement explosion expérimentale explosion extraction de fluide injection de fluide évènement hydroacoustique tremblement de glace induit explosion industrielle glissement de terrain météorite effondrement de mine explosion de mine inexistant non localisable non reporté explosion nucléaire autre événement en dehors de la zone d'intérêt crash d'avion tir de carrière chargement de réservoir coupe de route éclat de roche éboulement glissement avalanche de neige effet de souffle détonation supersonique tonnere crash de train éruption volcanique 