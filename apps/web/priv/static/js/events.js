'use strict'

var EventMap = function (element, xCenter, yCenter) {
  var map = L.map(element, {
    center: [yCenter, xCenter],
    keyboard: false,
    dragging: true,
    zoomControl: false,
    boxZoom: false,
    scrollWheelZoom: false,
    doubleClickZoom: true,
    tap: false,
    touchZoom: false,
    zoom: 5,
    maxZoom: 18
  })

  L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}', {
    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    subdomains: 'abcd',
    minZoom: 1,
    maxZoom: 18,
    ext: 'png'
  }).addTo(map)

  function eventMarkerColor (magnitude) {
    if (magnitude < 2.5) {
      return '#fff900'
    } else if (magnitude < 3.0) {
      return '#ffce01'
    } else if (magnitude < 3.5) {
      return '#ff9d02'
    } else if (magnitude < 4.0) {
      return '#ff5003'
    } else if (magnitude < 4.5) {
      return '#e36450'
    } else if (magnitude < 5.0) {
      return '#c74246'
    } else if (magnitude < 5.5) {
      return '#a94143'
    } else if (magnitude < 6.0) {
      return '#843e4f'
    } else {
      return '#6f4e4e'
    }
  }

  function eventMarkerRadius (magnitude) {
    // return magnitude * 4
    return Math.log(magnitude + 1) * 8
  }

  function eventMarker (latlng, magnitude, url, radius) {
    var radius = radius ? radius : eventMarkerRadius(magnitude)

    var geojsonMarkerOptions = {
      radius: radius,
      fillColor: eventMarkerColor(magnitude),
      color: '#000',
      weight: 1,
      opacity: 1,
      fillOpacity: 0.8
    }

    var marker = L.circleMarker(latlng, geojsonMarkerOptions)

    marker.on('click', function () {
      window.location.href = url
    })

    return marker
  }

  var obj = {
    addEvent (longitude, latitude, magnitude, url, tooltip, radius) {
      eventMarker(L.latLng(latitude, longitude), magnitude, url, radius)
        .bindTooltip(tooltip)
        .addTo(map)

      return this
    },

    addStation (longitude, latitude, color, tooltip) {
      var svgString = '<?xml version="1.0" encoding="UTF-8"?><svg width="8.5in" height="14in" version="1.1" viewBox="0 0 215.9 355.6" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0,58.6)"><path d="m106.96 63.789 51.283 88.824-102.57-1e-5z" fill="FILL_COLOR" style="paint-order:normal"/></g></svg>'

      var stationUrl = encodeURI("data:image/svg+xml," + svgString.replace('FILL_COLOR', color)).replace('#','%23')

      var stationIcon = L.icon({
        iconUrl: stationUrl,
        iconSize: 50
      })

      L.marker(L.latLng(latitude, longitude), {icon: stationIcon})
        .bindTooltip(tooltip)
        .addTo(map)

      return this
    },

    fitBounds (minimalLongitude, minimalLatitude, maximalLongitude, maximalLatitude) {
      var firstCorner = L.latLng(minimalLatitude, minimalLongitude)
      var secondCorner = L.latLng(maximalLatitude, maximalLongitude)
      var bounds = L.latLngBounds(firstCorner, secondCorner)

      map.fitBounds(bounds, { maxZoom: 10 })

      return this
    },

    addZoomControl () {
      L.control.zoom({position: 'topright'}).addTo(map)

      return this
    },

    addMagnitudeLegend () {
      if (map.getSize().x <= 768) {
        return this
      }

      var legend = L.control({position: 'bottomright'})

      legend.onAdd = function () {
        var div = L.DomUtil.create('div', 'box')

        var magnitudes = [
          { color: '#fff900', text: '< 2.5' },
          { color: '#ffce01', text: '< 3.0' },
          { color: '#ff9d02', text: '< 3.5' },
          { color: '#ff5003', text: '< 4.0' },
          { color: '#e36450', text: '< 4.5' },
          { color: '#c74246', text: '< 5.0' },
          { color: '#a94143', text: '< 5.5' },
          { color: '#843e4f', text: '< 6.0' },
          { color: '#6f4e4e', text: '>= 6.0' }
        ]

        div.innerHTML += '<h4 style="margin: 0;">Magnitudes</h4>'

        for (var i = 0; i < magnitudes.length; i++) {
          div.innerHTML += '<i style="margin-right: 8px; background: ' + magnitudes[i].color + '; height: 18px; width: 18px; float: left;"></i>' + magnitudes[i].text + '<br>'
        }

        return div
      }

      legend.addTo(map)

      return this
    }
  }

  return obj
}
