defmodule Web.QuestionController do
  import Plug.Conn
  import Ecto.Query

  use Web, :controller

  alias Namazu.Repo
  alias Namazu.Macro.{Answer, Form, Question, QuestionChoice, Testimony}

  @doc """
    Build context :
    - testimony
    - questions
    - current_question
    - answer for the current_question if it has been provided
  """
  defmodule Context do
    defstruct testimony: nil, questions: nil, current_question: nil, answer: nil
  end

  defp build_context(conn) do
    testimony =
      get_session(conn, :testimony_key)
      |> Testimony.get_by_key()

    current_question = Question |> Repo.get(get_session(conn, :current_question_id))

    %Context{
      testimony: testimony,
      questions: testimony.form.questions,
      current_question: current_question
    }
  end

  @doc """
    Entrypoint to handle the form progression by the user

    build_context
    |> next/previous_question
    |> render_question
  """
  def form_proceed(conn, params \\ []) do
    case params["direction"] do
      "next" ->
        next_question(conn, params)

      "previous" ->
        previous_question(conn, params)

      _ ->
        conn
        |> put_flash(:error, "Bad direction")
        |> question(params)
    end
  end

  @doc """
    Handles the form progression (next) by the user
    - Save/Update the user Answer for the question
    - Find the next question
    - Render the new form
  """
  def next_question(conn, params) do
    context = build_context(conn)

    # Checks if we have a question_choice_id or a value for the next steps
    # We should not have both; TODO: Add a changeset to test this.
    question_choice_id =
      Map.get(
        params["answer"],
        "question_choice_id",
        get_question_choice(context.current_question)
      )

    question_choice = QuestionChoice |> preload(:question) |> Repo.get(question_choice_id)

    value = Map.get(params["answer"], "value")

    # Instanciate the changeset
    # NB : If the value is nil, it will be automatically set
    # to the question_choice value
    answer_changeset =
      Answer.changeset(%Answer{}, %{
        question_choice: question_choice,
        testimony: context.testimony,
        value: value
      })

    previous_answer =
      Testimony.get_answer_for_question(context.testimony, question_choice.question)

    if answer_changeset.valid? do
      # Save the answer
      if previous_answer do
        Repo.get(Answer, previous_answer.id)
        |> Repo.delete()
      end

      Repo.insert(answer_changeset)
      # Test if the answer exists, and update the changeset instead of creating in this case
      # Answer.changeset(<response>, %{question_choice: question_choice, testimony: testimony})

      questions = Form.questions(context.testimony.form)

      case Question.get_next_question(questions, context.current_question, context.testimony) do
        {:ok, next_question} ->
          conn
          |> put_session(:current_question_id, next_question.id)
          |> question(params, next_question)

        {:error, :no_question} ->
          render(conn, "end.html")
      end
    end

    question(conn, params)
  end

  @doc """
    Assume there is only a single question_choice for a question and returns it
  """
  defp get_question_choice(question) do
    question_choice =
      Question.choices(question)
      |> List.first()

    question_choice.id
  end

  @doc """
    Handles the form progression (previous) by the user
    - Delete the user Answer for the question
    - Find the previous question
    - Render the new form
  """
  def previous_question(conn, params) do
    # Retreiving the user testimony from the database
    context = build_context(conn)

    # Delete the answer
    qc_ids =
      from(
        q in Question,
        join: qc in assoc(q, :question_choices),
        select: qc.id,
        where: q.id == ^context.current_question.id
      )
      |> Repo.all()

    req2 =
      from(
        a in Answer,
        join: t in assoc(a, :testimony),
        where: t.id == ^context.testimony.id,
        where: a.question_choice_id in ^qc_ids
      )
      |> Repo.one()

    unless is_nil(req2) do
      req2 |> Repo.delete()
    end

    questions = Form.questions(context.testimony.form)

    case Question.get_previous_question(questions, context.current_question, context.testimony) do
      {:ok, previous_question} ->
        conn
        |> put_session(:current_question_id, previous_question.id)
        |> question(params, previous_question)

      {:error, :no_question} ->
        conn
        |> put_flash(:error, "Pas de question précédente !")
        |> question(params, context.current_question)
    end
  end

  @doc """
      Renders a question. Final step in the form_proceed process.
  """
  def question(conn, params, question \\ nil, changeset \\ Answer.changeset(%Answer{})) do
    # If there is not question specified (flash, error handling, ...), we
    # render the template with the current session question untouched
    if is_nil(question) do
      current_question_id = get_session(conn, :current_question_id)

      question =
        Question
        |> join(:left, [q], qc in assoc(q, :question_choices))
        |> preload([q, qc], question_choices: qc)
        |> Repo.get(current_question_id)
    end

    conn
    |> render("question.html", question: question, changeset: changeset)
  end
end
