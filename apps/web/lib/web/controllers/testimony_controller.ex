defmodule Web.TestimonyController do
  use Web, :controller
  alias Namazu.Macro.{Form, Testimony}
  alias Namazu.Repo
  # import Ecto.Query

  def new(conn, params) do
    # Optional parameter
    event_id = params |> Map.get("event_id", nil)
    locale = params |> Map.get("locale")

    conn
    |> clear_session
    |> render("new.html", event_id: event_id, locale: locale)
  end

  defp build_datetime(params) do
    date = Map.get(params, "date")
    time = Map.get(params, "time")

    case date != nil and time != nil do
      true ->
        Map.put(params, "naive_felt_time", NaiveDateTime.from_iso8601!("#{date}T#{time}:00"))

      _ ->
        params
    end
  end

  def witness_address(%{method: "POST"} = conn, params) do
    address_params = params |> Map.get("address")
    testimony_params = params |> Map.get("testimony", %{})
    event_id = params |> Map.get("event_id", nil)
    locale = params |> Map.get("locale")

    witness_address_path =
      case event_id do
        nil ->
          # locale_testimony_testimony_path(conn, :witness_address, locale)
          Routes.testimony_testimony_path(conn, :witness_address, locale)

        "" ->
          # locale_testimony_testimony_path(conn, :witness_address, locale)
          Routes.testimony_testimony_path(conn, :witness_address, locale)

        value ->
          # locale_testimony_testimony_path(conn, :witness_address, locale, event_id: value)
          Routes.testimony_testimony_path(conn, :witness_address, locale, event_id: value)
      end

    # address_changeset = Address.changeset(%Address{}, address_params)

    # assigns = [
    #   changeset: address_changeset,
    #   event_id: event_id,
    #   ask_felt_time: event_id == nil,
    #   witness_address_path: witness_address_path
    # ]

    # if address_changeset.valid? do
    #   # Create to database the testimony and the address
    #   testimony_changeset =
    #     testimony_params
    #     |> Map.put("address", address_changeset)
    #     |> build_datetime
    #     |> (&Testimony.changeset(%Testimony{}, &1)).()

    #   if testimony_changeset.valid? do
    #     {:ok, testimony} = testimony_changeset |> Repo.insert()
    #     {:ok, testimony_key} = Ecto.UUID.load(testimony.key)

    #     question = Form.root_question(testimony.form)

    #     conn
    #     |> put_session(:current_question_id, question.id)
    #     |> put_session(:locale, locale)
    #     |> put_session(:testimony_key, testimony_key)
    #     # |> redirect(to: locale_testimony_question_path(conn, :question, locale, testimony_key))
    #     |> redirect(to: testimony_question_path(conn, :question, locale, testimony_key))
    #   end
    # else
    #   # Renders the same page with an error
    #   conn
    #   |> put_flash(:error, "Error")
    #   |> render("witness_address.html", assigns)
    # end
  end

  def witness_address(conn, params) do
    event_id = params |> Map.get("event_id", nil)
    locale = params |> Map.get("locale")

    witness_address_path =
      case event_id do
        nil ->
          # locale_testimony_testimony_path(conn, :witness_address, locale)
          Routes.testimony_testimony_path(conn, :witness_address, locale)

        "" ->
          # locale_testimony_testimony_path(conn, :witness_address, locale)
          Routes.testimony_testimony_path(conn, :witness_address, locale)

        value ->
          # locale_testimony_testimony_path(conn, :witness_address, locale, event_id: value)
          Routes.testimony_testimony_path(conn, :witness_address, locale, event_id: value)
      end

    # changeset = Address.changeset(%Address{}, params)

    render(
      conn,
      "witness_address.html",
      # changeset: changeset,
      event_id: event_id,
      ask_felt_time: event_id == nil,
      witness_address_path: witness_address_path
    )
  end
end
