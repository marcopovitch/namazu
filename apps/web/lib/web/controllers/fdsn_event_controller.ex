defmodule Web.FDSNEventController do
  import Plug.Conn

  alias Namazu.Seismic
  alias Namazu.Repo
  import Ecto.Query

  use Web, :controller

  @version 1.2

  # "catalog",
  # "contributor",
  # "updateafter"

  def parse_datetime(_datetime = ""), do: nil
  def parse_datetime(_datetime = nil), do: nil

  def parse_datetime(datetime) do
    case DateTime.from_iso8601(datetime) do
      {:ok, datetime, _offset} ->
        datetime

      {:error, :missing_offset} ->
        {:ok, datetime, _offset} = DateTime.from_iso8601(datetime <> "Z")
        datetime
    end
  end

  def parse_float(_value = ""), do: nil
  def parse_float(_value = nil), do: nil

  def parse_float(value) do
    {value, _} = Float.parse(value)
    value
  end

  def parse_integer(_value = ""), do: nil
  def parse_integer(_value = nil), do: nil

  def parse_integer(value), do: String.to_integer(value)

  def parse_event_types(_event_types = ""), do: nil
  def parse_event_types(_event_types = nil), do: nil

  def parse_event_types(event_types) do
    String.split(event_types, ",")
    |> Enum.map(&Namazu.Seismic.Event.quakeml_to_namazu_event_type(&1))
  end

  def parse_order_by(_value = ""), do: nil
  def parse_order_by(_value = nil), do: nil

  def parse_order_by(value) do
    case value do
      "time" -> :descending_time
      "time-asc" -> :ascending_time
      "magnitude" -> :descending_magnitude
      "magnitude-asc" -> :ascending_magnitude
      "ascending_time" -> :ascending_time
      "descending_time" -> :descending_time
      "ascending_magnitude" -> :ascending_magnitude
      "descending_magnitude" -> :descending_magnitude
      _ -> nil
    end
  end

  def fdsn_event_query(params) do
    Namazu.Search.EventSearch.query(%Namazu.Search.EventSearch{
      start_time: Map.get(params, "starttime") |> parse_datetime(),
      end_time: Map.get(params, "endtime") |> parse_datetime(),
      minimal_longitude: Map.get(params, "minlongitude") |> parse_float(),
      maximal_longitude: Map.get(params, "maxlongitude") |> parse_float(),
      minimal_latitude: Map.get(params, "minlatitude") |> parse_float(),
      maximal_latitude: Map.get(params, "maxlatitude") |> parse_float(),
      minimal_magnitude: Map.get(params, "minmagnitude") |> parse_float(),
      maximal_magnitude: Map.get(params, "maxmagnitude") |> parse_float(),
      minimal_depth: Map.get(params, "mindepth") |> parse_float(),
      maximal_depth: Map.get(params, "maxdepth") |> parse_float(),
      event_types: Map.get(params, "eventtype") |> parse_event_types(),
      event_id: Map.get(params, "eventid"),
      limit: Map.get(params, "limit") |> parse_integer(),
      offset: Map.get(params, "offset") |> parse_integer(),
      order_by: Map.get(params, "orderby") |> parse_order_by()
    })
  end

  def get_event_ids_chunks(params, chunk_size \\ 100) do
    fdsn_event_query(params)
    |> Repo.all()
    |> Enum.map(fn e -> e.id end)
    |> Enum.chunk_every(chunk_size)
  end

  def get_events_from_ids(event_ids) do
    Seismic.event_query()
    |> where([event: e], e.id in ^event_ids)
  end

  def sort_events_chunk(events, params) do
    case Map.get(params, "orderby") do
      "time" ->
        events |> Enum.sort_by(fn e -> e.preferred_origin.time end) |> Enum.reverse()

      "time-asc" ->
        events |> Enum.sort_by(fn e -> e.preferred_origin.time end)

      "magnitude" ->
        events |> Enum.sort_by(fn e -> e.preferred_magnitude.magnitude end) |> Enum.reverse()

      "magnitude-asc" ->
        events |> Enum.sort_by(fn e -> e.preferred_magnitude.magnitude end)

      _ ->
        events
    end
  end

  def query(conn, params) do
    format = Map.get(params, "format", "xml")

    case format do
      "json" ->
        features =
          fdsn_event_query(params)
          |> Seismic.preload_distinctive_cities()
          |> Repo.all()
          |> Enum.map(fn e ->
            %{
              id: e.id,
              geometry: %{
                coordinates: [
                  e.preferred_origin.longitude,
                  e.preferred_origin.latitude,
                  -e.preferred_origin.depth
                ],
                type: "Point"
              },
              properties: %{
                description: Web.Filters.event_description(e),
                automatic: e.preferred_origin.automatic,
                mag: e.preferred_magnitude.magnitude,
                magType: e.preferred_magnitude.magnitude_type,
                # place: "TODO!!!",
                time: e.preferred_origin.time,
                type: e.event_type,
                url: Routes.event_url(conn, :show, "en", e.id)
              },
              type: "Feature"
            }
          end)

        json(conn, %{
          type: "FeatureCollection",
          features: features
        })

      "text" ->
        conn =
          conn
          |> put_resp_content_type("text/plain")
          |> send_chunked(:ok)

        for chunk <- get_event_ids_chunks(params) do
          events =
            get_events_from_ids(chunk)
            |> Seismic.preload_preferred()
            |> Repo.all()
            |> sort_events_chunk(params)

          text = Phoenix.View.render(Web.FDSNEventView, "query.txt", events: events)

          chunk(conn, text)
        end

        conn

      _ ->
        include_all_origins = Map.get(params, "includeallorigins", "false") == "true"
        include_all_magnitudes = Map.get(params, "includeallmagnitudes", "false") == "true"
        include_arrivals = Map.get(params, "includearrivals", "false") == "true"

        conn =
          conn
          |> put_layout(:none)
          |> put_resp_content_type("application/xml")
          |> send_chunked(:ok)

        escaped_url = Phoenix.Controller.current_url(conn, params) |> String.replace("&", "&amp;")

        chunk(
          conn,
          Phoenix.View.render(Web.FDSNEventView, "query_start.xml", url: escaped_url)
        )

        for chunk <- get_event_ids_chunks(params) do
          events_query =
            get_events_from_ids(chunk)
            |> Seismic.preload_origins(!(include_all_origins || include_all_magnitudes))
            |> Seismic.preload_magnitudes(!(include_all_origins || include_all_magnitudes))

          events_query =
            if Map.has_key?(params, "orderby") do
              Seismic.preload_preferred(events_query)
            else
              events_query
            end

          events =
            if include_arrivals do
              Seismic.preload_origins_arrivals(events_query)
            else
              events_query
            end
            |> Repo.all()
            |> sort_events_chunk(params)

          xml =
            Phoenix.View.render(Web.FDSNEventView, "query.xml",
              events: events,
              include_all_origins: include_all_origins,
              include_all_magnitudes: include_all_magnitudes,
              include_arrivals: include_arrivals
            )

          chunk(conn, xml)
        end

        chunk(conn, Phoenix.View.render(Web.FDSNEventView, "query_end.xml", %{}))

        conn
    end
  end

  def base(conn, _params) do
    text(conn, "base")
  end

  def wadl(conn, _params) do
    conn
    |> put_layout(:none)
    |> put_resp_content_type("application/xml")
    |> render("application.wadl", version: @version)
  end

  def catalogs(conn, _params) do
    conn
    |> put_layout(:none)
    |> put_resp_content_type("application/xml")
    |> render("catalog.xml")
  end

  def contributors(conn, _params) do
    conn
    |> put_layout(:none)
    |> put_resp_content_type("application/xml")
    |> render("contributors.xml")
  end

  def version(conn, _params) do
    text(conn, @version)
  end
end
