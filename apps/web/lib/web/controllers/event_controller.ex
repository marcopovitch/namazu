defmodule Web.EventController do
  import Plug.Conn

  use Web, :controller

  require Web.Gettext

  import Web.FDSNEventController,
    only: [
      parse_datetime: 1,
      parse_float: 1,
      parse_integer: 1,
      parse_order_by: 1
    ]

  alias Namazu.Geography.City
  alias Namazu.Repo
  alias Namazu.Seismic.Origin
  alias Namazu.Seismic
  alias Namazu.Cache

  import Ecto.Query

  def search(conn, _params = %{"locale" => locale}) do
    event_types = Cache.get_event_types()

    title = Gettext.gettext(Web.Gettext, "Search")

    render(conn, "search.html", title: title, locale: locale, event_types: event_types)
  end

  def convert_start_date(start_date) do
    case start_date do
      "" -> nil
      nil -> nil
      start_date -> "#{start_date}T00:00:00Z" |> parse_datetime()
    end
  end

  def convert_end_date(end_date) do
    case end_date do
      "" -> nil
      nil -> nil
      end_date -> "#{end_date}T23:59:59.999999Z" |> parse_datetime()
    end
  end

  def search_results(conn, params = %{"locale" => locale}) do
    events_limit = 30
    query_params = Map.get(conn, :query_params)

    event_types =
      query_params
      |> Enum.filter(fn {key, value} ->
        String.starts_with?(key, "event_type_") and value == "true"
      end)
      |> Enum.map(fn {key, _value} -> String.slice(key, 11, 40) end)

    search_params = %Namazu.Search.EventSearch{
      start_time: Map.get(query_params, "start_date") |> convert_start_date(),
      end_time: Map.get(query_params, "end_date") |> convert_end_date(),
      minimal_longitude: Map.get(query_params, "minimal_longitude") |> parse_float(),
      maximal_longitude: Map.get(query_params, "maximal_longitude") |> parse_float(),
      minimal_latitude: Map.get(query_params, "minimal_latitude") |> parse_float(),
      maximal_latitude: Map.get(query_params, "maximal_latitude") |> parse_float(),
      minimal_magnitude: Map.get(query_params, "minimal_magnitude") |> parse_float(),
      maximal_magnitude: Map.get(query_params, "maximal_magnitude") |> parse_float(),
      minimal_depth: Map.get(query_params, "minimal_depth") |> parse_float(),
      maximal_depth: Map.get(query_params, "maximal_depth") |> parse_float(),
      event_id: Map.get(query_params, "event_id") |> parse_integer(),
      event_types: event_types,
      limit: Map.get(query_params, "limit") |> parse_integer(),
      offset: Map.get(query_params, "offset") |> parse_integer(),
      order_by: Map.get(query_params, "order_by") |> parse_order_by()
    }

    search_query = Namazu.Search.EventSearch.query(search_params)

    fdsn_xml_path = Namazu.Search.EventSearch.fdsn_query_path(search_params)

    geojson_path = Namazu.Search.EventSearch.fdsn_query_path(search_params, "json")

    fdsn_text_path = Namazu.Search.EventSearch.fdsn_query_path(search_params, "text")

    events_count = search_query |> Repo.aggregate(:count, :id)

    current_page = Map.get(params, "page", "1") |> String.to_integer()

    offset = events_limit * (current_page - 1)

    events =
      search_query
      |> Seismic.preload_distinctive_cities()
      |> offset(^offset)
      |> limit(^events_limit)
      |> Repo.all()

    title = Gettext.gettext(Web.Gettext, "Search results")

    render(conn, "search_results.html",
      locale: locale,
      title: title,
      events_count: events_count,
      events_limit: events_limit,
      events: events,
      fdsn_xml_path: fdsn_xml_path,
      geojson_path: geojson_path,
      fdsn_text_path: fdsn_text_path
    )
  end

  def show(conn, _params = %{"locale" => locale, "event_id" => event_id}) do
    event_id = String.to_integer(event_id)
    event = Cache.get_event(event_id)

    if event do
      search_link =
        Web.Router.Helpers.event_path(Web.Endpoint, :search_results, locale, event_id: event_id)

      # TODO : Should be cached too
      zone = Namazu.Zone.get_from_location(event.preferred_origin.location)

      past_seismicity = Cache.get_past_seismicity(event_id)

      title =
        event
        |> Web.Filters.event_description()
        |> String.capitalize()

      render(conn, "event.html",
        locale: locale,
        title: title,
        event: event,
        search_link: search_link,
        zone: zone,
        near_cities: City.distintive_cities(event.preferred_origin.location, 30),
        arrivals: Origin.get_used_arrivals(event.preferred_origin.id),
        past_seismicity: past_seismicity
      )
    else
      event = Repo.get(Seismic.Event, event_id)

      if not is_nil(event) and not is_nil(event.merged_with_event_id) do
        conn
        |> put_flash(:event_merged_id, event_id)
        |> redirect(
          to: Web.Router.Helpers.event_path(conn, :show, locale, event.merged_with_event_id)
        )
      else
        conn
        |> put_status(:not_found)
        |> put_view(Web.ErrorView)
        |> render("404.html", locale: locale)
      end
    end
  end
end
