defmodule Web.PageController do
  import Plug.Conn
  import Ecto.Query

  alias Namazu.Repo
  alias Namazu.Cache
  alias Namazu.Seismic
  alias Namazu.Zone
  alias Namazu.Seismic.Event

  use Web, :controller

  def index(conn, _params = %{"locale" => locale}) do
    zones =
      Zone
      |> order_by(desc: :weight)
      |> Repo.all()
      |> Enum.map(fn zone ->
        events =
          Zone.get_events(zone)
          |> Seismic.descending_time()
          |> Seismic.preload_distinctive_cities()
          |> limit(15)
          |> Repo.all()

        Map.put(zone, :events, events)
      end)

    title = Gettext.gettext(Web.Gettext, "Home")

    render(conn, "index.html", title: title, zones: zones, locale: locale)
  end

  def index(conn, _params) do
    redirect(conn, to: "/fr")
  end

  def dashboard(conn, _params = %{"locale" => locale, "zone_slug" => zone_slug}) do
    zone = Namazu.Zone |> Namazu.Repo.get_by(slug: zone_slug)
    events_limit = 30

    events =
      Namazu.Zone.get_events(zone)
      |> Seismic.preload_distinctive_cities()
      |> Seismic.descending_time()
      |> limit(^events_limit)
      |> Repo.all()

    render(conn, "dashboard.html",
      locale: locale,
      zone: zone,
      events: events,
      layout: {Web.LayoutView, "dashboard_app.html"}
    )
  end

  def page(conn, _params = %{"locale" => locale, "page_slug" => page_slug}) do
    page = Cache.get_page(locale, page_slug)

    render(conn, "page.html", title: page.title, locale: locale, page: page)
  end

  def contact(conn, _params = %{"locale" => locale}) do
    render(conn, "contact.html",
      title: Gettext.gettext(Web.Gettext, "Contact"),
      locale: locale,
      contact_phone: Namazu.get_contact_phone(),
      contact_email: Namazu.get_contact_email(),
      contact_address: Namazu.get_contact_address(),
      contact_address_latitude: Namazu.get_contact_address_latitude(),
      contact_address_longitude: Namazu.get_contact_address_longitude()
    )
  end

  def actors(conn, _params = %{"locale" => locale}) do
    title = Gettext.gettext(Web.Gettext, "Actors")

    render(conn, "actors.html", title: title, locale: locale, actors: Cache.get_actors())
  end

  def zone(conn, params = %{"locale" => locale, "zone_slug" => zone_slug}) do
    events_limit = 40

    zone = Namazu.Zone |> Namazu.Repo.get_by(slug: zone_slug)

    current_page = Map.get(params, "page", "1") |> String.to_integer()

    events_count = Namazu.Zone.get_events(zone) |> Repo.aggregate(:count, :id)

    offset = events_limit * (current_page - 1)

    # Retrieve events and then separate them in two groups :
    # - Earthquakes and automatic events
    # - Other events
    event_groups =
      Namazu.Zone.get_events(zone)
      |> Seismic.preload_distinctive_cities()
      |> Seismic.descending_time()
      |> limit(^events_limit)
      |> offset(^offset)
      |> Repo.all()
      |> Event.separate_events()

    render(conn, "zone.html",
      title: zone.name,
      locale: locale,
      zone: zone,
      event_groups: event_groups,
      events_limit: events_limit,
      events_count: events_count
    )
  end

  def zone_stations(conn, _params = %{"locale" => locale, "zone_slug" => zone_slug}) do
    zone = Namazu.Zone |> Namazu.Repo.get_by(slug: zone_slug)

    stations = Cache.get_stations_by_zone(zone)

    stations_chunked_by_network =
      stations
      |> Enum.chunk_by(fn s -> s.network.network_code end)
      |> Enum.sort(fn a, b -> length(a) >= length(b) end)

    {minimal_latitude, maximal_latitude} =
      stations
      |> Enum.map(fn s -> s.periods |> List.first() |> Map.get(:latitude) end)
      |> Enum.min_max()

    {minimal_longitude, maximal_longitude} =
      stations
      |> Enum.map(fn s -> s.periods |> List.first() |> Map.get(:longitude) end)
      |> Enum.min_max()

    render(conn, "zone_stations.html",
      locale: locale,
      title: Web.Gettext.gettext("Stations of %{zone_name}", zone_name: zone.name),
      zone: zone,
      stations_chunked_by_network: stations_chunked_by_network,
      minimal_latitude: minimal_latitude,
      maximal_latitude: maximal_latitude,
      minimal_longitude: minimal_longitude,
      maximal_longitude: maximal_longitude
    )
  end
end
