defmodule Web.Bulma do
  def label(name) do
    Phoenix.HTML.Tag.content_tag(:label, name, class: "label")
  end

  def level_item(heading, title) do
    Phoenix.View.render(Web.BulmaView, "level_item.html", heading: heading, title: title)
  end

  def input(form, input_fn, id, label, icon \\ nil) do
    Phoenix.View.render(Web.BulmaView, "input.html",
      form: form,
      input_fn: input_fn,
      id: id,
      label: label,
      icon: icon
    )
  end

  def date_input(form, id, label, icon \\ nil) do
    input(form, &Phoenix.HTML.Form.date_input/3, id, label, icon)
  end

  def number_input(form, id, label, icon \\ nil) do
    input(form, &Phoenix.HTML.Form.number_input/3, id, label, icon)
  end

  def number_input_with_step(form, field, opts \\ []) do
    Phoenix.HTML.Form.number_input(form, field, [step: 0.1] ++ opts)
  end

  def float_input(form, id, label, icon \\ nil) do
    input(form, &number_input_with_step/3, id, label, icon)
  end

  def text_input(form, id, label, icon \\ nil) do
    input(form, &Phoenix.HTML.Form.text_input/3, id, label, icon)
  end

  def submit(text) do
    Phoenix.HTML.Form.submit(text, class: "button is-link is-fullwidth is-medium")
  end

  def reset(text) do
    Phoenix.HTML.Form.reset(text, class: "button is-fullwidth is-medium")
  end

  def select(form, id, options, label, icon) do
    Phoenix.View.render(Web.BulmaView, "select.html",
      form: form,
      id: id,
      options: options,
      label: label,
      icon: icon
    )
  end

  def automatic_tag(automatic) do
    Phoenix.View.render(Web.BulmaView, "automatic_tag.html", automatic: automatic)
  end
end
