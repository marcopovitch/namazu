defmodule Web.Router do
  use Web, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(Web.Plugs.SetLocale, "en")
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", Web do
    pipe_through(:browser)

    get("/", PageController, :index)
  end

  scope "/fdsnws/event/1", Web do
    pipe_through(:browser)

    get("/", FDSNEventController, :base)
    get("/query", FDSNEventController, :query)
    get("/application.wadl", FDSNEventController, :wadl)
    get("/catalogs", FDSNEventController, :catalogs)
    get("/contributors", FDSNEventController, :contributors)
    get("/version", FDSNEventController, :version)
  end

  scope "/:locale", Web do
    pipe_through(:browser)

    get("/", PageController, :index, as: :index_with_locale)

    scope "/actors" do
      get("/", PageController, :actors)
    end

    get("/dashboards/:zone_slug", PageController, :dashboard)

    get("/pages/:page_slug", PageController, :page)

    get("/about", PageController, :about)
    get("/webservices", PageController, :webservices)
    get("/contact", PageController, :contact)
    get("/:zone_slug", PageController, :zone)
    get("/:zone_slug/stations", PageController, :zone_stations)

    scope "/events" do
      get("/search", EventController, :search)
      get("/search/results", EventController, :search_results)
      get("/:event_id", EventController, :show)
    end

    scope "/testimony", as: :testimony do
      get("/new", TestimonyController, :new)
      get("/new/address", TestimonyController, :witness_address)
      get("/:testimony_key", QuestionController, :question)

      post("/new/address", TestimonyController, :witness_address)
      post("/:testimony_key", QuestionController, :form_proceed)
      post("/:testimony_key/next_question", QuestionController, :next_question)
    end
  end
end
