defmodule Web.Page do
  use Ecto.Schema

  alias Namazu.Repo

  import Ecto.Changeset

  @schema_prefix :web

  schema "pages" do
    field(:title, :string)
    field(:slug, :string)
    field(:content, :string)
    field(:published, :boolean)
    field(:locale, :string)
  end

  def distinctive_fields() do
    [:locale, :slug]
  end

  def changeset(page, params \\ %{}) do
    page
    |> cast(params, [
      :locale,
      :slug,
      :title,
      :content,
      :published
    ])
  end

  def get_page(locale, slug) do
    __MODULE__
    |> Repo.get_by(locale: locale, slug: slug)
  end
end
