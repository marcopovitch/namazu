defmodule Web.Filters do
  require Web.Gettext

  alias Web.Router.Helpers, as: Routes

  @days_names {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}

  @months_names {"January", "February", "March", "April", "May", "June", "July", "August",
                 "September", "October", "November", "December"}

  def deduplicate_picks(event) do
    event.origins
    |> Enum.map(fn o -> o.arrivals end)
    |> List.flatten()
    |> Enum.map(fn a -> a.pick end)
    |> Enum.uniq_by(fn p -> p.id end)
  end

  def format_date(%{year: year, month: month, day: day}, locale) do
    case locale do
      "fr" ->
        [day, month, year]

      _ ->
        [year, month, day]
    end
    |> Enum.map(&Integer.to_string(&1))
    |> Enum.map(&String.pad_leading(&1, 2, "0"))
    |> Enum.join("/")
  end

  def format_date_text(datetime, _locale) do
    day_of_week =
      datetime
      |> DateTime.to_date()
      |> Date.day_of_week()

    Web.Gettext.gettext("%{day_name}, %{day} %{month_name} %{year}",
      day_name: Gettext.dgettext(Web.Gettext, "dates", elem(@days_names, day_of_week - 1)),
      day: datetime.day,
      month_name: Gettext.dgettext(Web.Gettext, "dates", elem(@months_names, datetime.month - 1)),
      year: datetime.year
    )
  end

  def format_time(%{hour: hour, minute: minute, second: second}, seconds \\ true) do
    if seconds do
      [hour, minute, second]
    else
      [hour, minute]
    end
    |> Enum.map(&Integer.to_string(&1))
    |> Enum.map(&String.pad_leading(&1, 2, "0"))
    |> Enum.join(":")
  end

  def format_depth(depth), do: "#{Float.round(depth, 1)} km"

  def format_magnitude(magnitude), do: Float.round(magnitude, 1)

  def format_latitude(latitude), do: "#{Float.round(latitude, 2)}"

  def format_longitude(longitude), do: "#{Float.round(longitude, 2)}"

  def format_two_digits(value), do: :erlang.float_to_binary(value, decimals: 2)

  def change_locale_url(path, locale) do
    path |> String.split("/") |> List.replace_at(1, locale) |> Enum.join("/")
  end

  def automatic_tag(automatic) do
    if automatic do
      text = Web.Gettext.gettext("Automatic")
      "<span class=\"tag is-warning is-hidden-mobile\">#{text}</span>"
    else
      text = Web.Gettext.gettext("Validated")
      "<span class=\"tag is-info is-hidden-mobile\">#{text}</span>"
    end
    |> Phoenix.HTML.raw()
  end

  def automatic_icon(automatic) do
    if automatic do
      "<span class=\"icon\"><i class=\"fas fa-question-circle\"></i></span>"
    else
      "<span class=\"icon has-text-info\"><i class=\"fas fa-check-circle\"></i></span>"
    end
    |> Phoenix.HTML.raw()
  end

  def country_flag_path(conn, country_iso2) do
    country_iso2 = String.downcase(country_iso2)

    Routes.static_path(conn, "/images/country-flags/#{country_iso2}.svg")
  end

  def country_flag(conn, country_iso2) do
    path = country_flag_path(conn, country_iso2)

    '<span class="icon is-small"><img src="#{path}"></span>'
    |> Phoenix.HTML.raw()
  end

  def events_table(conn, locale, events, events_count, events_limit, previous_label, next_label) do
    current_page = Map.get(conn.params, "page", "1") |> String.to_integer()
    pages_count = (events_count / events_limit) |> Float.ceil() |> round()

    previous_link =
      cond do
        current_page < 2 ->
          nil

        current_page == 2 ->
          Phoenix.Controller.current_path(conn, Map.delete(conn.query_params, "page"))

        current_page > 2 ->
          Phoenix.Controller.current_path(
            conn,
            Map.put(conn.query_params, "page", current_page - 1)
          )
      end

    next_link =
      cond do
        current_page < pages_count ->
          Phoenix.Controller.current_path(
            conn,
            Map.put(conn.query_params, "page", current_page + 1)
          )

        current_page >= pages_count ->
          nil
      end

    Phoenix.View.render(Web.EventView, "events_table.html",
      conn: conn,
      locale: locale,
      events: events,
      next_label: next_label,
      next_link: next_link,
      previous_label: previous_label,
      previous_link: previous_link
    )
  end

  def markdown(body) do
    body
    |> Earmark.as_html!()
    |> Phoenix.HTML.raw()
  end

  def translate_event_type(event_type) do
    if event_type do
      Gettext.dgettext(Web.Gettext, "event_types", event_type)
    else
      Gettext.dgettext(Web.Gettext, "event_types", "event")
    end
  end

  def event_description(event) do
    event_type =
      event.event_type
      |> translate_event_type()
      |> String.capitalize()

    Web.Gettext.gettext(
      "%{event_type} of magnitude %{magnitude}, near of %{city}",
      city: event.distinctive_city.name,
      magnitude: format_magnitude(event.preferred_magnitude.magnitude),
      event_type: event_type
    )
  end
end
