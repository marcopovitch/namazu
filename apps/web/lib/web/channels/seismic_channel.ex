defmodule Web.SeismicChannel do
  use Phoenix.Channel

  def join("seismic:events", _message, socket) do
    {:ok, socket}
  end

  # def handle_in("lasts", %{"limit" => limit}, socket) do
  # 	# push socket, "lasts", %{"limit" => limit <> "!"}
  # 	push socket, "lasts", %{events: Namazu.Seismic.get_last_events(limit)}
  # 	{:noreply, socket}
  # # {:reply, :ok, %{"limit" => limit <> "!"}}
  # 	# {:reply, {:ok, %{"limit" => limit <> "!"}}, socket}
  # end
end
