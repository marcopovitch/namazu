# Namazu

A project to manage seismic and macroseismic data.

## Try it

### Using Docker

As Namazu docker images are not published yet, you'll have to build them by executing the folling commands :
```sh
docker build -t namazu .
docker build -t namazu-postgis -f Dockerfile.postgis .
```

To connect the namazu container to its database container, let's create a private network :
```
docker network create namazu-net
```

Now we can launch these two images, first let's launch the database image :
```
docker run -it --rm -p 127.0.0.1:5432:5432 -e POSTGRES_USER=namazu -v ~/Databases/namazu:/var/lib/postgresql/data --name namazu-postgis --network namazu-net namazu-postgis postgres -c log_min_duration_statement=1 -c shared_buffers=8GB
```

This command will store PostgresSQL data in **~/Databases/namazu** (schemas and tables are automatically created).

Next, launch namazu itself :
```
docker run -it --rm -p 4000:4000 -e NMZ_DATABASE=postgres://namazu@namazu-postgis/namazu -e SC3_DATABASE="postgres://renass_ro:renass_ro@db_server:5432/renass" --name namazu --network namazu-net namazu
```

### Manually

#### Install Elixir

Simply follow official instructions : https://elixir-lang.org/install.html

#### PostgreSQL and PostGIS

It can depends of your distribution, for the ones I use :
```
# Archlinux
sudo pacman -S  postgis

# Ubuntu 18.04
sudo apt install postgis
```

Create a PostgreSQL database with a user and a password and run the following script : [sql/create.sql](sql/create.sql).

#### Prepare the databases

##### Create zones (optional)

To display events through the web interfaces, you need to create some zones.
To try, you can import the example file :
```bash
psql -h 127.0.0.1 -U namazu -f sql/examples/zones.sql namazu
```

##### Import cities (optional)

As example, the way we import cities for France and the rest of the world :
```bash
# Convert AdminExpress shapefiles to GeoJSON
./scripts/admin-express-cities.sh ~/Downloads/ae/ADMIN-EXPRESS_2-0__SHP__FRA_2019-06-18 > ~/admin-express-cities.json
./scripts/admin-express-cheflieux.sh ~/Downloads/ae/ADMIN-EXPRESS_2-0__SHP__FRA_2019-06-18 > ~/admin-express-chef-lieux.json

# Then patch the cities by replacing the location by the official chef lieux
mix namazu.patch_admin_express ~/admin-express-cities.json ~/admin-express-chef-lieux.json > ~/admin-express-patched.json

# Import the patched file containing the French cities
mix namazu.import_cities ~/admin-express-patched.json

# Import cities in the rest of the world
mix namazu.import_cities --exclude-country-iso2=FR,RE,GF,YT,GP,MQ data/cities.json
```

##### Import pages (optional)
```bash
mix namazu.import_page --locale "fr" --slug "about" --title "A propos" ~/Projects/namazu/data/pages/about_fr.md
mix namazu.import_page --locale "en" --slug "about" --title "About" ~/Projects/namazu/data/pages/about_en.md
mix namazu.import_page --locale "fr" --slug "legal-notice" --title "Crédits et mentions légales" ~/Projects/namazu/data/pages/resif_legal_notice_fr.md
mix namazu.import_page --locale "en" --slug "legal-notice" --title "Credits and legal notice" ~/Projects/namazu/data/pages/resif_legal_notice_en.md
```



#### Run Namazu

Then clone this repository on your machine :
```
git clone https://github.com/resif/namazu.git
cd namazu
mix local.hex
mix local.rebar
mix deps.get
```

Finally, you're ready to launch an instance :
```
# For developement, with debugging enabled
mix phx.digest
# For production, optimizations enabled
MIX_ENV=prod mix phx.digest
```

## Contributing

### Translations

Extract strings and update .po files :
```
cd apps/web
mix gettext.extract
mix gettext.merge priv/gettext
```
