--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4
-- Dumped by pg_dump version 11.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: zones; Type: TABLE DATA; Schema: namazu; Owner: namazu
--

COPY namazu.zones (id, exclusive_economic_zone_id, name, slug, description, weight, show_manual, manual_minimal_magnitude, manual_maximal_depth, manual_minimal_used_phase_count, manual_event_types, show_automatic, automatic_minimal_magnitude, automatic_maximal_depth, automatic_minimal_used_phase_count, minimal_latitude, maximal_latitude, minimal_longitude, maximal_longitude) FROM stdin;
2	\N	Mayotte	mayotte	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	1	t	\N	\N	\N	{earthquake}	t	\N	\N	\N	-13.25	-12.25	44.75	45.75
3	\N	Monde	monde	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	0	t	5	\N	\N	{earthquake}	t	5	\N	\N	-90	90	-180	180
1	\N	France métropolitaine	france	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2	t	\N	\N	\N	{}	t	\N	40	7	40	52	-6	10
\.


--
-- Name: zones_id_seq; Type: SEQUENCE SET; Schema: namazu; Owner: namazu
--

SELECT pg_catalog.setval('namazu.zones_id_seq', 3, true);


--
-- PostgreSQL database dump complete
--

