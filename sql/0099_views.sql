CREATE SCHEMA IF NOT EXISTS views;

CREATE OR REPLACE VIEW views.seiscomp_events AS
SELECT e.id,
       e.event_type,
       s.event_publicid,
       o.time,
       m.magnitude,
       m.magnitude_type,
       o.latitude,
       o.latitude_uncertainty,
       o.longitude,
       o.longitude_uncertainty,
       o.depth,
       o.depth_type,
       o.earthmodel_id,
       o.automatic,
       q.standard_error,
       q.azimuthal_gap,
       q.minimum_distance,
       q.maximum_distance,
       q.used_phase_count,
       q.associated_phase_count,
       o.location,
       z.name,
       z.slug
FROM seismic.events AS e
JOIN seiscomp3.mappings s ON s.event_id = e.id
JOIN seismic.origins o ON o.id = e.preferred_origin_id
JOIN seismic.magnitudes m ON m.id = e.preferred_magnitude_id
LEFT OUTER JOIN seismic.origins_qualities q ON q.origin_id = o.id
LEFT OUTER JOIN namazu.zones z ON o.latitude >= z.minimal_latitude
                              AND o.latitude <= z.maximal_latitude
                              AND o.longitude >= z.minimal_longitude
                              AND o.longitude <= z.maximal_longitude;

CREATE OR REPLACE VIEW views.sync_stats AS
SELECT time AS origin_time,
       created_at - time AS seiscomp_latency,
       inserted_at - created_at AS sync_latency,
       inserted_at - time AS publication_latency,
       latitude,
       longitude
FROM seismic.origins;

CREATE OR REPLACE VIEW views.stations AS
SELECT s.id,
       s.station_code,
       n.network_code,
       p.period_start,
       p.period_end,
       p.latitude,
       p.longitude,
       location
FROM instruments.stations AS s
JOIN instruments.networks n ON s.network_id = n.id
JOIN instruments.stations_periods p ON p.station_id = s.id;

CREATE OR REPLACE VIEW views.missing_stations AS
SELECT s.network_code,
       s.station_code,
       min(p.time) AS min_time,
       max(p.time) AS max_time,
       count(*)
FROM seismic.picks AS p,
     instruments.stations_periods AS sp,
     seismic.streams AS s
LEFT OUTER JOIN instruments.stations st ON st.station_code = s.station_code
WHERE s.id = p.stream_id
  AND st IS NULL
   OR p.time <@ sp.period
GROUP BY s.network_code, s.station_code
ORDER BY count(*) DESC;

CREATE OR REPLACE VIEW views.arrivals_vs_used_phase_count AS
SELECT m.event_publicid AS sc3_publicid,
       q.origin_id AS nmz_origin_id,
       o.latitude AS latitude,
       o.longitude AS longitude,
       q.used_phase_count AS used_phase_count,
       count(a) AS arrival_count,
       count(a) - q.used_phase_count AS arrivals_minus_used_phase_count
FROM seismic.origins_qualities q,
     seismic.arrivals a,
     seiscomp3.mappings m,
     seismic.origins o
WHERE o.id = q.origin_id
  AND o.event_id = m.event_id
  AND q.origin_id = a.origin_id
  AND a.time_weight > 0
GROUP BY m.event_publicid,
         q.origin_id,
         o.latitude,
         o.longitude,
         q.used_phase_count
HAVING q.used_phase_count <> count(a);
