CREATE TABLE IF NOT EXISTS geography.geoname (
    geonameid INTEGER,
    name VARCHAR(200),
    asciiname VARCHAR(200),
    alternatenames VARCHAR(10000),
    latitude NUMERIC(8, 5),
    longitude NUMERIC(8, 5),
    feature_class VARCHAR(1),
    feature_code VARCHAR(10),
    country_code VARCHAR(2),
    cc2 VARCHAR(200),
    admin1_code VARCHAR(20),
    admin2_code VARCHAR(80),
    admin3_code VARCHAR(20),
    admin4_code VARCHAR(20),
    population BIGINT,
    elevation INTEGER,
    dem INTEGER,
    timezone VARCHAR(40),
    modification_date DATE
);

COMMENT ON COLUMN geography.geoname.geonameid IS 'integer id of record in geonames database';
COMMENT ON COLUMN geography.geoname.name IS 'name of geographical point (utf8) varchar(200)';
COMMENT ON COLUMN geography.geoname.asciiname IS 'name of geographical point in plain ascii characters, varchar(200)';
COMMENT ON COLUMN geography.geoname.alternatenames IS 'alternatenames, comma separated, ascii names automatically transliterated, convenience attribute from alternatename table, varchar(10000)';
COMMENT ON COLUMN geography.geoname.latitude IS 'latitude in decimal degrees (wgs84)';
COMMENT ON COLUMN geography.geoname.longitude IS 'longitude in decimal degrees (wgs84)';
COMMENT ON COLUMN geography.geoname.feature_class IS 'see http://www.geonames.org/export/codes.html, char(1)';
COMMENT ON COLUMN geography.geoname.feature_code IS 'see http://www.geonames.org/export/codes.html, varchar(10)';
COMMENT ON COLUMN geography.geoname.country_code IS 'ISO-3166 2-letter country code, 2 characters';
COMMENT ON COLUMN geography.geoname.cc2 IS 'alternate country codes, comma separated, ISO-3166 2-letter country code, 200 characters';
COMMENT ON COLUMN geography.geoname.admin1_code IS 'fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)';
COMMENT ON COLUMN geography.geoname.admin2_code IS 'code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80)';
COMMENT ON COLUMN geography.geoname.admin3_code IS 'code for third level administrative division, varchar(20)';
COMMENT ON COLUMN geography.geoname.admin4_code IS 'code for fourth level administrative division, varchar(20)';
COMMENT ON COLUMN geography.geoname.population IS 'bigint (8 byte int)';
COMMENT ON COLUMN geography.geoname.elevation IS 'in meters, integer';
COMMENT ON COLUMN geography.geoname.dem IS 'digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.';
COMMENT ON COLUMN geography.geoname.timezone IS 'the iana timezone id (see file timeZone.txt) varchar(40)';
COMMENT ON COLUMN geography.geoname.modification_date IS 'date of last modification in yyyy-MM-dd format';

CREATE TABLE IF NOT EXISTS geography.alternatenames (
    alternatenameid INTEGER,
    geonameid INTEGER,
    isolanguage VARCHAR(7),
    alternate_name VARCHAR(400),
    ispreferredname VARCHAR(1),
    isshortname VARCHAR(1),
    iscolloquial VARCHAR(1),
    ishistoric VARCHAR(1),
    "from" VARCHAR(80),
    "to" VARCHAR(80)
);

COMMENT ON TABLE geography.alternatenames IS 'alternate names with language codes and geonameId, file with iso language codes, with new columns from and to';
COMMENT ON COLUMN geography.alternatenames.alternatenameid IS 'the id of this alternate name, int';
COMMENT ON COLUMN geography.alternatenames.geonameid IS 'geonameId referring to id in table ''geoname'', int';
COMMENT ON COLUMN geography.alternatenames.isolanguage IS 'iso 639 language code 2- or 3-characters; 4-characters ''post'' for postal codes and ''iata'',''icao'' and faac for airport codes, fr_1793 for French Revolution names,  abbr for abbreviation, link to a website (mostly to wikipedia), wkdt for the wikidataid, varchar(7)';
COMMENT ON COLUMN geography.alternatenames.alternate_name IS 'alternate name or name variant, varchar(400)';
COMMENT ON COLUMN geography.alternatenames.ispreferredname IS '''1'', if this alternate name is an official/preferred name';
COMMENT ON COLUMN geography.alternatenames.isshortname IS '''1'', if this is a short name like ''California'' for ''State of California''';
COMMENT ON COLUMN geography.alternatenames.iscolloquial IS '''1'', if this alternate name is a colloquial or slang term. Example: ''Big Apple'' for ''New York''.';
COMMENT ON COLUMN geography.alternatenames.ishistoric IS '''1'', if this alternate name is historic and was used in the past. Example ''Bombay'' for ''Mumbai''.';
COMMENT ON COLUMN geography.alternatenames."from" IS 'from period when the name was used';
COMMENT ON COLUMN geography.alternatenames."to" IS 'to period when the name was used';

\copy geography.geoname from './$WDIR$/allCountries.txt' delimiter E'\t' NULL AS ''
\copy geography.alternatenames from './$WDIR$/alternateNamesV2.txt' delimiter E'\t' NULL AS ''

ALTER TABLE geography.geoname ADD PRIMARY KEY (geonameid);
ALTER TABLE geography.alternatenames ADD PRIMARY KEY (alternatenameid);
CREATE INDEX ON geography.alternatenames (geonameid);
CREATE INDEX ON geography.alternatenames (isolanguage);

/*
For a language, many traduction exists. The following query build the table `alternate_unique`
by choosing arbitrary (MIN(alternatenameid)) one of them.
*/
CREATE TABLE geography.alternate_unique AS
WITH t1 AS (SELECT * FROM geography.alternatenames),
t2 AS (SELECT * FROM t1
       WHERE t1.ispreferredname = '1'),
t3 AS (SELECT t1.isolanguage, MIN(t1.alternatenameid) AS alternatenameid FROM t1
       WHERE NOT EXISTS (SELECT 1 FROM t2
                         WHERE t2.geonameid = t1.geonameid AND t2.isolanguage = t1.isolanguage)
                         GROUP BY geonameid, isolanguage)

SELECT t2.geonameid, t2.isolanguage, t2.alternate_name FROM t2
UNION ALL
SELECT t1.geonameid, t1.isolanguage, t1.alternate_name FROM t1 INNER JOIN t3 ON t1.alternatenameid = t3.alternatenameid
;
