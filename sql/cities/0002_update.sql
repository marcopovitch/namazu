-- UPDATE country_iso2
UPDATE geography.cities SET country_iso2 = co.code_iso2
FROM geography.countries co
WHERE ST_CONTAINS (co.boundary, geography.cities.location)
AND geography.cities.country_iso2 = '-99'
;

-- UPDATE cities with ign data
WITH t1 AS (SELECT cl.gid,
                   cl.nom_chf,
                   co.population,
                   cl.geom
            FROM geography.chef_lieu_fr cl,
                 geography.com_fr co
            WHERE cl.insee_com = co.insee_com)

UPDATE geography.chef_lieu_fr
SET name_en = maj.name_en, name_de = maj.name_de, name_es = maj.name_es,
name_fr = maj.name_fr, name_pt = maj.name_pt, name_ru = maj.name_ru,
name_zh = maj.name_zh, name_ar = maj.name_ar, name_bn = maj.name_bn,
name_el = maj.name_el, name_hi = maj.name_hi, name_hu = maj.name_hu,
name_id = maj.name_id, name_it = maj.name_it, name_ja = maj.name_ja,
name_ko = maj.name_ko, name_nl = maj.name_nl, name_pl = maj.name_pl,
name_sv = maj.name_sv, name_tr = maj.name_tr, name_vi = maj.name_vi
FROM (SELECT t1.gid,
             oc.name_en, oc.name_de, oc.name_es,
             oc.name_fr, oc.name_pt, oc.name_ru,
             oc.name_zh, oc.name_ar, oc.name_bn,
             oc.name_el, oc.name_hi, oc.name_hu,
             oc.name_id, oc.name_it, oc.name_ja,
             oc.name_ko, oc.name_nl, oc.name_pl,
             oc.name_sv, oc.name_tr, oc.name_vi
      FROM geography.cities oc, t1
      WHERE ST_DWithin(oc.location, t1.geom, 10000, false)
      AND oc.name = t1.nom_chf) maj
WHERE geography.chef_lieu_fr.gid = maj.gid
;

DELETE FROM geography.cities WHERE country_iso2 IN ('FR', 'GP', 'MQ', 'GF', 'RE', 'YT');

INSERT INTO geography.cities (name, country_iso2, latitude, longitude, population,
name_en, name_de, name_es, name_fr, name_pt, name_ru, name_zh,
name_ar, name_bn, name_el, name_hi, name_hu, name_id, name_it,
name_ja, name_ko, name_nl, name_pl, name_sv, name_tr, name_vi,
location)
SELECT cl.nom_chf AS name,
'FR' AS country_iso2,
0 AS latitude,
0 AS longitude,
co.population,
cl.name_en, cl.name_de, cl.name_es, cl.name_fr, cl.name_pt, cl.name_ru, cl.name_zh,
cl.name_ar, cl.name_bn, cl.name_el, cl.name_hi, cl.name_hu, cl.name_id, cl.name_it,
cl.name_ja, cl.name_ko, cl.name_nl, cl.name_pl, cl.name_sv, cl.name_tr, cl.name_vi,
cl.geom AS location
FROM geography.chef_lieu_fr cl
INNER JOIN geography.com_fr co ON cl.insee_com = co.insee_com
;

-- UPDATE longitude and latitude
UPDATE geography.cities SET longitude = ST_X(geography.cities.location),
                            latitude = ST_Y(geography.cities.location)
;