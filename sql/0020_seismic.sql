CREATE SCHEMA IF NOT EXISTS seismic;

CREATE DOMAIN seismic.network_code AS character varying(2) NOT NULL;
CREATE DOMAIN seismic.station_code AS character varying(5) NOT NULL;
CREATE DOMAIN seismic.location_code AS character varying(2);
CREATE DOMAIN seismic.channel_code AS character varying(3);

CREATE TYPE seismic.depth_type AS ENUM (
  'from location',
  'from moment tensor inversion',
  'from modeling of broad-band P waveforms',
  'constrained by depth phases',
  'constrained by direct phases',
  'constrained by depth and direct phases',
  'operator assigned',
  'other'
);

CREATE TYPE seismic.evaluation_status AS ENUM (
  'confirmed',
  'final',
  'preliminary',
  'rejected',
  'reviewed'
);

CREATE TYPE seismic.event_type AS ENUM (
  'accidental explosion',
  'acoustic noise',
  'anthropogenic event',
  'atmospheric event',
  'avalanche',
  'blasting levee',
  'boat crash',
  'building collapse',
  'cavity collapse',
  'chemical explosion',
  'collapse',
  'controlled explosion',
  'crash',
  'debris avalanche',
  'earthquake',
  'experimental explosion',
  'explosion',
  'fluid extraction',
  'fluid injection',
  'hydroacoustic event',
  'ice quake',
  'induced', -- QuakeML : induced or triggered event
  'industrial explosion',
  'landslide',
  'meteorite',
  'mine collapse',
  'mining explosion',
  'not existing',
  'not locatable', -- Imported from SeisComp3
  'not reported',
  'nuclear explosion',
  'other event',
  'outside of network interest', -- Imported from SeisComp3
  'plane crash',
  'quarry blast',
  'reservoir loading',
  'road cut',
  'rock burst',
  'rockslide',
  'slide',
  'snow avalanche',
  'sonic blast',
  'sonic boom',
  'thunder',
  'train crash',
  'volcanic eruption'
);

CREATE TYPE seismic.origin_type AS ENUM (
  'amplitude',
  'centroid',
  'hypocenter',
  'macroseismic',
  'rupture end',
  'rupture start'
);

CREATE OR REPLACE FUNCTION seismic.set_event_timezone() RETURNS trigger AS $$
  DECLARE
    location geography;
  BEGIN
    IF NEW.preferred_origin_id IS NULL THEN
      RETURN NEW;
    END IF;

    SELECT o.location INTO STRICT location
      FROM seismic.origins o
     WHERE NEW.preferred_origin_id = o.id;

    NEW.timezone = geography.get_timezone(location);

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION seismic.set_event_distinctive_city() RETURNS trigger AS $$
  DECLARE
    location geography;
    city geography.cities%rowtype;
  BEGIN
    IF NEW.preferred_origin_id IS NULL OR (NEW.preferred_origin_id = OLD.preferred_origin_id AND OLD.distinctive_city_id IS NOT NULL) THEN
      RETURN NEW;
    END IF;

    SELECT o.location INTO STRICT location
      FROM seismic.origins o
     WHERE NEW.preferred_origin_id = o.id;

    SELECT * INTO city FROM geography.get_distinctive_city(location, 10);

    IF FOUND THEN
      NEW.distinctive_city_id = city.id;
      NEW.distinctive_city_distance = (ST_Distance(location, city.location, false) / 1000)::integer;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS seismic.events (
  id serial PRIMARY KEY,
  preferred_origin_id integer,
  preferred_magnitude_id integer,
  merged_with_event_id integer REFERENCES seismic.events(id),

  event_type seismic.event_type,
  -- m_typecertainty
  timezone character varying(30),

  distinctive_city_id integer REFERENCES geography.cities(id),
  distinctive_city_distance integer,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER events_set_timezone
  BEFORE INSERT OR UPDATE ON seismic.events
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_timezone();

CREATE TRIGGER events_set_distintive_city
  BEFORE INSERT OR UPDATE ON seismic.events
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_distinctive_city();

CREATE TRIGGER events_set_updated_time
  BEFORE UPDATE ON seismic.events
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE INDEX events_preferred_origin_id_idx ON seismic.events(preferred_origin_id);
CREATE INDEX events_preferred_magnitude_idx ON seismic.events(preferred_magnitude_id);

CREATE TABLE IF NOT EXISTS seismic.streams (
  id serial PRIMARY KEY,

  network_code seismic.network_code,
  station_code seismic.station_code,
  location_code seismic.location_code,
  channel_code seismic.channel_code,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER streams_set_updated_time
  BEFORE UPDATE ON seismic.streams
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE UNIQUE INDEX streams_unique_location_code_not_null_idx ON seismic.streams(network_code, station_code, location_code, channel_code) WHERE location_code IS NOT NULL;
CREATE UNIQUE INDEX streams_unique_location_code_null_idx ON seismic.streams(network_code, station_code, channel_code) WHERE location_code IS NULL;

CREATE TABLE IF NOT EXISTS seismic.picks (
  id serial PRIMARY KEY,
  stream_id integer NOT NULL REFERENCES seismic.streams(id),

  "time" timestamp with time zone NOT NULL,
  -- m_time_uncertainty double precision,
  time_lower_uncertainty double precision,
  time_upper_uncertainty double precision,
  -- m_time_confidencelevel
  -- m_waveformid_resourceuri
  filter_id character varying(255),
  method_id character varying(255),
  -- m_horizontalslowness_value
  -- m_horizontalslowness_uncertainty
  -- m_horizontalslowness_loweruncertainty
  -- m_horizontalslowness_upperuncertainty
  -- m_horizontalslowness_confidencelevel
  -- m_horizontalslowness_used
  -- m_backazimuth_value
  -- m_backazimuth_uncertainty
  -- m_backazimuth_loweruncertainty
  -- m_backazimuth_upperuncertainty
  -- m_backazimuth_confidencelevel
  -- m_backazimuth_used
  -- m_slownessmethodid
  -- m_onset
  phase_hint_code character varying(4) NOT NULL,
  phase_hint_used boolean NOT NULL,
  polarity character varying(255),
  -- m_evaluationstatus
  automatic boolean,
  agency character varying(255),
  author character varying(255),

  created_at timestamp with time zone,
  inserted_at timestamp with time zone DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER picks_set_updated_time
  BEFORE UPDATE ON seismic.picks
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE INDEX picks_time_idx ON seismic.picks("time");

CREATE TABLE IF NOT EXISTS seismic.origins (
  id serial PRIMARY KEY,
  event_id integer NOT NULL REFERENCES seismic.events(id),

  "time" timestamp with time zone NOT NULL,
  time_uncertainty double precision,
  -- m_time_loweruncertainty
  -- m_time_upperuncertainty
  -- m_time_confidencelevel
  latitude double precision NOT NULL,
  latitude_uncertainty double precision,
  -- m_latitude_loweruncertainty
  -- m_latitude_upperuncertainty
  -- m_latitude_confidencelevel
  longitude double precision NOT NULL,
  longitude_uncertainty double precision,
  -- m_longitude_loweruncertainty
  -- m_longitude_upperuncertainty
  -- m_longitude_confidencelevel
  depth double precision NOT NULL,
  depth_uncertainty double precision,
  -- m_depth_loweruncertainty
  -- m_depth_upperuncertainty
  -- m_depth_confidencelevel
  depth_used boolean NOT NULL DEFAULT false,
  depth_type seismic.depth_type NOT NULL,
  -- m_timefixed
  -- m_epicenterfixed
  -- m_referencesystemid
  method_id character varying(255),
  earthmodel_id character varying(255),
  -- m_type
  automatic boolean,
  evaluation_status seismic.evaluation_status,
  origin_type seismic.origin_type NOT NULL,
  location geography(Point, 4326),
  agency character varying(255),
  author character varying(255),

  created_at timestamp with time zone,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER origins_set_updated_time
  BEFORE UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE OR REPLACE FUNCTION seismic.manage_merged_event() RETURNS trigger AS $$
  DECLARE
    origin_count integer;
  BEGIN
    IF OLD.event_id <> NEW.event_id THEN
      SELECT count(*) INTO origin_count FROM seismic.origins WHERE id <> OLD.id AND event_id = OLD.event_id;

      IF origin_count = 0 THEN
        UPDATE seismic.events SET event_type = 'not existing', merged_with_event_id = NEW.event_id WHERE id = OLD.event_id;
      END IF;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER origins_manage_merged_event
  BEFORE UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE seismic.manage_merged_event();

CREATE INDEX origins_coordinates_idx ON seismic.origins(longitude, latitude);
CREATE INDEX origins_time_idx ON seismic.origins("time");
CREATE INDEX origins_depth_idx ON seismic.origins(depth);
CREATE INDEX origins_automatic_idx ON seismic.origins(automatic);
CREATE INDEX origins_location_idx ON seismic.origins USING GIST(location);

CREATE TRIGGER origin_set_location
BEFORE INSERT OR UPDATE ON seismic.origins
FOR EACH ROW EXECUTE PROCEDURE namazu.set_location();

CREATE TABLE IF NOT EXISTS seismic.origins_qualities (
  origin_id integer NOT NULL UNIQUE REFERENCES seismic.origins(id) ON DELETE CASCADE,

  associated_phase_count integer,
  used_phase_count smallint,
  associated_station_count smallint,
  used_station_count smallint,
  -- m_quality_depthphasecount
  standard_error double precision,
  azimuthal_gap double precision,
  -- m_quality_secondaryazimuthalgap
  -- m_quality_groundtruthlevel
  maximum_distance double precision,
  minimum_distance double precision,
  median_distance double precision,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER origins_qualities_set_updated_time
  BEFORE UPDATE ON seismic.origins_qualities
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS seismic.origins_uncertainties (
  origin_id integer NOT NULL UNIQUE REFERENCES seismic.origins(id) ON DELETE CASCADE,

  horizontal double precision,
  minimum_horizontal double precision,
  maximum_horizontal double precision,
  -- m_uncertainty_azimuthmaxhorizontaluncertainty
  confidenceellipsoid_semimajoraxislength double precision,
  confidenceellipsoid_semiminoraxislength double precision,
  confidenceellipsoid_semiintermediateaxislength double precision,
  confidenceellipsoid_majoraxisplunge double precision,
  confidenceellipsoid_majoraxisazimuth double precision,
  confidenceellipsoid_majoraxisrotation double precision,
  confidenceellipsoid_used boolean NOT NULL DEFAULT false,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER origins_uncertainties_set_updated_time
  BEFORE UPDATE ON seismic.origins_uncertainties
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS seismic.arrivals (
  id serial PRIMARY KEY,
  origin_id integer NOT NULL REFERENCES seismic.origins(id) ON DELETE CASCADE,
  pick_id integer NOT NULL REFERENCES seismic.picks(id),

  phase_code character varying(255) NOT NULL,
  -- m_timecorrection
  azimuth double precision,
  distance double precision,
  -- m_takeoffangle
  time_residual double precision,
  -- m_horizontalslownessresidual
  -- m_backazimuthresidual
  -- m_timeused
  -- m_horizontalslownessused
  -- m_backazimuthused
  time_weight double precision,
  -- m_earthmodelid
  -- m_preliminary

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE (origin_id, pick_id)
);

CREATE TRIGGER arrivals_set_updated_time
  BEFORE UPDATE ON seismic.arrivals
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS seismic.amplitudes (
  id serial PRIMARY KEY,
  pick_id integer NOT NULL REFERENCES seismic.picks(id),
  stream_id integer NOT NULL REFERENCES seismic.streams(id),

  amplitude double precision NOT NULL,
  amplitude_type character varying(16) NOT NULL,
  -- m_amplitude_loweruncertainty
  -- m_amplitude_upperuncertainty
  -- m_amplitude_confidencelevel
  -- m_amplitude_used
  time_window_reference timestamp with time zone NOT NULL,
  time_window_begin double precision NOT NULL,
  time_window_end double precision NOT NULL,
  time_window_used boolean NOT NULL DEFAULT false,
  period_value double precision,
  -- m_period_uncertainty
  -- m_period_loweruncertainty
  -- m_period_upperuncertainty
  -- m_period_confidencelevel
  period_used boolean NOT NULL DEFAULT false,
  snr double precision,
  -- m_unit
  -- m_waveformid_resourceuri
  waveformid_used boolean NOT NULL DEFAULT false,
  filter_id character varying(255),
  -- m_methodid
  -- m_scalingtime_value
  -- m_scalingtime_uncertainty
  -- m_scalingtime_loweruncertainty
  -- m_scalingtime_upperuncertainty
  -- m_scalingtime_confidencelevel
  -- m_scalingtime_used
  -- m_magnitudehint
  automatic boolean,

  created_at timestamp with time zone,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX amplitudes_pick_id_idx ON seismic.amplitudes(pick_id);
CREATE INDEX amplitudes_stream_id_idx ON seismic.amplitudes(stream_id);

CREATE TRIGGER amplitudes_set_updated_time
  BEFORE UPDATE ON seismic.amplitudes
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS seismic.stationmagnitudes (
  id serial PRIMARY KEY,
  amplitude_id integer NOT NULL REFERENCES seismic.amplitudes(id),
  stream_id integer NOT NULL REFERENCES seismic.streams(id),

  magnitude double precision NOT NULL,
  -- m_magnitude_uncertainty
  -- m_magnitude_loweruncertainty
  -- m_magnitude_upperuncertainty
  -- m_magnitude_confidencelevel
  magnitude_type character varying(255) NOT NULL,
  -- m_methodid
  -- m_waveformid_resourceuri
  -- m_waveformid_used

  created_at timestamp with time zone,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER stationmagnitudes_set_updated_time
  BEFORE UPDATE ON seismic.stationmagnitudes
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE INDEX stationmagnitudes_amplitude_id_idx ON seismic.stationmagnitudes(amplitude_id);
CREATE INDEX stationmagnitudes_stream_id_idx ON seismic.stationmagnitudes(stream_id);

CREATE TABLE IF NOT EXISTS seismic.magnitudes (
  id serial PRIMARY KEY,
  origin_id integer NOT NULL REFERENCES seismic.origins(id),

  magnitude double precision NOT NULL,
  magnitude_uncertainty double precision,
  -- m_magnitude_uncertainty
  -- m_magnitude_loweruncertainty
  -- m_magnitude_upperuncertainty
  -- m_magnitude_confidencelevel
  magnitude_type character varying(255) NOT NULL,
  method_id character varying(255),
  station_count smallint,
  -- m_azimuthalgap
  evaluation_status seismic.evaluation_status,
  agency character varying(255),
  author character varying(255),

  created_at timestamp with time zone,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER magnitudes_set_updated_time
  BEFORE UPDATE ON seismic.magnitudes
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE INDEX magnitudes_origin_id_idx ON seismic.magnitudes(origin_id);
CREATE INDEX magnitudes_magnitude_idx ON seismic.magnitudes(magnitude);

CREATE TABLE IF NOT EXISTS seismic.stationmagnitudes_contributions (
  id serial PRIMARY KEY,
  magnitude_id integer NOT NULL REFERENCES seismic.magnitudes(id) ON DELETE CASCADE,
  station_magnitude_id integer NOT NULL REFERENCES seismic.stationmagnitudes(id) ON DELETE CASCADE,

  residual double precision,
  weight double precision NOT NULL,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER stationmagnitudes_contributions_set_updated_time
  BEFORE UPDATE ON seismic.stationmagnitudes_contributions
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE INDEX stationmagnitudes_contributions_magnitude_id_idx ON seismic.stationmagnitudes_contributions(magnitude_id);
CREATE INDEX stationmagnitudes_contributions_station_magnitude_id_idx ON seismic.stationmagnitudes_contributions(station_magnitude_id);

ALTER TABLE seismic.events ADD FOREIGN KEY(preferred_origin_id) REFERENCES seismic.origins(id);
ALTER TABLE seismic.events ADD FOREIGN KEY(preferred_magnitude_id) REFERENCES seismic.magnitudes(id);
