CREATE SCHEMA IF NOT EXISTS seiscomp3;

CREATE TABLE IF NOT EXISTS seiscomp3.states (
    table_name varchar PRIMARY KEY,
    last_oid bigint NOT NULL,
    last_modified timestamp with time zone NOT NULL
);

CREATE TABLE IF NOT EXISTS seiscomp3.mappings (
    event_oid bigint PRIMARY KEY,

    event_id integer NOT NULL UNIQUE REFERENCES seismic.events(id) ON DELETE CASCADE,
    event_publicid varchar
);
