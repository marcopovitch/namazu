- Load from PostGIS : `geography.v_tz` and `geography.v_gmt` layers
- Execute the grass command *v.generalize* ([doc](1)) with `lang` algorithm and `100` for threshold.

  Complete list of parameters :
  ```sh
  { '-l' : True, '-t' : False,   'GRASS_MIN_AREA_PARAMETER' : 0.0001,   'GRASS_OUTPUT_TYPE_PARAMETER' : 0,   'GRASS_REGION_PARAMETER' : None,   'GRASS_SNAP_TOLERANCE_PARAMETER' : -1,   'GRASS_VECTOR_DSCO' : '', 'GRASS_VECTOR_EXPORT_NOCAT'   : False, 'GRASS_VECTOR_LCO' : '', 'alpha' : 1,   'angle_thresh' : 3, 'beta' : 1, 'betweeness_thresh' :   0, 'cats' : '', 'closeness_thresh' : 0,   'degree_thresh' : 0, 'error' : 'TEMPORARY_OUTPUT',   'input' : 'dbname=\'namazu-test\' host=10.0.0.12   port=5432 user=\'namazu-test\' sslmode=disable   key=\'id\' srid=3857 type=MultiPolygon   table=\"geography\".\"v_tz\" (geom) sql=',   'iterations' : 1, 'look_ahead' : 7, 'method' : 2,   'output' : 'TEMPORARY_OUTPUT', 'reduction' : 50,   'slide' : 0.5, 'threshold' : 100, 'type' : [0,1,2],   'where' : '' }
  ```
- Delete all fields on output except `fid` and `tzid`
- Create a buffer at 0 for correct all invalid geometries
- Reproject output layer to WGS 84 (EPSG:4326).
- *Dissolve* ([doc](2)) algorithm on `tzid` field
- Save it > `output_2`
- Make the *Difference* ([doc](3)) algorithm with input: `v_gmt`, overlay: `output_2`
- Copy features from the last difference output to `output_2`

[1]: https://grass.osgeo.org/grass76/manuals/v.generalize.html

[2]: https://docs.qgis.org/testing/en/docs/user_manual/processing_algs/qgis/vectorgeometry.html#qgisdissolve

[3]: https://docs.qgis.org/testing/en/docs/user_manual/processing_algs/qgis/vectoroverlay.html#qgisdifference

Annexes:

```sql
-- geography.v_gmt
DROP VIEW IF EXISTS geography.v_gmt;
CREATE VIEW geography.v_gmt AS
WITH l AS (
  SELECT -180 AS l
  UNION ALL
  SELECT generate_series(-172.5, 172.5, 15)
  UNION ALL
  SELECT 180
),

c AS (
  SELECT l.l AS l_0,
  lead(l.l) OVER() AS l_1
  FROM l
  LIMIT 25),

g AS (
  SELECT ROW_NUMBER() OVER () AS id,
  ST_GeomFromText('POLYGON(('
    || c.l_0 || ' 90, '
    || c.l_1 || ' 90, '
    || c.l_1 || ' -90, '
    || c.l_0 || ' -90, '
    || c.l_0 || ' 90))', 4326) AS geom
  FROM c)

SELECT g.id,
CASE
  WHEN 13 - g.id > 0 THEN 'Etc/GMT+' || 13 - g.id
  WHEN 13 - g.id = 0 THEN 'Etc/GMT'
  ELSE 'Etc/GMT' || 13 - g.id
END AS tzid,
g.geom
FROM g
;

-- geography.v_tz
DROP VIEW IF EXISTS geography.v_tz;
CREATE VIEW geography.v_tz AS
SELECT ROW_NUMBER() OVER () AS id,
timezone_id AS tzid,
ST_TRANSFORM(boundary::geometry, 3857) AS geom
FROM geography.timezones
WHERE timezone_id NOT LIKE 'Etc/GMT%'
;
```
