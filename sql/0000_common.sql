CREATE EXTENSION postgis;
CREATE EXTENSION btree_gist;
CREATE EXTENSION fuzzystrmatch;

CREATE SCHEMA IF NOT EXISTS namazu;

CREATE OR REPLACE FUNCTION namazu.set_location() RETURNS trigger AS $$
  BEGIN
    IF (NEW.latitude IS NOT NULL) AND (NEW.longitude IS NOT NULL) THEN
      NEW.location := ST_SetSRID(ST_Point(NEW.longitude, NEW.latitude), 4326)::geography;
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.set_period() RETURNS trigger AS $$
  BEGIN
    IF NEW.period_end IS NOT NULL THEN
      NEW.period := '[' || NEW.period_start || ',' || NEW.period_end || ')';
    ELSE
      NEW.period := '[' || NEW.period_start || ',)';
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION namazu.set_updated_time() RETURNS trigger AS $$
  BEGIN
    NEW.updated_at := current_timestamp;
    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS namazu.parameters (
  key varchar(50) NOT NULL PRIMARY KEY,
  value varchar NOT NULL
);

CREATE TABLE IF NOT EXISTS namazu.actors (
    id serial PRIMARY KEY,
    name varchar NOT NULL UNIQUE,
    acronym varchar UNIQUE,
    slug varchar NOT NULL UNIQUE,
    country_iso2 varchar(3) NOT NULL,
    url varchar,
    description varchar
);

CREATE SCHEMA IF NOT EXISTS web;

CREATE TABLE IF NOT EXISTS web.pages (
    id serial PRIMARY KEY,
    title varchar NOT NULL,
    locale varchar NOT NULL,
    slug varchar NOT NULL,
    content text NOT NULL,
    published boolean NOT NULL DEFAULT false,
    UNIQUE (locale, slug)
);
