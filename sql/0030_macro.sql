CREATE SCHEMA IF NOT EXISTS macro;

CREATE TYPE macro.address_source AS ENUM (
  'gps',
  'witness'
);

CREATE TYPE macro.question_type AS ENUM (
  'integer',
  'string',
  'image',
  'level'
);

CREATE TYPE macro.form_type AS ENUM (
  'city',
  'individual'
);

CREATE TABLE IF NOT EXISTS macro.forms (
  id serial PRIMARY KEY,

  name varchar NOT NULL,
  "type" macro.form_type NOT NULL,
  "default" boolean NOT NULL DEFAULT FALSE,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

/* CREATE UNIQUE INDEX unique_default_form ON macro.forms(type, "default") WHERE "default"; */

CREATE TRIGGER forms_set_updated_time
  BEFORE UPDATE ON macro.forms
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS macro.questions (
  id serial PRIMARY KEY,
  form_id integer NOT NULL REFERENCES macro.forms(id) ON DELETE CASCADE,

  type macro.question_type NOT NULL,
  multiple boolean NOT NULL DEFAULT FALSE,
  "order" smallint NOT NULL,
  text varchar NOT NULL,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(form_id, "order")
);

CREATE TRIGGER questions_set_updated_time
  BEFORE UPDATE ON macro.questions
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS macro.questions_choices (
  id serial PRIMARY KEY,
  question_id integer NOT NULL REFERENCES macro.questions(id) ON DELETE CASCADE,

  value varchar,
  "order" smallint NOT NULL,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone

  /* UNIQUE(question_id, "order") */
);

CREATE TRIGGER questions_choices_set_updated_time
  BEFORE UPDATE ON macro.questions_choices
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS macro.questions_conditions (
  id serial PRIMARY KEY,
  question_id integer NOT NULL REFERENCES macro.questions(id) ON DELETE CASCADE,
  question_choice_id integer NOT NULL REFERENCES macro.questions_choices(id) ON DELETE CASCADE,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(question_id, question_choice_id)
);

CREATE TRIGGER questions_conditions_set_updated_time
  BEFORE UPDATE ON macro.questions_conditions
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS macro.testimonies (
  id serial PRIMARY KEY,
  form_id integer REFERENCES macro.forms(id),
  event_id integer REFERENCES seismic.events(id),

  preferred_intensity_id integer,
  key uuid NOT NULL,
  naive_felt_time timestamp without time zone,
  utc_offset smallint,
  quality double precision NOT NULL, -- Enum poor, very poor, great
  author varchar,
  address varchar,
  address_source macro.address_source NOT NULL,
  geocoding_type varchar,
  latitude double precision,
  longitude double precision,
  location geography(Point, 4326),

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER testimony_set_location
  BEFORE INSERT OR UPDATE ON macro.testimonies
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_location();

CREATE TRIGGER testimonies_set_updated_time
  BEFORE UPDATE ON macro.testimonies
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS macro.answers (
  id serial PRIMARY KEY,
  question_choice_id integer NOT NULL REFERENCES macro.questions_choices(id),
  testimony_id integer NOT NULL REFERENCES macro.testimonies(id) ON DELETE CASCADE,

  value varchar,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(testimony_id, question_choice_id)
);

CREATE TRIGGER answers_set_updated_time
  BEFORE UPDATE ON macro.answers
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS macro.intensities (
  id serial PRIMARY KEY,
  event_id integer REFERENCES seismic.events(id),

  intensity double precision NOT NULL,
  intensity_lower_uncertainty double precision,
  intensity_upper_uncertainty double precision,
  intensity_type varchar NOT NULL,
  method varchar NOT NULL, -- imagette ?
  evaluation_status varchar, -- Enum ?
  quality double precision,
  automatic boolean NOT NULL,
  author varchar, -- public.authors ?
  notes text,
  city_source varchar(40),
  city_source_id varchar(40),
  latitude double precision,
  longitude double precision,
  location geography(Point, 4326),

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER intensities_set_location
  BEFORE INSERT OR UPDATE ON macro.intensities
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_location();

CREATE TRIGGER intensities_set_updated_time
  BEFORE UPDATE ON macro.intensities
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

ALTER TABLE macro.testimonies ADD FOREIGN KEY(preferred_intensity_id) REFERENCES macro.intensities(id);

CREATE TABLE IF NOT EXISTS macro.testimonies_contributions (
  id serial PRIMARY KEY,
  testimony_id integer NOT NULL REFERENCES macro.testimonies(id),
  intensity_id integer NOT NULL REFERENCES macro.intensities(id),

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE TRIGGER testimonies_contributions_set_updated_time
  BEFORE UPDATE ON macro.testimonies_contributions
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE IF NOT EXISTS macro.intensities_contributions (
  parent_intensity_id integer NOT NULL REFERENCES macro.intensities(id),
  intensity_id integer NOT NULL REFERENCES macro.intensities(id),

  UNIQUE (parent_intensity_id, intensity_id)
);
