CREATE SCHEMA IF NOT EXISTS geography;

CREATE TYPE geography.city_location_type AS ENUM (
  'centroid',
  'chef-lieu'
);

CREATE TABLE geography.countries (
  id serial PRIMARY KEY,

  code_iso2 varchar(3) NOT NULL,
  code_iso3 varchar(3) NOT NULL,

  source varchar(40) NOT NULL,
  source_version varchar(40) NOT NULL,
  source_id varchar(40) NOT NULL,

  name varchar NOT NULL UNIQUE,
  name_ar varchar,
  name_bn varchar,
  name_de varchar,
  name_el varchar,
  name_en varchar,
  name_es varchar,
  name_fr varchar,
  name_hi varchar,
  name_hu varchar,
  name_id varchar,
  name_it varchar,
  name_ja varchar,
  name_ko varchar,
  name_nl varchar,
  name_pl varchar,
  name_pt varchar,
  name_ru varchar,
  name_sv varchar,
  name_tr varchar,
  name_vi varchar,
  name_zh varchar,

  boundary geography(MultiPolygon, 4326) NOT NULL,

  created_at timestamp with time zone,
  inserted_at timestamp with time zone DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX countries_boundary_idx ON geography.countries USING GIST(boundary);

CREATE TRIGGER countries_set_updated_time
  BEFORE UPDATE ON geography.countries
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE geography.exclusive_economic_zones (
  id serial PRIMARY KEY,

  code_iso3 varchar(5) NOT NULL,
  country_name varchar NOT NULL,

  boundary geography(MultiPolygon, 4326) NOT NULL,
  buffer integer NOT NULL DEFAULT 0,

  created_at timestamp with time zone,
  inserted_at timestamp with time zone DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX exclusive_economic_zones_boundary_idx ON geography.exclusive_economic_zones USING GIST(boundary);

CREATE TRIGGER exclusive_economic_zones_set_updated_time
  BEFORE UPDATE ON geography.exclusive_economic_zones
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TABLE geography.cities (
  id serial PRIMARY KEY,
  country_id integer REFERENCES geography.countries(id),

  country_iso2 varchar(3) NOT NULL,
  district boolean NOT NULL DEFAULT false,
  administrative_code_type varchar(10),
  administrative_code varchar(10),
  population integer NOT NULL,

  source varchar(40) NOT NULL,
  source_version varchar(40) NOT NULL,
  source_id varchar(40) NOT NULL,
  geoname_id integer,

  latitude double precision NOT NULL,
  longitude double precision NOT NULL,
  location_type geography.city_location_type NOT NULL DEFAULT 'centroid',
  location geography(Point, 4326) NOT NULL,
  boundary geography(MultiPolygon, 4326),

  period_start timestamp with time zone,
  period_end timestamp with time zone,
  period tstzrange,

  name varchar NOT NULL,
  name_ar varchar,
  name_bn varchar,
  name_de varchar,
  name_en varchar,
  name_es varchar,
  name_fr varchar,
  name_el varchar,
  name_hi varchar,
  name_hu varchar,
  name_id varchar,
  name_it varchar,
  name_ja varchar,
  name_ko varchar,
  name_nl varchar,
  name_pl varchar,
  name_pt varchar,
  name_ru varchar,
  name_sv varchar,
  name_tr varchar,
  name_vi varchar,
  name_zh varchar,

  inserted_at timestamp with time zone DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX cities_location_idx ON geography.cities USING GIST(location);
CREATE INDEX cities_boundary_idx ON geography.cities USING GIST(boundary);

CREATE TRIGGER cities_set_updated_time
  BEFORE UPDATE ON geography.cities
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE FUNCTION geography.set_geoms() RETURNS trigger AS $$
  BEGIN
    IF (NEW.latitude IS NOT NULL) AND (NEW.longitude IS NOT NULL) THEN
      NEW.location := ST_SetSRID(ST_Point(NEW.longitude, NEW.latitude), 4326)::geography;
    ELSIF NEW.boundary IS NOT NULL THEN
      NEW.location := public.ST_Centroid(NEW.boundary);
      NEW.location_type := 'centroid';
    END IF;

    IF (NEW.location IS NOT NULL) THEN
      NEW.latitude := public.ST_Y(NEW.location::geometry);
      NEW.longitude := public.ST_X(NEW.location::geometry);
    END IF;

    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER cities_set_period
  BEFORE INSERT OR UPDATE ON geography.cities
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_period();

CREATE TRIGGER cities_set_geoms
  BEFORE INSERT OR UPDATE ON geography.cities
  FOR EACH ROW EXECUTE PROCEDURE geography.set_geoms();

CREATE TABLE IF NOT EXISTS geography.timezones (
    timezone_id varchar NOT NULL UNIQUE,
    boundary geography(MultiPolygon, 4326) NOT NULL,

    inserted_at timestamp with time zone DEFAULT current_timestamp,
    updated_at timestamp with time zone
);

CREATE INDEX IF NOT EXISTS timezones_boundary_idx ON geography.timezones USING GIST(boundary);

CREATE TRIGGER timezones_set_updated_time
  BEFORE UPDATE ON geography.timezones
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE OR REPLACE FUNCTION geography.get_timezone(location geography) RETURNS varchar AS $$
  DECLARE
    timezone_id varchar;
  BEGIN
    SELECT t.timezone_id INTO timezone_id
      FROM geography.timezones t
     WHERE st_covers(t.boundary, location)
       AND t.timezone_id NOT LIKE 'Etc/GMT%';

    IF FOUND THEN
      RETURN timezone_id;
    END IF;

    SELECT t.timezone_id INTO timezone_id
      FROM geography.timezones t
     WHERE t.timezone_id NOT LIKE 'Etc/GMT%'
    ORDER BY st_distance(t.boundary, location)
    LIMIT 1;

    IF FOUND THEN
      RETURN timezone_id;
    END IF;

    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION geography.get_nearest_cities(loc geography, lim integer) RETURNS SETOF geography.cities AS $$
  DECLARE
    r geography.cities%rowtype;
  BEGIN
    FOR r IN SELECT * FROM geography.cities c WHERE c.period_end IS NULL ORDER BY c.location <-> loc LIMIT lim
    LOOP
      RETURN NEXT r;
    END LOOP;
    RETURN;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION geography.get_distinctive_city(loc geography, lim integer) RETURNS geography.cities AS $$
  DECLARE
    r geography.cities%rowtype;
  BEGIN
    SELECT * FROM geography.get_nearest_cities(loc, lim) c ORDER BY c.population DESC LIMIT 1 INTO r;

    IF FOUND THEN
      RETURN r;
    END IF;

    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;
