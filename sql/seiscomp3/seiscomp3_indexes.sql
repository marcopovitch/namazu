CREATE INDEX ON arrival(_last_modified);
CREATE INDEX ON event(_last_modified);
CREATE INDEX ON magnitude(_last_modified);
CREATE INDEX ON origin(_last_modified);
CREATE INDEX ON originreference(_last_modified);
