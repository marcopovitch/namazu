
CREATE TABLE IF NOT EXISTS namazu.zones (
    id serial PRIMARY KEY,
    exclusive_economic_zone_id integer REFERENCES geography.exclusive_economic_zones(id),

    name varchar NOT NULL UNIQUE,
    slug varchar NOT NULL UNIQUE,
    description varchar,
    introduction varchar,
    weight smallint DEFAULT 0 NOT NULL,
    show_manual boolean DEFAULT true NOT NULL,
    manual_minimal_magnitude double precision,
    manual_maximal_depth double precision,
    manual_minimal_used_phase_count smallint,
    manual_event_types seismic.event_type[] NOT NULL DEFAULT array['earthquake']::seismic.event_type[],
    show_automatic boolean DEFAULT false NOT NULL,
    automatic_minimal_magnitude double precision,
    automatic_maximal_depth double precision,
    automatic_minimal_used_phase_count smallint,
    minimal_latitude double precision NOT NULL,
    maximal_latitude double precision NOT NULL,
    minimal_longitude double precision NOT NULL,
    maximal_longitude double precision NOT NULL
);
