# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# By default, the umbrella project as well as each child
# application will require this configuration file, ensuring
# they all use the same configuration. While one could
# configure all applications here, we prefer to delegate
# back to each application for organization purposes.
import_config "../apps/*/config/config.exs"

config :namazu, Namazu.Repo,
  url: System.get_env("NMZ_DATABASE") || "postgres://namazu@127.0.0.1:5432/namazu",
  types: Namazu.PostgresTypes # Add Postgis support


config :seiscomp3, Seiscomp3.Repo,
  url: System.get_env("SC3_DATABASE") || "postgres://postgres@127.0.0.1:5433/postgres",
  timeout: 60_000

if Mix.env() == :prod, do: config(:logger, level: :info)
