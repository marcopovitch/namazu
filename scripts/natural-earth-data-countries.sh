#!/bin/sh

ned_dir=$1
ned_version=$(sed -e 's/\r$//' $1/ne_10m_admin_0_countries.VERSION.txt)

ogr2ogr -f GeoJSON /vsistdout/ $ned_dir/ne_10m_admin_0_countries.shp | jq """
{
  type: .type,
  name: .name,
  crs: .crs,
  features: [.features[] |
  {
    type: \"Feature\",
    geometry: .geometry,
    properties: {
      code_iso2: .properties.ISO_A2|tostring,
      code_iso3: .properties.ISO_A3|tostring,
      source: \"NaturalEarthData\",
      source_version: \"$ned_version\",
      source_id: (.properties.NE_ID)|tostring,
      name: .properties.NAME,
      name_ar: .properties.NAME_AR,
      name_bn: .properties.NAME_BN,
      name_de: .properties.NAME_DE,
      name_en: .properties.NAME_EN,
      name_es: .properties.NAME_ES,
      name_fr: .properties.NAME_FR,
      name_el: .properties.NAME_EL,
      name_hi: .properties.NAME_HI,
      name_hu: .properties.NAME_HU,
      name_id: .properties.NAME_ID,
      name_it: .properties.NAME_IT,
      name_ja: .properties.NAME_JA,
      name_ko: .properties.NAME_KO,
      name_nl: .properties.NAME_NL,
      name_pl: .properties.NAME_PL,
      name_pt: .properties.NAME_PT,
      name_ru: .properties.NAME_RU,
      name_sv: .properties.NAME_SV,
      name_tr: .properties.NAME_TR,
      name_vi: .properties.NAME_VI,
      name_zh: .properties.NAME_ZH
    }
  }]
}
"""
