#!/bin/sh

ned_dir=$1
ned_version=$(sed -e 's/\r$//' $1/ne_10m_populated_places.VERSION.txt)

ogr2ogr -f GeoJSON /vsistdout/ $ned_dir/ne_10m_populated_places.shp | jq """
{
  type: .type,
  name: .name,
  crs: .crs,
  features: [.features[] |
  {
    type: \"Feature\",
    geometry: {
      type: \"Point\",
      coordinates: .geometry.coordinates
    },
    properties: {
      country_iso2: .properties.ISO_A2,
      source: \"NaturalEarthData\",
      source_version: \"$ned_version\",
      source_id: (.properties.ne_id)|tostring,
      geoname_id: .properties.GEONAMEID,
      population: .properties.GN_POP,
      name: .properties.NAME,
      name_ar: .properties.name_ar,
      name_bn: .properties.name_bn,
      name_de: .properties.name_de,
      name_en: .properties.name_en,
      name_es: .properties.name_es,
      name_fr: .properties.name_fr,
      name_el: .properties.name_el,
      name_hi: .properties.name_hi,
      name_hu: .properties.name_hu,
      name_id: .properties.name_id,
      name_it: .properties.name_it,
      name_ja: .properties.name_ja,
      name_ko: .properties.name_ko,
      name_nl: .properties.name_nl,
      name_pl: .properties.name_pl,
      name_pt: .properties.name_pt,
      name_ru: .properties.name_ru,
      name_sv: .properties.name_sv,
      name_tr: .properties.name_tr,
      name_vi: .properties.name_vi,
      name_zh: .properties.name_zh
    }
  }]
}
"""
