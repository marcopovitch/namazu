FROM elixir:1.9-alpine

ENV MIX_ENV=prod

EXPOSE 4000

COPY . /opt/namazu

RUN adduser -S -h /opt/namazu namazu \
 && chown -R namazu /opt/namazu

WORKDIR /opt/namazu

USER namazu

RUN mix local.hex --force \
 && mix local.rebar --force \
 && mix deps.get \
 && mix deps.compile \
 && mix compile \
 && cd apps/web \
 && mix phx.digest

# CMD mix phx.server --no-compile --no-halt
CMD mix compile --force && mix phx.server --no-halt
